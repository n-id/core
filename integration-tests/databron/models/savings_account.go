package models

import (
	"context"
	"errors"
	"time"

	"github.com/gofrs/uuid"
	"gorm.io/gorm"
)

// SavingsAccount Relational Model
type SavingsAccount struct {
	ID            uuid.UUID    `gorm:"type:uuid;default:gen_random_uuid();primaryKey" json:"id"` // primary key
	Amount        int          `json:"amount"`
	BankAccount   *BankAccount `gorm:"foreignKey:BankAccountID" json:"bank_account"`
	BankAccountID uuid.UUID    `gorm:"index:idx_savings_account_bank_account_id" json:"bank_account_id"`
	Name          string       `json:"name"`
	CreatedAt     time.Time    `json:"created_at"`
	DeletedAt     *time.Time   `json:"deleted_at"`
	UpdatedAt     time.Time    `json:"updated_at"`
}

// TableName overrides the table name settings in Gorm to force a specific table name
// in the database.
func (m *SavingsAccount) TableName() string {
	return "savings_accounts"
}

// SavingsAccountDB is the implementation of the storage interface for
// SavingsAccount.
type SavingsAccountDB struct {
	//nolint:stylecheck //deprecated
	Db *gorm.DB // Deprecated: Use SavingsAccountDB.DB() instead.
}

// NewSavingsAccountDB creates a new storage type.
func NewSavingsAccountDB(db *gorm.DB) *SavingsAccountDB {
	return &SavingsAccountDB{Db: db}
}

// DB returns the underlying database.
func (m *SavingsAccountDB) DB() interface{} {
	return m.Db
}

// TableName returns the table name of the associated model
//
// Deprecated: Use db.Model(SavingsAccount{}) instead.
func (m *SavingsAccountDB) TableName() string {
	return "savings_accounts"
}

// CRUD Functions

// Get returns a single SavingsAccount as a Database Model
func (m *SavingsAccountDB) Get(_ context.Context, id uuid.UUID) (*SavingsAccount, error) {
	var native SavingsAccount
	err := m.Db.Table(m.TableName()).Where("id = ?", id).First(&native).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, err
	}

	return &native, err
}

// List returns an array of SavingsAccount
func (m *SavingsAccountDB) List(_ context.Context) ([]*SavingsAccount, error) {
	var objs []*SavingsAccount
	err := m.Db.Table(m.TableName()).Find(&objs).Error
	if err != nil && !errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, err
	}

	return objs, nil
}

// Add creates a new record.
func (m *SavingsAccountDB) Add(_ context.Context, model *SavingsAccount) error {
	err := m.Db.Create(model).Error
	if err != nil {
		return err
	}

	return nil
}

// Update modifies a single record.
func (m *SavingsAccountDB) Update(ctx context.Context, model *SavingsAccount) error {
	obj, err := m.Get(ctx, model.ID)
	if err != nil {
		return err
	}
	err = m.Db.Model(obj).Updates(model).Error

	return err
}

// Delete removes a single record.
func (m *SavingsAccountDB) Delete(_ context.Context, id uuid.UUID) error {
	err := m.Db.Where("id = ?", id).Delete(&SavingsAccount{}).Error
	if err != nil {
		return err
	}

	return nil
}
