package models

// GetModels returns the models for this package.
func GetModels() []interface{} {
	return []interface{}{
		Address{},
		BankAccount{},
		ContactDetail{},
		SavingsAccount{},
		User{},
	}
}
