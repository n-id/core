//go:build integration || to || files

package integration

import (
	"testing"

	gql "gitlab.com/n-id/core/pkg/gqlclient"

	"github.com/stretchr/testify/suite"
)

type AuthGQLIntegrationTestSuite struct {
	BaseTestSuite
}

type introspectResponse struct {
	Schema schema `json:"__schema"`
}

type schema struct {
	Types []gqlType
}

type gqlType struct {
	Name   string
	Fields []string
}

func TestAuthGQLIntegrationTestSuite(t *testing.T) {
	suite.Run(t, new(AuthGQLIntegrationTestSuite))
}

func (s *AuthGQLIntegrationTestSuite) TestAuthGQLListNamespaces() {
	introspectQuery := `{
		 __schema {
		   types {
			 name
		   }
		 }
		}`
	res := &introspectResponse{}
	s.Require().NoError(s.clients.authGQLClient.Run(s.ctx, gql.NewRequest(introspectQuery), res, gql.MethodPost))
	s.Require().LessOrEqual(1, len(res.Schema.Types))
}
