//go:build integration || to || files

package integration

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/suite"
	"github.com/vrischmann/envconfig"
)

type WellKnownIntegrationTestSuiteConfig struct {
	Namespace string `envconfig:"NAMESPACE"`
}

type WellKnownIntegrationTestSuite struct {
	BaseTestSuite
	conf       *WellKnownIntegrationTestSuiteConfig
	httpClient *http.Client
}

func (s *WellKnownIntegrationTestSuite) SetupSuite() {
	s.BaseTestSuite.SetupSuite()
	s.httpClient = http.DefaultClient

	s.conf = &WellKnownIntegrationTestSuiteConfig{}
	err := envconfig.Init(&s.conf)
	s.Require().NoError(err, "reading env")
}

func TestWellKnownIntegrationTestSuite(t *testing.T) {
	suite.Run(t, new(WellKnownIntegrationTestSuite))
}

func (s *WellKnownIntegrationTestSuite) TestWellKnownOpenIDConfiguration() {
	url := fmt.Sprintf("http://auth.%s/.well-known/openid-configuration", s.envConfig.BackendURL)
	req, err := http.NewRequestWithContext(s.ctx, http.MethodGet, url, nil)
	s.Require().NoError(err)

	resp, err := s.httpClient.Do(req)
	s.Require().NoError(err, "expected no error (this is probably due the http transcoder)")
	defer func(test *WellKnownIntegrationTestSuite) {
		test.Require().NoError(resp.Body.Close())
	}(s)

	// nolint: govet
	respBody := struct {
		Issuer                           string   `json:"issuer"`
		AuthorizationEndpoint            string   `json:"authorizationEndpoint"`
		TokenEndpoint                    string   `json:"tokenEndpoint"`
		JWKSURI                          string   `json:"jwksUri"`
		ScopesSupported                  []string `json:"scopesSupported"`
		ResponseTypesSupported           []string `json:"responseTypesSupported"`
		GrantTypesSupported              []string `json:"grantTypesSupported"`
		IDTokenSigningAlgValuesSupported []string `json:"idTokenSigningAlgValuesSupported"`
		ClaimTypesSupported              []string `json:"claimTypesSupported"`
	}{}

	s.Require().Equal(http.StatusOK, resp.StatusCode)
	body, err := io.ReadAll(resp.Body)
	s.Require().NoError(err)

	s.Require().NoError(json.Unmarshal(body, &respBody))
	s.Equal("auth."+s.conf.Namespace, respBody.Issuer)
	s.Require().Len(respBody.ScopesSupported, 1)
	s.Equal("openid", respBody.ScopesSupported[0])
	s.Require().Len(respBody.ResponseTypesSupported, 1)
	s.Equal("code", respBody.ResponseTypesSupported[0])
	s.Require().Len(respBody.GrantTypesSupported, 1)
	s.Equal("authorization_code", respBody.GrantTypesSupported[0])
	s.Require().Len(respBody.IDTokenSigningAlgValuesSupported, 1)
	s.Equal("RS256", respBody.IDTokenSigningAlgValuesSupported[0])
}
