//go:build integration || to || files

package integration

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"testing"

	"github.com/stretchr/testify/suite"
	walletPB "gitlab.com/n-id/core/svc/wallet-rpc/proto"
	"google.golang.org/protobuf/types/known/emptypb"

	"github.com/go-resty/resty/v2"
	"github.com/vrischmann/envconfig"
	"google.golang.org/grpc/metadata"

	databron "gitlab.com/n-id/core/integration-tests/databron/models"
	"gitlab.com/n-id/core/pkg/utilities/errors"
	authModels "gitlab.com/n-id/core/svc/auth/models"
	authpb "gitlab.com/n-id/core/svc/auth/transport/grpc/proto"
)

const basicAuthPrefix = "basic "

type AuthIntegrationTestSuite struct {
	BaseTestSuite

	httpClient *http.Client
}

func (s *AuthIntegrationTestSuite) SetupSuite() {
	s.BaseTestSuite.SetupSuite()

	s.Require().NoError(envconfig.Init(&s.envConfig), "unable to initialise environment config")

	s.connectAuthService()

	s.connectWalletRPCService()

	s.httpClient = http.DefaultClient
}

func (s *AuthIntegrationTestSuite) connectAuthService() {
	// Create auth client connection
	conn, err := s.GetGRPCClient(s.envConfig.Service.Auth)
	s.Require().NoError(err, "unable to start grpc service for auth")
	s.authClient = authpb.NewAuthClient(conn)
}

func (s *AuthIntegrationTestSuite) connectWalletRPCService() {
	conn, err := s.GetGRPCClient(s.envConfig.Service.WalletRPC)
	s.Require().NoError(err, "unable to start grpc service for wallet client")
	s.walletAuthClient = walletPB.NewAuthorizationClient(conn)
}

func (s *AuthIntegrationTestSuite) TestAuthorize() {
	audienceDB := authModels.AudienceDB{}
	accessModelDB := authModels.AccessModelDB{}

	authTestConfig, err := GetAuthTestConfig()
	s.Require().NoError(err)

	token, accessType := AuthorizeTokens(&s.BaseTestSuite, &s.authClient, s.authHTTPClient,
		NewAuthTest(audienceDB.DefaultModelPrimary(s.envConfig.Namespace),
			accessModelDB.DefaultModelUserFirstAndLastName(s.envConfig.Namespace),
			accessModelDB.DefaultModelOptionalBankAccounts(s.envConfig.Namespace),
			true,
			authTestConfig))

	s.NotEmpty(token)
	s.NotEmpty(accessType)
}

func (s *AuthIntegrationTestSuite) TestAuthorizeHTTP() {
	audienceDB := authModels.AudienceDB{}
	accessModelDB := authModels.AccessModelDB{}

	authTestConfig, err := GetAuthTestConfig()
	s.Require().NoError(err)

	token, accessType := s.AuthorizeTokensHTTP(&s.authClient, s.authHTTPClient,
		NewAuthTest(audienceDB.DefaultModelPrimary(s.envConfig.Namespace),
			accessModelDB.DefaultModelUserFirstAndLastName(s.envConfig.Namespace),
			accessModelDB.DefaultModelOptionalBankAccounts(s.envConfig.Namespace),
			true,
			authTestConfig))

	s.NotEmpty(token)
	s.NotEmpty(accessType)
}

type AuthFlowTest struct {
	*AuthTestConfig
	inScope                 string
	inResponseType          string
	inClientID              string
	inRedirectURI           string
	inAudience              string
	inOptionalScopes        string
	optOptionalAccessModels bool
}

type AuthTestConfig struct {
	ClientPassword string `envconfig:"CLIENT_PASSWORD"`
	DeviceCode     string `envconfig:"DEVICE_CODE"`
	DeviceSecret   string `envconfig:"DEVICE_SECRET"`
	UserPseudo     string `envconfig:"optional,USER_PSEUDO"`
}

func NewAuthTest(audience authModels.Audience, required, optional authModels.AccessModel, acceptOptional bool, testConfig *AuthTestConfig) AuthFlowTest {
	clientDB := authModels.ClientDB{}
	defaultClient := clientDB.DefaultModel()
	redirectTargetDB := authModels.RedirectTargetDB{}
	defaultRedirectTarget := redirectTargetDB.DefaultModel(defaultClient.ID)
	defaultAudience := audience
	defaultAccessModel := required
	defaultAccessModelopt := optional

	scope := fmt.Sprintf("openid %s@%s", defaultAccessModel.Name, defaultAccessModel.Hash)
	optscope := fmt.Sprintf("%s@%s", defaultAccessModelopt.Name, defaultAccessModelopt.Hash)

	defaultTest := AuthFlowTest{
		inScope:                 scope,
		inResponseType:          "code",
		inClientID:              defaultClient.ID.String(),
		inRedirectURI:           defaultRedirectTarget.RedirectTarget,
		inAudience:              defaultAudience.Audience,
		inOptionalScopes:        optscope,
		optOptionalAccessModels: acceptOptional,
		AuthTestConfig:          testConfig,
	}

	return defaultTest
}

// AuthorizeTokens Runs tests methods against the auth service.
// This method will invoke all the GRPC methods to authorize and get a JWT with access models.
func AuthorizeTokens(s *BaseTestSuite, protoClient *authpb.AuthClient, httpClient *resty.Client, test AuthFlowTest) (string, string) {
	s.Require().NotNil(s, "suite can't be nil")
	s.Require().NotNil(protoClient, "proto client can't be nil")
	s.Require().NotNil(httpClient, "http client can't be nil")
	s.Require().NotEmpty(test, "test can't be empty")

	c := *protoClient

	var accessToken, tokenType, sessionID, authorizationCode, sessionFinaliseToken string
	var accessModelIDs []string

	s.Run("authorize", func() {
		req := httpClient.R()
		query := url.Values{
			"scope":         []string{test.inScope},
			"response_type": []string{test.inResponseType},
			"client_id":     []string{test.inClientID},
			"redirect_uri":  []string{test.inRedirectURI},
			"audience":      []string{test.inAudience},
		}

		scopeURL := url.URL{Path: test.inScope}
		scopeEncoded := scopeURL.String()[2:]

		optionalScopesURL := url.URL{Path: test.inOptionalScopes}
		optionalScopesEncoded := optionalScopesURL.String()[2:]

		res, err := req.Get("/authorize?" + query.Encode() + "&scope=" + scopeEncoded + "&optional_scopes=" + optionalScopesEncoded)
		s.Require().ErrorIs(err, errStopRedirection)

		headers := res.Header()
		s.Require().NotNil(headers)

		s.Require().Equal(http.StatusFound, res.StatusCode(), "body: %s, headers: %v", string(res.Body()), headers)

		// get session id
		locationList, ok := headers["Location"]
		s.Require().True(ok, "%v", headers)
		s.Require().NotEmpty(locationList)
		s.Require().Len(locationList, 1)
		location := locationList[0]
		splitLocation := strings.Split(location, "#")
		s.Require().Len(splitLocation, 2)
		sessionID = splitLocation[len(splitLocation)-1]
		requestSessionState(s, c, sessionID, authpb.SessionState_UNCLAIMED)
	})

	s.Require().NotEmpty(sessionID)

	basicAuthHeader := basicAuthPrefix + base64.StdEncoding.EncodeToString([]byte(test.DeviceCode+":"+test.DeviceSecret))
	ctx := metadata.AppendToOutgoingContext(context.Background(), "authorization", basicAuthHeader)
	signInResponse, err := s.walletAuthClient.SignIn(ctx, &emptypb.Empty{})
	s.Require().NoError(err)

	walletAuthHeader := "Bearer " + signInResponse.Bearer

	s.Run("session-token", func() {
		ctx = metadata.AppendToOutgoingContext(context.Background(), "authorization", walletAuthHeader)
		tokenReq := &authpb.SessionRequest{
			SessionId: sessionID,
		}
		resp, err := c.GenerateSessionFinaliseToken(ctx, tokenReq)
		s.Require().NoError(err)
		s.Require().NotNil(resp)
		s.Require().NotNil(resp.GetFinaliseToken())

		sessionFinaliseToken = resp.GetFinaliseToken()
	})

	s.Run("claim", func() {
		ctx = metadata.AppendToOutgoingContext(context.Background(), "authorization", walletAuthHeader)
		claimReq := &authpb.SessionRequest{
			SessionId: sessionID,
		}
		resp, err := c.Claim(ctx, claimReq)
		s.Require().NoError(err)
		s.Require().NotNil(resp)
		s.Require().NotNil(resp.GetClient())
		s.Require().Equal(test.inClientID, resp.GetClient().Id)
		s.Require().NotNil(resp.GetAudience())
		s.Require().Equal(test.inAudience, resp.GetAudience().Audience)
		s.Require().NotNil(resp.GetRequiredAccessModels())
		s.Require().Len(resp.GetOptionalAccessModels(), len(strings.Split(test.inOptionalScopes, " ")))
		requestSessionState(s, c, sessionID, authpb.SessionState_CLAIMED)

		if test.optOptionalAccessModels {
			for _, model := range resp.GetOptionalAccessModels() {
				accessModelIDs = append(accessModelIDs, model.Id)
			}
		}
	})

	s.Run("accept", func() {
		ctx = metadata.AppendToOutgoingContext(context.Background(), "authorization", walletAuthHeader)
		acceptReq := &authpb.AcceptRequest{
			SessionId:      sessionID,
			AccessModelIds: accessModelIDs,
		}
		resp, err := c.Accept(ctx, acceptReq)
		s.Require().NoError(err)
		s.Require().NotNil(resp)
		s.Require().NotNil(resp.GetClient())
		s.Require().Equal(test.inClientID, resp.GetClient().Id)
		s.Require().NotNil(resp.GetAudience())
		s.Require().Equal(test.inAudience, resp.GetAudience().Audience)
		s.Require().NotNil(resp.GetRequiredAccessModels())
		requestSessionState(s, c, sessionID, authpb.SessionState_ACCEPTED)
	})

	s.Run("finalise", func() {
		sessionRequest := &authpb.FinaliseRequest{SessionId: sessionID, SessionFinaliseToken: sessionFinaliseToken}

		response, err := c.Finalise(context.Background(), sessionRequest)
		s.Require().NoError(err)
		s.Require().NotNil(response)
		s.NotEmpty(response.GetRedirectLocation())

		location := response.GetRedirectLocation()
		u, err := url.Parse(location)
		s.Require().NoError(err)
		m, _ := url.ParseQuery(u.RawQuery)
		codeList, ok := m["authorization_code"]
		s.Require().True(ok)
		s.Require().Len(codeList, 1)
		authorizationCode = codeList[0]
		requestSessionState(s, c, sessionID, authpb.SessionState_CODE_GRANTED)
	})

	s.Run("token", func() {
		tokenReq := &authpb.TokenRequest{
			GrantType: "authorization_code",
			TypeValue: &authpb.TokenRequest_AuthorizationCode{AuthorizationCode: authorizationCode},
		}

		basicAuthHeader := basicAuthPrefix + base64.StdEncoding.EncodeToString([]byte(test.inClientID+":"+test.ClientPassword))
		ctx := metadata.AppendToOutgoingContext(context.Background(), "authorization", basicAuthHeader)

		tokenResp, err := c.Token(ctx, tokenReq)
		s.Require().NoError(err)
		s.Require().NotNil(tokenResp)
		s.Require().NotEmpty(tokenResp.GetAccessToken())
		s.Require().NotEmpty(tokenResp.GetTokenType())
		accessToken = tokenResp.GetAccessToken()
		tokenType = tokenResp.GetTokenType()
		requestSessionState(s, c, sessionID, authpb.SessionState_TOKEN_GRANTED)
	})

	return accessToken, tokenType
}

// AuthorizeTokensHTTP Runs tests methods against the auth service.
// This method will call all the HTTP endpoints to authorize and get a JWT with access models.
func (s *AuthIntegrationTestSuite) AuthorizeTokensHTTP(protoClient *authpb.AuthClient, httpClient *resty.Client, test AuthFlowTest) (string, string) {
	s.Require().NotNil(s, "suite can't be nil")
	s.Require().NotNil(protoClient, "proto client can't be nil")
	s.Require().NotNil(httpClient, "http client can't be nil")
	s.Require().NotEmpty(test, "test can't be empty")

	var accessToken, tokenType, sessionID, authorizationCode, sessionFinaliseToken string
	var accessModelIDs []string

	s.Run("authorize", func() {
		req := httpClient.R()
		query := url.Values{
			"scope":         []string{test.inScope},
			"response_type": []string{test.inResponseType},
			"client_id":     []string{test.inClientID},
			"redirect_uri":  []string{test.inRedirectURI},
			"audience":      []string{test.inAudience},
		}

		scopeURL := url.URL{Path: test.inScope}
		scopeEncoded := scopeURL.String()[2:]

		optionalScopesURL := url.URL{Path: test.inOptionalScopes}
		optionalScopesEncoded := optionalScopesURL.String()[2:]

		res, err := req.Get("/authorize?" + query.Encode() + "&scope=" + scopeEncoded + "&optional_scopes=" + optionalScopesEncoded)
		s.Require().ErrorIs(err, errStopRedirection)

		headers := res.Header()
		s.Require().NotNil(headers)

		s.Require().Equal(http.StatusFound, res.StatusCode(), "body: %s, headers: %v", string(res.Body()), headers)

		// get session id
		locationList, ok := headers["Location"]
		s.Require().True(ok, "%v", headers)
		s.Require().NotEmpty(locationList)
		s.Require().Len(locationList, 1)
		location := locationList[0]
		splitLocation := strings.Split(location, "#")
		s.Require().Len(splitLocation, 2)
		sessionID = splitLocation[len(splitLocation)-1]
		s.requestSessionStateHTTP(sessionID, authModels.SessionStateUnclaimed)
	})

	s.Require().NotEmpty(sessionID)

	basicAuthHeader := basicAuthPrefix + base64.StdEncoding.EncodeToString([]byte(test.DeviceCode+":"+test.DeviceSecret))
	ctx := metadata.AppendToOutgoingContext(context.Background(), "authorization", basicAuthHeader)
	signInResponse, err := s.walletAuthClient.SignIn(ctx, &emptypb.Empty{})
	s.Require().NoError(err)

	walletAuthHeader := "Bearer " + signInResponse.Bearer

	s.Run("session-token", func() {
		req := httpClient.NewRequest()

		req.SetBody(authModels.SessionRequest{
			SessionID: sessionID,
		})
		resp, err := req.Post("/generate-session-finalise-token")
		s.Require().NoError(err)
		s.Require().NotNil(resp)

		sessionAuthz := authModels.SessionAuthorization{}
		err = json.Unmarshal(resp.Body(), &sessionAuthz)
		s.Require().NoError(err)
		s.Require().NotNil(sessionAuthz.FinaliseToken)

		sessionFinaliseToken = sessionAuthz.FinaliseToken
	})

	s.Run("claim", func() {
		req := httpClient.NewRequest()

		req.Header.Set("Authorization", walletAuthHeader)
		req.SetBody(&authModels.SessionRequest{
			SessionID: sessionID,
		})

		resp, err := req.Post("claim")
		s.Require().NoError(err)
		s.Require().NotNil(resp)

		claimResp := authModels.SessionResponse{}
		err = json.Unmarshal(resp.Body(), &claimResp)
		s.Require().NoError(err)

		s.Require().NotNil(claimResp.Client)
		s.Require().Equal(test.inClientID, claimResp.Client.ID.String())
		s.Require().NotNil(claimResp.Audience)
		s.Require().Equal(test.inAudience, claimResp.Audience.Audience)
		s.Require().NotNil(claimResp.RequiredAccessModels)
		s.Require().Len(claimResp.OptionalAccessModels, len(strings.Split(test.inOptionalScopes, " ")))
		s.requestSessionStateHTTP(sessionID, authModels.SessionStateClaimed)

		if test.optOptionalAccessModels {
			for _, model := range claimResp.OptionalAccessModels {
				accessModelIDs = append(accessModelIDs, model.ID.String())
			}
		}
	})

	s.Run("accept", func() {
		req := httpClient.NewRequest()

		req.Header.Set("Authorization", walletAuthHeader)
		req.SetBody(&authModels.SessionRequest{
			SessionID: sessionID,
		})

		resp, err := req.Post("accept")
		s.Require().NoError(err)
		s.Require().NotNil(resp)

		accepResp := authModels.SessionResponse{}
		err = json.Unmarshal(resp.Body(), &accepResp)
		s.Require().NoError(err)

		s.Require().NoError(err)
		s.Require().NotNil(accepResp)
		s.Require().NotNil(accepResp.Client.ID)
		s.Require().Equal(test.inClientID, accepResp.Client.ID.String())
		s.Require().NotNil(accepResp.Audience)
		s.Require().Equal(test.inAudience, accepResp.Audience.Audience)
		s.Require().NotNil(accepResp.RequiredAccessModels)
		s.requestSessionStateHTTP(sessionID, authModels.SessionStateAccepted)
	})

	s.Run("finalise", func() {
		req := httpClient.NewRequest()

		sessionRequest := &authModels.FinaliseRequest{SessionID: sessionID, SessionFinaliseToken: sessionFinaliseToken}
		req.SetBody(sessionRequest)

		response, err := req.Post("/finalise")
		s.Require().NoError(err)
		s.Require().NotNil(response)

		finaliseResp := &authModels.FinaliseResponse{}
		err = json.Unmarshal(response.Body(), finaliseResp)
		s.Require().NoError(err)
		s.Require().NotEmpty(finaliseResp.RedirectLocation)

		location := finaliseResp.RedirectLocation
		u, err := url.Parse(location)
		s.Require().NoError(err)
		m, _ := url.ParseQuery(u.RawQuery)
		codeList, ok := m["authorization_code"]
		s.Require().True(ok)
		s.Require().Len(codeList, 1)
		authorizationCode = codeList[0]
		s.requestSessionStateHTTP(sessionID, authModels.SessionStateCodeGranted)
	})

	s.Run("token", func() {
		req := httpClient.NewRequest()

		basicAuthHeader := basicAuthPrefix + base64.StdEncoding.EncodeToString([]byte(test.inClientID+":"+test.ClientPassword))

		req.SetQueryParams(map[string]string{
			"grant_type":         "authorization_code",
			"authorization_code": authorizationCode,
		})
		req.SetHeader("Authorization", basicAuthHeader)

		resp, err := req.Get("/token")
		s.Require().NoError(err)
		s.Require().NotNil(resp)

		tokenResponse := &authModels.TokenResponse{}
		err = json.Unmarshal(resp.Body(), tokenResponse)
		s.Require().NoError(err)

		s.Require().NotEmpty(tokenResponse.AccessToken)
		s.Require().NotEmpty(tokenResponse.TokenType)
		accessToken = tokenResponse.AccessToken
		tokenType = tokenResponse.TokenType
		s.requestSessionStateHTTP(sessionID, authModels.SessionStateTokenGranted)
	})

	return accessToken, tokenType
}

func (s *AuthIntegrationTestSuite) requestSessionStateHTTP(sessionID string, state authModels.SessionState) {
	req := s.authHTTPClient.NewRequest()

	req.SetBody(authModels.SessionRequest{
		SessionID: sessionID,
	})
	resp, err := req.Post("/status")
	s.Require().NotNil(resp)
	s.Require().NoError(err)

	statusResp := authModels.StatusResponse{}
	err = json.Unmarshal(resp.Body(), &statusResp)

	s.Require().NoError(err)
	s.Require().Equal(state, statusResp.State)
}

func requestSessionState(s *BaseTestSuite, c authpb.AuthClient, sessionID string, state authpb.SessionState) {
	statusReq := &authpb.SessionRequest{SessionId: sessionID}
	statusResp, err := c.Status(context.Background(), statusReq)
	s.Require().NoError(err)
	s.Require().Equal(state, statusResp.GetState())
}

func GetAuthTestConfig() (*AuthTestConfig, error) {
	config := &AuthTestConfig{}
	err := envconfig.Init(config)
	if err != nil {
		return nil, errors.Wrap(err, "initialising env")
	}

	if config.UserPseudo == "" {
		config.UserPseudo = DefaultUserPseudonym()
	}

	return config, nil
}

// UserPseudonym retrieves user pseudonym
func DefaultUserPseudonym() string {
	userDB := databron.UserDB{}
	defaultUser := userDB.DefaultModel()
	return defaultUser.Pseudonym
}

func TestAuthTestSuite(t *testing.T) {
	suite.Run(t, &AuthIntegrationTestSuite{})
}
