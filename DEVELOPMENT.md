# Development documentation

This documentation aims to provide all the necessary information for getting up and running with this project locally.

## Prerequisites

This project uses devbox to manage dependencies, `make` to run commands and `docker` to run postgres for integration tests.

- [devbox](https://www.jetify.com/devbox/docs/installing_devbox/)
- [docker](https://docs.docker.com/engine/install/) (or our favorite docker alternative [orbstack](https://docs.orbstack.dev/quick-start))

After installing devbox, from the project root allow direnv to activate the devbox environment by running:

```bash
direnv allow
```

## Running integration tests

See `integration-tests/README.md`.

## Running unit tests

Running unit tests in this project requires a local postgres database. You can run it with docker:

```shell
docker run -it -p 5432:5432 -e POSTGRES_PASSWORD=postgres postgres:alpine3.18
```

## Developing a service with protobuf

### Editing a existing request or response

When changing anything in a .proto file you must make sure the protobuf gen reruns, this updates the interfaces. In the root of the project (`~/core/`), run:

```bash
make generate
```

### New RPC/endpoint

When creating a new rpc/endpoint you change the .proto file accordingly by registering the `message`'s and `rpc`. Make sure to [update the interfaces](#editing-a-existing-request-or-response) and if the service uses mocks for testing, update the mocks as well.

1. [Update the interfaces.](#editing-a-existing-request-or-response)
1. Navigate to the service's directory e.g. `~/core/svc/auth/`.
1. Find the name of the client interface that the protobuf gen generated, usually the name of the .proto file e.g `auth` + client pascal cased e.g. `AuthClient`. Run the following command for the updated service in the service's proto directory eg.

   ```bash
   mockery --name=<ExampleClient> --filename=<example_client>.go --output=proto/mock --outpkg=mock --dir=proto
   ```

   For example:

   ```bash
   mockery --name=AuthClient --filename=auth_client.go --output=proto/mock --outpkg=mock --dir=proto
   ```
