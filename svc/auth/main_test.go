//go:build integration
// +build integration

package main

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"testing"

	"github.com/cucumber/godog"
	"github.com/vrischmann/envconfig"
	"gitlab.com/n-id/core/pkg/password"
	"gitlab.com/n-id/core/pkg/utilities/database/v2"
	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/pkg/utilities/log/v2"
	"gitlab.com/n-id/core/svc/auth/models"
	"gorm.io/datatypes"
	"gorm.io/gorm"
)

var err = errors.New("feature error")

type Config struct {
	Domain string `envconfig:"CLUSTER_HOST"`
	Port   string `envconfig:"TRANSPORT_HTTP_PORT"`
}

func TestFeatures(t *testing.T) {
	db := database.MustConnectTest("auth", nil)
	err := db.AutoMigrate(models.GetModels()...)
	if err != nil {
		t.Fatal(err)
	}

	clientPass := "test^123"
	passHash, err := password.NewDefaultManager().GenerateHash(clientPass)
	if err != nil {
		t.Fatal(err)
	}

	client := &models.Client{
		Color:    "blue",
		Name:     "testclient",
		Password: passHash,
		Metadata: datatypes.JSON([]byte(`{"oin":"000012345"}`)),
	}

	err = db.Create(client).Error
	if err != nil {
		t.Fatal(err)
	}

	var conf Config
	if err := envconfig.Init(&conf); err != nil {
		log.WithError(err).Fatal("unable to load environment config")
	}

	suite := godog.TestSuite{
		ScenarioInitializer: InitializeScenario(db, conf, client.ID.String(), clientPass),
		Options: &godog.Options{
			Format:   "pretty",
			Paths:    []string{"features"},
			TestingT: t,
		},
	}

	if suite.Run() != 0 {
		t.Fatal("non-zero status returned, failed to run feature tests")
	}
}

func InitializeScenario(db *gorm.DB, conf Config, clientID, clientPass string) func(ctx *godog.ScenarioContext) {
	return func(ctx *godog.ScenarioContext) {
		api := &apiFeature{
			config:         conf,
			requestBody:    make(map[string]interface{}),
			requestHeaders: make(http.Header),
		}

		ctx.Before(func(ctx context.Context, _ *godog.Scenario) (context.Context, error) {
			api.tx = db.Begin()
			err := api.tx.AutoMigrate(models.GetModels()...)
			if err != nil {
				return nil, err
			}

			api.testClientBasicAuth = base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf("%s:%s", clientID, clientPass)))
			return ctx, nil
		})

		ctx.After(func(ctx context.Context, _ *godog.Scenario, _ error) (context.Context, error) {
			err = api.tx.Rollback().Error
			if err != nil {
				return ctx, err
			}

			return ctx, err
		})

		ctx.Given(`^I request (?:the|a|an) "([^"]*)" of "([^"]*)"$`, api.iRequestBodyWith)
		ctx.Given(`^I request (with|without) basic authentication$`, api.iRequestBasicAuthentication)

		ctx.When(`^I request a token from the "token" endpoint$`, api.iRequestToken)
		ctx.When(`^I send "([^"]*)" request to "([^"]*)"$`, api.iSendRequestTo)

		ctx.Then(`^the token should contain no aggregated claims$`, api.theResponseShouldContainNoAggregatedClaims)
		ctx.Then(`^the response should be "([^"]*)"$`, api.theResponseShouldBe)
		ctx.Then(`^the response code should be (\d+)$`, api.theResponseCodeShouldBe)
		ctx.Then(`^the token should contain the "([^"]*)" (aggregated\sclaim|claim)$`, api.theTokenShouldContainClaim)
		ctx.Then(`^the response should contain "(.*)"$`, api.theResponseShouldContain)
	}
}

type apiFeature struct {
	config             Config
	tx                 *gorm.DB
	responseStatusCode int
	responseBody       []byte
	requestBody        map[string]interface{}
	requestHeaders     http.Header

	testClientBasicAuth string
}

func (a *apiFeature) iRequestBodyWith(key, value string) error {
	a.requestBody[key] = value
	return nil
}

func (a *apiFeature) iRequestBasicAuthentication(withOrWithout string) error {
	switch withOrWithout {
	case "with":
		a.requestHeaders.Add("Authorization", fmt.Sprintf("Basic %s", a.testClientBasicAuth))
	case "without":
	}
	return nil
}

func (a *apiFeature) iRequestToken() error {
	a.requestBody["grant_type"] = "client_credentials"

	jsonBody, err := json.Marshal(a.requestBody)
	if err != nil {
		return err
	}

	request, err := http.NewRequestWithContext(context.Background(), http.MethodPost, fmt.Sprintf("http://%s:%s/token", a.config.Domain, a.config.Port), bytes.NewBuffer(jsonBody))
	if err != nil {
		return err
	}

	request.Header.Set("Content-Type", "application/json")
	for key, values := range a.requestHeaders {
		request.Header.Add(key, values[0])
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return err
	}

	a.responseStatusCode = response.StatusCode
	a.responseBody, err = io.ReadAll(response.Body)
	if err != nil {
		return err
	}

	return response.Body.Close()
}

func (a *apiFeature) theTokenShouldContainClaim(claim string, claimType string) error {
	claimsMap, err := bodyClaims(a.responseBody)
	if err != nil {
		return err
	}

	switch claimType {
	case "claim":
		if _, ok := claimsMap[claim]; !ok {
			return errors.Wrapf(err, "claim %s not found in token", claim)
		}
	case "aggregated claim":
		claimNames, err := claimNames(claimsMap)
		if err != nil {
			return err
		}

		if _, ok := claimNames[claim]; !ok {
			return errors.Wrapf(err, "aggregated claim %s not found in token", claim)
		}
	}

	return nil
}

func (a *apiFeature) theResponseShouldContainNoAggregatedClaims() error {
	claimsMap, err := bodyClaims(a.responseBody)
	if err != nil {
		return err
	}

	claimNames, err := claimNames(claimsMap)
	if err != nil {
		return nil
	}

	if len(claimNames) != 0 {
		return errors.New("aggregated claims found in token")
	}

	return nil
}

func (a *apiFeature) theResponseShouldBe(expected string) error {
	expectedCode := http.StatusOK //nolint:ineffassign

	switch expected {
	case "ok":
		expectedCode = http.StatusOK
	case "unauthorized":
		expectedCode = http.StatusUnauthorized
	case "bad request":
		expectedCode = http.StatusBadRequest
	case "internal server error":
		expectedCode = http.StatusInternalServerError
	case "not found":
		expectedCode = http.StatusNotFound
	default:
		return errors.Wrapf(err, "unexpected expected response: %s", expected)
	}

	if a.responseStatusCode != expectedCode {
		return errors.Wrapf(err, "expected response to be: \"%s\" but it is \"%s\"", expected, strings.ToLower(http.StatusText(a.responseStatusCode)))
	}
	return nil
}

func (a *apiFeature) iSendRequestTo(method, endpoint string) error {
	a.responseStatusCode = 0

	request, err := http.NewRequestWithContext(context.Background(), method, fmt.Sprintf("http://%s:%s%s", a.config.Domain, a.config.Port, endpoint), nil)
	if err != nil {
		return nil
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return err
	}

	a.responseStatusCode = response.StatusCode
	a.responseBody, err = io.ReadAll(response.Body)
	if err != nil {
		return err
	}

	return response.Body.Close()
}

func (a *apiFeature) theResponseCodeShouldBe(code int) error {
	if a.responseStatusCode != code {
		return errors.Wrapf(err, "expected response code to be: %d, but actual is: %d", code, a.responseStatusCode)
	}

	return nil
}

func (a *apiFeature) theResponseShouldContain(expected string) error {
	if !strings.Contains(string(a.responseBody), expected) {
		return errors.Wrapf(err, "expected response to contain: %s but was not found in response body: %s", expected, string(a.responseBody))
	}

	return nil
}

func bodyClaims(body []byte) (map[string]interface{}, error) {
	if len(body) == 0 {
		return nil, errors.New("response body is empty")
	}

	var responseBodyMap map[string]interface{}
	err := json.Unmarshal(body, &responseBodyMap)
	if err != nil {
		return nil, err
	}

	token, ok := responseBodyMap["access_token"].(string)
	if !ok {
		return nil, errors.New("access_token claim not found")
	}

	parts := strings.Split(token, ".")
	if len(parts) != 3 {
		return nil, errors.New("token is not in JWT format")
	}

	claims, err := base64.RawStdEncoding.DecodeString(parts[1])
	if err != nil {
		return nil, err
	}

	var claimsMap map[string]interface{}
	err = json.Unmarshal(claims, &claimsMap)
	if err != nil {
		return nil, err
	}

	return claimsMap, nil
}

func claimNames(claimsMap map[string]interface{}) (map[string]interface{}, error) {
	claimNames, ok := claimsMap["_claim_names"]
	if !ok {
		return nil, errors.Wrapf(err, "claim_names not in claims")
	}

	claimNamesMap, ok := claimNames.(map[string]interface{})
	if !ok {
		return nil, errors.Wrapf(err, "claim_names is not a map")
	}

	return claimNamesMap, nil
}
