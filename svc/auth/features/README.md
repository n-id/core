# Running Feature Tests

## OPA Server

Run an opa server with the bundle from the `bundle` directory.
Make sure to load the test config and run it with --authorization=basic like so:

Run within the features directory:

```shell
opa build bundle && opa run --server -b bundle.tar.gz --config-file opa-config.yaml --authorization=basic
```

> [!TIP]
> You can log the output of opa with jq using `2>&1 > /dev/null | jq` to make it easier to read.

## Run auth service

Run the auth service, an example configuration is provided in `.auth.env.example`:

> [!NOTE]
> The feature tests can only be ran with a database identity provider, and delegation does not work for database identities.

```shell
source features/.auth.env.example && go run gitlab.com/n-id/core/svc/auth
```

## Running Tests

The tests require database access to the `auth` database. You can run the tests with the following command, when using the default test database the configuration is already set up in `.test.env.example`:

```shell
source features/.test.env.example && go test -timeout 30s -tags integration -run ^TestFeatures$ gitlab.com/n-id/core/svc/auth -count=1
```
