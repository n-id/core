Feature: Aggregated claims
    When scopes are requested that belong to an issuing authority, 
    the claims should be retrieved from the issuing authority,
    and these claims should be in the result token

    Background: Client Credentials Flow request
        Given I request a "grant_type" of "client_credentials"
        And I request an "audience" of "nid"
        And I request with basic authentication

    Scenario: request without scopes for aggregation
        Given I request the "scope" of "not-aggregated"
        When I request a token from the "token" endpoint
        Then the response should be "ok"
        And the token should contain no aggregated claims

    Scenario: request with one scope for aggregation
        Given I request the "scope" of "one-aggregated"
        When I request a token from the "token" endpoint
        Then the response should be "ok"
        And the token should contain the "issuing_authority_one" aggregated claim

    Scenario: request with multiple scopes for aggregation
        Given I request the "scope" of "multi-aggregated"
        When I request a token from the "token" endpoint
        Then the response should be "ok"
        And the token should contain the "issuing_authority_one" aggregated claim
        And the token should contain the "issuing_authority_two" aggregated claim

    Scenario: request with scopes for unauthorized authorization chaining
        Given I request the "scope" of "unauthorized-auth-chaining"
        When I request a token from the "token" endpoint
        Then the response should be "unauthorized"
