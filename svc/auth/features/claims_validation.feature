Feature: Claims validation
    When a token is requested,
    as last step in the token issuance process,
    all claims should be validated by an OPA policy.

    Background: Client Credentials Flow request
        Given I request a "grant_type" of "client_credentials"
        And I request an "audience" of "nid"
        And I request with basic authentication

    Scenario: claims validator returns allowed
        Given I request the "scope" of "valid"
        When I request a token from the "token" endpoint
        Then the response should be "ok"

    Scenario: claims validator returns a list of errors
        Given I request the "scope" of "invalid-claim"
        When I request a token from the "token" endpoint
        Then the response should be "bad request"
        And the response should contain "scope 'invalid-claim' not allowed"