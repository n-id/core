Feature: Delegation
    When a token is requested on behalf of a client,
    a token with the subject of the client should be in the result token.
