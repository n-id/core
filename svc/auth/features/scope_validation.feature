Feature: Scope validation
    When a token is requested,
    after a client has authenticated,
    the scopes should be validated by an OPA policy.

    Background: Client Credentials Flow request
        Given I request a "grant_type" of "client_credentials"
        And I request an "audience" of "nid"
        And I request with basic authentication

    Scenario: validator returns allowed
        Given I request the "scope" of "valid"
        When I request a token from the "token" endpoint
        Then the response should be "ok"

    Scenario: validator returns denied with errors
        Given I request the "scope" of "invalid-scope"
        When I request a token from the "token" endpoint
        Then the response should be "bad request"
        And the response should contain "invalid-scope"

    Scenario: validator returns denied without any errors
        Given I request the "scope" of "valid-denied-scope"
        When I request a token from the "token" endpoint
        Then the response should be "bad request"
