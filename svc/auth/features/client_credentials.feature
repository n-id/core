Feature: Client Credentials Flow
    When a token is requested using the client credentials flow,
    the token should be returned.

    Background: Client Credentials Flow request
        Given I request a "grant_type" of "client_credentials"

    Scenario: valid request with scope and audience
        Given I request the "scope" of "valid"
        And I request an "audience" of "nid"
        And I request with basic authentication
        When I request a token from the "token" endpoint
        Then the response should be "ok"

    Scenario: request without audience
        Given I request the "scope" of "valid"
        And I request with basic authentication
        When I request a token from the "token" endpoint
        Then the response should be "bad request"

    Scenario: request without basic authentication
        Given I request the "scope" of "valid"
        And I request an "audience" of "nid"
        When I request a token from the "token" endpoint
        Then the response should be "unauthorized"
