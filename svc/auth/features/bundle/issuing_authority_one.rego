package issuing_authority_one

import rego.v1

result := {
    "access_token": io.jwt.encode_sign(
        {
            "typ": "JWT",
            "alg": "HS256",
        },
        {"issuing_authority_one": "hello"},
        {
            "kty": "oct",
            "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow",
        },
    )
}

token := {
	"access_token": io.jwt.encode_sign(
		{
			"typ": "JWT",
			"alg": "HS256",
		},
		{"issuing_authority_one": "hello"},
		{
			"kty": "oct",
			"k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow",
		},
	),
	"token_type": "bearer",
} if {
	input.grant_type == "urn:ietf:params:oauth:grant-type:jwt-bearer"
	input.assertion != ""
	not "unauthorized-auth-chaining" in split(input.scope, " ")
}
