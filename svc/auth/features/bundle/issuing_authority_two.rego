package issuing_authority_two

import rego.v1

result := {
    "access_token": io.jwt.encode_sign(
        {
            "typ": "JWT",
            "alg": "HS256",
        },
        {"issuing_authority_two": "world"},
        {
            "kty": "oct",
            "k": "AyM1SysPpbyDfgZld3umj1qzKObwVMkoqQ-EstJQLr_T-1qS0gZH75aKtMN3Yj0iPS4hcgUuTwjAzZr1Z9CAow",
        },
    )
}
