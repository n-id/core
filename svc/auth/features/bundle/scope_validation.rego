package scope_validation

import rego.v1

allowed_scopes := [
	# Scope validation
	"valid-allowed-scope",

	# Claims validation
	"valid",
	"invalid-claim",

	# Aggregation
	"not-aggregated",
	"one-aggregated",
	"multi-aggregated",

	# Authorization chaining
	"unauthorized-auth-chaining",
]

result["valid_scopes"] := [valid_scope |
	valid_scope := input.scopes[_]
    valid_scope in allowed_scopes
]

default result["allowed"] := true

result["allowed"] := false if {
	"invalid-scope" in input.scopes
}

result["allowed"] := false if {
	"valid-denied-scope" in input.scopes
}

result["errors"] := msg if {
	"invalid-scope" in input.scopes
	msg := [{
		"message": sprintf("scope '%s' is not allowed", ["invalid-scope"])
	}]
}
