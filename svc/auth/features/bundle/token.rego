package system.authz

import rego.v1

default allow := true

allow := false if {
	print("input: ", input)
	"unauthorized-auth-chaining" in split(input.body.input.scope, " ")
}