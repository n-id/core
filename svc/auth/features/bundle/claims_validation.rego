package claims_validation

import rego.v1

errors contains msg if {
	"invalid-claim" in input.scopes

	msg := {
		"message": sprintf("scope '%s' not allowed", ["invalid-claim"])
	}
}