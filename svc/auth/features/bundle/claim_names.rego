package claim_names

import rego.v1

result[key] := value if {
	some key in object.keys(claims)
	value := [item | item := claims[key][_][_]]
}

# Single Aggregated
claims["http://localhost:8181/v1/data/issuing_authority_one/result"] contains {"issuing_authority_one"} if {
	"one-aggregated" in input.scopes
}

# Multi Aggregated
claims["http://localhost:8181/v1/data/issuing_authority_one/result"] contains {"issuing_authority_one"} if {
	"multi-aggregated" in input.scopes
}

claims["http://localhost:8181/v1/data/issuing_authority_two/result"] contains {"issuing_authority_two"} if {
	"multi-aggregated" in input.scopes
}
