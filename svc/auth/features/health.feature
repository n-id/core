Feature: get health
  In order to know auth API health
  As an API user
  I need to be able to request healthiness

  Scenario: should get health status
    When I send "GET" request to "/v1/health"
    Then the response code should be 200