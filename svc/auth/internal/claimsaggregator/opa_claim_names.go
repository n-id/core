package claimsaggregator

import (
	"context"

	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/internal/opa"
	"gitlab.com/n-id/core/svc/auth/models"
)

// OpaClaimNames is an implementation of ClaimNames that uses OPA to get claim names for a token request.
type OpaClaimNames struct {
	conf    *opa.Config
	opaEval contract.OpaEvaluator[models.ClaimNamesMap]
}

// NewOpaClaimNames returns a new instance of OpaClaimsSetProvider.
func NewOpaClaimNames(conf *opa.Config) *OpaClaimNames {
	return &OpaClaimNames{
		conf:    conf,
		opaEval: opa.NewOpaEval[models.ClaimNamesMap](conf),
	}
}

// ClaimNames fetches a _claim_names map from OPA.
func (p *OpaClaimNames) ClaimNames(ctx context.Context, claims *models.TokenClaims) (models.ClaimNamesMap, error) {
	decision, err := p.opaEval.Eval(ctx, claims)
	if err != nil {
		return nil, err
	}

	return decision.Result, nil
}
