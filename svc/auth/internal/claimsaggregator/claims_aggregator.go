// Package claimsaggregator provides a service that aggregates claims from multiple claims providers.
package claimsaggregator

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"sort"
	"strings"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/models"
)

const (
	errGetClaimsProvider     = "failed to get claims from provider"
	errReadRespondBodyClaims = "failed to read response body claims endpoint"
)

// ClaimsAggregator is responsible for aggregating claims from various providers.
type ClaimsAggregator struct {
	claimNames      contract.ClaimNames
	httpClient      contract.HTTPClient
	conf            *Config
	iaAuthenticator IAAuthenticator
}

// NewClaimsAggregator creates a new instance of ClaimsAggregator.
func NewClaimsAggregator(claimNames contract.ClaimNames, conf *Config, iaAuthenticator IAAuthenticator) *ClaimsAggregator {
	if conf != nil {
		conf = &Config{
			IssuingAuthorityConfigs: conf.IssuingAuthorityConfigs,
		}
	}

	return &ClaimsAggregator{
		claimNames:      claimNames,
		httpClient:      http.DefaultClient,
		conf:            conf,
		iaAuthenticator: iaAuthenticator,
	}
}

// Aggregate retrieves and aggregates claims from different providers based on the provided claims.
func (a *ClaimsAggregator) Aggregate(ctx context.Context, claims *models.TokenClaims) (*models.AggregateClaims, error) {
	allClaimNames, err := a.claimNames.ClaimNames(ctx, claims)
	if err != nil || allClaimNames == nil {
		return nil, err
	}

	aggregateClaims := &models.AggregateClaims{
		ClaimNames:   make(map[string]string),
		ClaimSources: make(map[string]*models.ClaimSource),
	}

	// Make sure the providers names are always the same for the same input.
	var claimNameProviders []string
	for k := range allClaimNames {
		claimNameProviders = append(claimNameProviders, k)
	}
	sort.Strings(claimNameProviders)

	for _, providerURL := range claimNameProviders {
		// Get the credentials and token for given providerURL
		token, err := a.iaAuthenticator.Authenticate(ctx, providerURL, claims)
		if err != nil {
			return nil, errors.Wrapf(err, "authenticating with issuing authority")
		}

		// Gather the claims from the provider.
		claimsToken, err := a.claims(ctx, token, claims, providerURL, allClaimNames[providerURL])
		if err != nil {
			return nil, errors.Wrapf(err, "failed to get claims from provider")
		}

		// If the claims token is empty, return an error.
		if claimsToken == "" {
			return nil, errors.Wrap(err, "claims token was empty")
		}

		// Create a unique provider name.
		providerName, err := aggregateClaims.CreateProviderName(providerURL)
		if err != nil {
			return nil, err
		}

		// Set the claim names and sources.
		for _, claimName := range allClaimNames[providerURL] {
			aggregateClaims.ClaimNames[claimName] = providerName
		}
		aggregateClaims.ClaimSources[providerName] = &models.ClaimSource{
			JWT: claimsToken,
		}
	}

	return aggregateClaims, nil
}

// ClaimsRequest represents a request for specific claims.
type ClaimsRequest struct {
	UID      string                           `json:"uid"`
	Audience jwt.ClaimStrings                 `json:"aud"`
	Claims   map[string]ClaimsRequestPriority `json:"claims"`
}

// ClaimsRequestPriority defines the priority of claims.
type ClaimsRequestPriority struct {
	Essential bool `json:"essential"`
}

// ClaimsResponse represents the response containing access token.
type ClaimsResponse struct {
	AccessToken string `json:"access_token"`
}

func makeClaimsRequestBody(providerURL string, claims *models.TokenClaims, claimNames []string) ([]byte, error) {
	if claims == nil {
		return nil, nil
	}

	claimsRequest := &ClaimsRequest{
		UID:      claims.Subject,
		Audience: claims.Audience,
		Claims:   make(map[string]ClaimsRequestPriority),
	}

	for _, claimName := range claimNames {
		claimsRequest.Claims[claimName] = ClaimsRequestPriority{
			Essential: true,
		}
	}

	// Facilitate OPA request to claims provider.
	if strings.HasPrefix(providerURL, "http://localhost:") {
		claimsRequestJSON, err := json.Marshal(models.OpaRequest[*ClaimsRequest]{
			Input: claimsRequest,
		})
		if err != nil {
			return nil, errors.Wrap(err, "failed to marshal claims for opa formed request")
		}

		return claimsRequestJSON, nil
	}

	claimsRequestJSON, err := json.Marshal(claimsRequest)
	if err != nil {
		return claimsRequestJSON, errors.Wrap(err, "failed to marshal claims request")
	}

	return claimsRequestJSON, nil
}

func (a *ClaimsAggregator) claims(ctx context.Context, token string, claims *models.TokenClaims, providerURL string, claimNames []string) (string, error) {
	reqBody, err := makeClaimsRequestBody(providerURL, claims, claimNames)
	if err != nil {
		return "", err
	}

	req, err := http.NewRequestWithContext(ctx, "POST", providerURL, bytes.NewBuffer(reqBody))
	if err != nil {
		return "", err
	}
	req.Header.Set("Content-Type", "application/json")
	if token != "" {
		req.Header.Set("Authorization", "Bearer "+token)
	}

	resp, err := a.httpClient.Do(req)
	if err != nil {
		return "", fmt.Errorf("%s: %w", errGetClaimsProvider, err)
	}
	var deferErr error
	defer func() {
		deferErr = resp.Body.Close()
	}()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("%s: %w", errReadRespondBodyClaims, err)
	}

	claimResponse, err := makeClaimsResponse(body)
	if err != nil {
		return "", err
	}

	return claimResponse.AccessToken, deferErr
}

func makeClaimsResponse(body []byte) (*ClaimsResponse, error) {
	var claimResponse ClaimsResponse
	err := json.Unmarshal(body, &claimResponse)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal response from claims endpoint")
	}

	if claimResponse.AccessToken == "" {
		// Try to unmarshal the response as an OPA response.
		opaClaimResponse, err := makeOpaClaimsResponse(body)
		if err != nil {
			return nil, errors.Wrap(err, "access token was empty, failed to unmarshal opa response from claims endpoint")
		}

		claimResponse = *opaClaimResponse
	}

	return &claimResponse, nil
}

func makeOpaClaimsResponse(body []byte) (*ClaimsResponse, error) {
	var decision models.OpaResponse[ClaimsResponse]
	err := json.Unmarshal(body, &decision)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to unmarshal claims from OPA")
	}

	return &decision.Result, nil
}
