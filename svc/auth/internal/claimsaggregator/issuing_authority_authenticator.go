package claimsaggregator

import (
	"context"

	"gitlab.com/n-id/core/svc/auth/models"
)

// IAAuthenticator is the interface that wraps the the authentication method for the Issuing Authority.
type IAAuthenticator interface {
	Authenticate(ctx context.Context, providerURL string, claims *models.TokenClaims) (string, error)
}
