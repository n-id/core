package claimsaggregator

import (
	"os"

	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/pkg/utilities/log/v2"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gopkg.in/yaml.v3"
)

// Config contains the configuration for the ClaimsAggregator
type Config struct {
	ProviderCredentialsFilePath string                            `envconfig:"default=/provider-credentials/provider-credentials.yaml,PROVIDER_CREDENTIALS_FILE_PATH"`
	IssuingAuthorityConfigs     map[string]IssuingAuthorityConfig `envconfig:"-"`
	Issuer                      string                            `envconfig:"-"`
}

// IssuingAuthorityConfig contains the configuration for an issuing authority, configured via yaml.
type IssuingAuthorityConfig struct {
	Credentials   string `yaml:"credentials"`
	TokenEndpoint string `yaml:"token_endpoint"`
	IsOPA         bool   `yaml:"is_opa"`
}

// Validate Validates the config for ClaimsAggregator Config
func (c *Config) Validate() error {
	if c.ProviderCredentialsFilePath == "" {
		return errors.Wrap(contract.ErrIncorrectEnvironmentConfig, "PROVIDER_CREDENTIALS_FILE_PATH is empty")
	}

	return nil
}

// Load loads provider credentials yaml from the provided path.
func (c *Config) Load() error {
	log.Infof("Loading ProviderCredentials from file: %s", c.ProviderCredentialsFilePath)
	yamlFile, err := os.ReadFile(c.ProviderCredentialsFilePath)
	if err != nil {
		log.Warn("did not find a provider credentials file config")
		return nil
	}

	issuingAuthorityConfig := map[string]IssuingAuthorityConfig{}
	if err := yaml.Unmarshal(yamlFile, &issuingAuthorityConfig); err != nil {
		return err
	}
	c.IssuingAuthorityConfigs = issuingAuthorityConfig

	if c.IssuingAuthorityConfigs == nil {
		log.Warn("no provider credentials provided")
	}

	return nil
}
