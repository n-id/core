package claimsaggregator

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLoadIssuingAuthorityConfigSuccessful(t *testing.T) {
	// Create a temporary directory
	tempDir, err := os.MkdirTemp("", "test-load-success")
	assert.NoError(t, err)
	defer os.RemoveAll(tempDir)

	// Create a temporary YAML file
	yamlContent := []byte(`
  provider1:
    credentials: credential1
    token_endpoint: /v1/token
  provider2:
    credentials: credential2
`)
	tempFile := filepath.Join(tempDir, "provider-credentials.yaml")
	err = os.WriteFile(tempFile, yamlContent, 0644)
	assert.NoError(t, err)

	// Create and load config
	config := &Config{ProviderCredentialsFilePath: tempFile}
	err = config.Load()
	assert.NoError(t, err)

	// Assert the loaded credentials
	expectedCredentials := map[string]IssuingAuthorityConfig{
		"provider1": {
			Credentials:   "credential1",
			TokenEndpoint: "/v1/token",
		},
		"provider2": {
			Credentials: "credential2",
		},
	}
	assert.Equal(t, expectedCredentials, config.IssuingAuthorityConfigs)
}

func TestLoadFileNotFound(t *testing.T) {
	config := &Config{ProviderCredentialsFilePath: "non-existent-file.yaml"}
	err := config.Load()
	assert.NoError(t, err)
	assert.Nil(t, config.IssuingAuthorityConfigs)
}

func TestLoadInvalidYAML(t *testing.T) {
	// Create a temporary directory
	tempDir, err := os.MkdirTemp("", "test-load-invalid")
	assert.NoError(t, err)
	defer os.RemoveAll(tempDir)

	// Create an invalid YAML file
	invalidYAML := []byte(`
  provider1:
    credentials: 
	  - credential1
    token_endpoint: 
	  - /v1/token
  provider2:
    credentials: 
	  - credential2
`)
	tempFile := filepath.Join(tempDir, "invalid-credentials.yaml")
	err = os.WriteFile(tempFile, invalidYAML, 0644)
	assert.NoError(t, err)

	// Attempt to load invalid config
	config := &Config{ProviderCredentialsFilePath: tempFile}
	err = config.Load()
	assert.Error(t, err)
	assert.Nil(t, config.IssuingAuthorityConfigs)
}

func TestLoadEmptyCredentials(t *testing.T) {
	// Create a temporary directory
	tempDir, err := os.MkdirTemp("", "test-load-empty")
	assert.NoError(t, err)
	defer os.RemoveAll(tempDir)

	// Create an empty YAML file
	emptyYAML := []byte(``)
	tempFile := filepath.Join(tempDir, "empty-credentials.yaml")
	err = os.WriteFile(tempFile, emptyYAML, 0644)
	assert.NoError(t, err)

	// Load empty config
	config := &Config{ProviderCredentialsFilePath: tempFile}
	err = config.Load()
	assert.NoError(t, err)
	assert.Empty(t, config.IssuingAuthorityConfigs)
}
