package claimsaggregator

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/pkg/utilities/log/v2"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/models"
)

var (
	errInvalidTokenType         = errors.New("response did not contain the token type bearer")
	errNoConfig                 = errors.New("claims aggregator configuration is not provided")
	errNoIssuingAuthorityConfig = errors.New("issuing authority config is not provided for the given provider")
	errEmptyCredentials         = errors.New("issuing authority credentials are empty")
	errNonOkStatusCode          = errors.New("non-OK status code")
)

// AuthorizationChainingIAAuthenticator authenticates requests with OAuth Identity and Authorization Chaining.
type AuthorizationChainingIAAuthenticator struct {
	conf       *Config
	jwtClient  contract.JWTClient
	httpClient contract.HTTPClient
}

// NewAuthorizationChainingIAAuthenticator creates a new AuthorizationChainingIAAuthenticator.
func NewAuthorizationChainingIAAuthenticator(conf *Config, jwtClient contract.JWTClient) *AuthorizationChainingIAAuthenticator {
	return &AuthorizationChainingIAAuthenticator{
		conf:       conf,
		jwtClient:  jwtClient,
		httpClient: http.DefaultClient,
	}
}

type accessTokenResponse struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
}

type accessTokenRequest struct {
	GrantType string `json:"grant_type"`
	Scope     string `json:"scope"`
	Assertion string `json:"assertion"`
}

// Authenticate OAuth Identity and Authorization Chaining Across Domains.
// Use nID as a trusted provider to exchange a token for a different domain.
// https://www.ietf.org/archive/id/draft-ietf-oauth-identity-chaining-01.html#section-2.3.
func (a *AuthorizationChainingIAAuthenticator) Authenticate(ctx context.Context, providerURL string, claims *models.TokenClaims) (string, error) {
	if a.conf == nil {
		return "", errNoConfig
	}

	claims.RegisteredClaims.ExpiresAt = jwt.NewNumericDate(time.Now().Add(time.Minute * 1))
	claims.RegisteredClaims.Issuer = a.conf.Issuer

	authorizationGrant, err := a.jwtClient.SignToken(claims)
	if err != nil {
		return "", errors.Wrap(err, "signing token")
	}

	issuingAuthorityConfig, err := a.issuingAuthorityConfig(providerURL)
	if err != nil {
		return "", errors.Wrap(err, "getting issuing authority config for provider "+providerURL)
	}

	scopesArray, err := claims.ScopesArray()
	if err != nil {
		return "", errors.Wrap(err, "getting scopes from claims, scopes should be an array")
	}

	requestBody := accessTokenRequest{
		GrantType: "urn:ietf:params:oauth:grant-type:jwt-bearer",
		Assertion: authorizationGrant,
		Scope:     strings.Join(scopesArray, " "),
	}

	resp, err := a.doAuthenticationRequest(ctx, providerURL, issuingAuthorityConfig, requestBody)
	if err != nil {
		return "", errors.Wrap(err, "sending an authentication request")
	}

	var deferErr error
	defer func() {
		deferErr = resp.Body.Close()
	}()

	token, err := a.getAccessToken(resp)
	if err != nil {
		return "", errors.Wrap(err, "getting access token from response")
	}

	return token, deferErr
}

// issuingAuthorityConfig gets the IssuingAuthorityConfig for the given provider from the YAML config.
func (a *AuthorizationChainingIAAuthenticator) issuingAuthorityConfig(providerURL string) (*IssuingAuthorityConfig, error) {
	issuingAuthorityConfig, ok := a.conf.IssuingAuthorityConfigs[providerURL]
	if !ok {
		return nil, errNoIssuingAuthorityConfig
	} else if issuingAuthorityConfig.Credentials == "" {
		return nil, errEmptyCredentials
	}

	return &issuingAuthorityConfig, nil
}

// doAuthenticationRequest sends an authentication request to the provider.
func (a *AuthorizationChainingIAAuthenticator) doAuthenticationRequest(ctx context.Context, providerURL string, config *IssuingAuthorityConfig, body interface{}) (*http.Response, error) {
	tokenURL, err := buildTokenURL(providerURL, config)
	if err != nil {
		return nil, errors.Wrap(err, "building token URL")
	}

	if config.IsOPA {
		body = models.OpaRequest[interface{}]{Input: body}
	}

	jsonBody, err := json.Marshal(body)
	if err != nil {
		return nil, errors.Wrap(err, "marshalling request body")
	}

	log.Debugf("sending authentication request to %s", tokenURL)
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, tokenURL, bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, errors.Wrap(err, "creating a new request")
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("grant_type", "client_credentials")
	req.Header.Set("Authorization", "Basic "+config.Credentials)

	resp, err := a.httpClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "doing authorization request")
	}

	if resp.StatusCode != http.StatusOK {
		switch resp.StatusCode {
		case http.StatusUnauthorized:
			return nil, errors.Wrapf(contract.ErrUnauthorized, "status: %s", resp.Status)
		case http.StatusNotFound:
			return nil, errors.Wrapf(contract.ErrNotFound, "status: %s", resp.Status)
		default:
			return nil, errors.Wrapf(errNonOkStatusCode, "status: %s", resp.Status)
		}
	}

	return resp, nil
}

// buildTokenURL builds the token URL for the given provider URL and config.
func buildTokenURL(providerURL string, config *IssuingAuthorityConfig) (string, error) {
	parsedURL, err := url.ParseRequestURI(providerURL)
	if err != nil {
		return "", err
	}

	tokenURL := url.URL{
		Scheme: "https",
		Host:   parsedURL.Host,
		Path:   "/token",
	}

	if strings.HasPrefix(parsedURL.Host, "localhost") {
		tokenURL.Scheme = "http"
	}

	if config != nil && config.TokenEndpoint != "" {
		tokenURL.Path = config.TokenEndpoint
	}

	// .String() escapes all invalid characters, so there is no need to validate the URL again.
	return tokenURL.String(), nil
}

// getAccessToken gets the access token from the response body.
func (a *AuthorizationChainingIAAuthenticator) getAccessToken(response *http.Response) (string, error) {
	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		return "", errors.Wrap(err, "reading body of response")
	}

	// Try a normal response first.
	var unmarshal accessTokenResponse
	err = json.Unmarshal(responseBody, &unmarshal)
	if err != nil || unmarshal.AccessToken == "" {
		// Try unmarshalling as an opa response.
		var opaResponse models.OpaResponse[accessTokenResponse]
		if opaUnmarshalErr := json.Unmarshal(responseBody, &opaResponse); opaUnmarshalErr != nil {
			return "", errors.Wrap(err, "unmarshalling response body")
		}

		unmarshal = opaResponse.Result
	}

	if !strings.EqualFold(unmarshal.TokenType, "bearer") {
		return "", errInvalidTokenType
	}

	return unmarshal.AccessToken, nil
}
