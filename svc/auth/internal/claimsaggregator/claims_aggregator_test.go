package claimsaggregator

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"io"
	"net/http"
	"testing"

	"github.com/golang-jwt/jwt/v5"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/n-id/core/pkg/utilities/errors"
	contractMock "gitlab.com/n-id/core/svc/auth/contract/mock"
	aggregationMock "gitlab.com/n-id/core/svc/auth/internal/claimsaggregator/mock"
	"gitlab.com/n-id/core/svc/auth/models"
)

func TestAggregate(t *testing.T) {
	defaultTokenClaims := &models.TokenClaims{
		RegisteredClaims: jwt.RegisteredClaims{
			Subject:  "subject",
			Audience: []string{"audience"},
		},
	}

	provider1URL := "https://provider1.com"
	provider2URL := "https://provider2.com"

	defaultProviderCredentials := map[string]string{
		"provider1.com": base64.URLEncoding.EncodeToString([]byte("username:password123")),
		"provider2.com": base64.URLEncoding.EncodeToString([]byte("aUsername:aPassword123")),
	}

	claimsResponse := ClaimsResponse{AccessToken: "a.b.c"}
	responseJSON, _ := json.Marshal(claimsResponse)
	testError := errors.New("error expected by test")

	tests := []struct {
		Scenario            string
		TokenClaims         *models.TokenClaims
		ClaimNames          models.ClaimNamesMap
		Conf                *Config
		ClaimNamesError     error
		IAAuthenticateError error
		Expected            *models.AggregateClaims
		ExpectedError       error
	}{
		{
			Scenario:    "no claim names, should return empty aggregate claims",
			TokenClaims: defaultTokenClaims,
			ClaimNames:  models.ClaimNamesMap{},
			Expected: &models.AggregateClaims{
				ClaimNames:   make(map[string]string),
				ClaimSources: make(map[string]*models.ClaimSource),
			},
			ExpectedError: nil,
		},
		{
			Scenario:    "error authenticating with issuing authority, should return error",
			TokenClaims: defaultTokenClaims,
			Conf:        &Config{},
			ClaimNames: models.ClaimNamesMap{
				provider1URL: {"claim1"},
			},
			Expected:            nil,
			IAAuthenticateError: testError,
			ExpectedError:       testError,
		},
		{
			Scenario:    "claim names from one provider, should return token and claim name + source",
			TokenClaims: defaultTokenClaims,
			Conf:        &Config{},
			ClaimNames: models.ClaimNamesMap{
				provider1URL: {"claim1"},
			},
			Expected: &models.AggregateClaims{
				ClaimNames: map[string]string{
					"claim1": "provider1",
				},
				ClaimSources: map[string]*models.ClaimSource{
					"provider1": {JWT: claimsResponse.AccessToken},
				},
			},
			ExpectedError: nil,
		},
		{
			Scenario:    "claim names from same provider domain, should return unique provider source name",
			TokenClaims: defaultTokenClaims,
			Conf:        &Config{},
			ClaimNames: models.ClaimNamesMap{
				provider1URL + "/claims":    {"claim1", "claim2"},
				provider1URL + "/v2/claims": {"claim3"},
			},
			Expected: &models.AggregateClaims{
				ClaimNames: map[string]string{
					"claim1": "provider1",
					"claim2": "provider1",
					"claim3": "provider1-2",
				},
				ClaimSources: map[string]*models.ClaimSource{
					"provider1":   {JWT: claimsResponse.AccessToken},
					"provider1-2": {JWT: claimsResponse.AccessToken},
				},
			},
			ExpectedError: nil,
		},
		{
			Scenario:    "claim names from multiple providers, should return token and claim name + source for each provider",
			TokenClaims: defaultTokenClaims,
			Conf:        &Config{},
			ClaimNames: models.ClaimNamesMap{
				provider1URL: {"claim1"},
				provider2URL: {"claim2"},
			},
			Expected: &models.AggregateClaims{
				ClaimNames: map[string]string{
					"claim1": "provider1",
					"claim2": "provider2",
				},
				ClaimSources: map[string]*models.ClaimSource{
					"provider1": {JWT: claimsResponse.AccessToken},
					"provider2": {JWT: claimsResponse.AccessToken},
				},
			},
			ExpectedError: nil,
		},
		{
			Scenario:    "claims with claims provider with token, should return token and claim name + source",
			TokenClaims: defaultTokenClaims,
			ClaimNames: models.ClaimNamesMap{
				provider1URL: {"claim1"},
			},
			Conf: &Config{
				IssuingAuthorityConfigs: map[string]IssuingAuthorityConfig{
					provider1URL: {
						Credentials: defaultProviderCredentials[provider1URL],
					},
				},
			},
			Expected: &models.AggregateClaims{
				ClaimNames: map[string]string{
					"claim1": "provider1",
				},
				ClaimSources: map[string]*models.ClaimSource{
					"provider1": {JWT: claimsResponse.AccessToken},
				},
			},
			ExpectedError: nil,
		},
	}

	for _, test := range tests {
		claimNamesMock := contractMock.NewClaimNames(t)
		iaAuthenticatorMock := aggregationMock.NewIAAuthenticator(t)
		aggregateClaimsProvider := NewClaimsAggregator(claimNamesMock, test.Conf, iaAuthenticatorMock)
		httpClientMock := contractMock.NewHTTPClient(t)
		aggregateClaimsProvider.httpClient = httpClientMock

		t.Run(test.Scenario, func(t *testing.T) {
			claimNamesMock.EXPECT().
				ClaimNames(mock.Anything, test.TokenClaims).
				Return(test.ClaimNames, test.ClaimNamesError).
				Once()

			if test.ClaimNames != nil {
				for providerURL := range test.ClaimNames {
					url := providerURL
					httpClientMock.EXPECT().
						Do(mock.MatchedBy(func(req *http.Request) bool {
							return req.Method == http.MethodPost && req.URL.String() == url
						})).
						Return(&http.Response{Body: io.NopCloser(bytes.NewReader(responseJSON))}, nil).
						Maybe()

					iaAuthenticatorMock.EXPECT().
						Authenticate(mock.Anything, providerURL, mock.Anything).
						Return("a.b.c", test.IAAuthenticateError).
						Once()
				}
			}

			aggregatedClaims, err := aggregateClaimsProvider.Aggregate(context.Background(), test.TokenClaims)
			assert.ErrorIs(t, err, test.ExpectedError)
			assert.Equal(t, test.Expected, aggregatedClaims)
		})
	}
}

func TestMakeClaimsRequestBody(t *testing.T) {
	claimsRequest := &ClaimsRequest{
		UID:      "subject",
		Audience: []string{"audience"},
		Claims: map[string]ClaimsRequestPriority{
			"claim1": {Essential: true},
		},
	}
	claimsRequestJSON, _ := json.Marshal(claimsRequest)
	opaClaimsRequestJSON, _ := json.Marshal(models.OpaRequest[*ClaimsRequest]{
		Input: claimsRequest,
	})

	tests := []struct {
		Scenario      string
		ProviderURL   string
		ClaimNames    []string
		Expected      []byte
		ExpectedError error
	}{
		{
			Scenario:    "valid request to external provider",
			ProviderURL: "https://provider1.com",
			Expected:    claimsRequestJSON,
		},
		{
			Scenario:    "valid request to local opa provider",
			ProviderURL: "http://localhost:8181",
			Expected:    opaClaimsRequestJSON,
		},
	}

	for _, test := range tests {
		requestBody, err := makeClaimsRequestBody(test.ProviderURL, &models.TokenClaims{
			RegisteredClaims: jwt.RegisteredClaims{
				Subject:  "subject",
				Audience: []string{"audience"},
			},
		}, []string{"claim1"})

		assert.Equal(t, test.Expected, requestBody)
		assert.ErrorIs(t, err, test.ExpectedError)
	}
}

func TestMakeOpaClaimsResponse(t *testing.T) {
	tests := []struct {
		Scenario  string
		Body      []byte
		Expected  *ClaimsResponse
		ErrorFunc func(*testing.T, error)
	}{
		{
			Scenario: "valid response",
			Body:     []byte(`{"result": {"access_token": "a.b.c"}}`),
			Expected: &ClaimsResponse{
				AccessToken: "a.b.c",
			},
		},
		{
			Scenario: "invalid opa response",
			Body:     []byte(`{"invalid json"}`),
			Expected: nil,
			ErrorFunc: func(t *testing.T, err error) {
				var syntaxError *json.SyntaxError
				assert.ErrorAs(t, err, &syntaxError)
			},
		},
	}

	for _, test := range tests {
		t.Run(test.Scenario, func(t *testing.T) {
			claimsResponse, err := makeOpaClaimsResponse(test.Body)
			assert.Equal(t, test.Expected, claimsResponse)

			if test.ErrorFunc == nil {
				assert.Nil(t, err)
			} else {
				test.ErrorFunc(t, err)
			}
		})
	}
}

func TestMakeClaimsResponse(t *testing.T) {
	tests := []struct {
		Scenario  string
		Body      []byte
		Expected  *ClaimsResponse
		ErrorFunc func(*testing.T, error)
	}{
		{
			Scenario: "valid response",
			Body:     []byte(`{"access_token": "a.b.c"}`),
			Expected: &ClaimsResponse{
				AccessToken: "a.b.c",
			},
			ErrorFunc: nil,
		},
		{
			Scenario: "valid opa response",
			Body:     []byte(`{"result": {"access_token": "a.b.c"}}`),
			Expected: &ClaimsResponse{
				AccessToken: "a.b.c",
			},
			ErrorFunc: nil,
		},
		{
			Scenario: "invalid response",
			Body:     []byte(`{"invalid json"}`),
			Expected: nil,
			ErrorFunc: func(t *testing.T, err error) {
				var syntaxError *json.SyntaxError
				assert.ErrorAs(t, err, &syntaxError)
			},
		},
	}

	for _, test := range tests {
		t.Run(test.Scenario, func(t *testing.T) {
			claimsResponse, err := makeClaimsResponse(test.Body)
			assert.Equal(t, test.Expected, claimsResponse)

			if test.ErrorFunc == nil {
				assert.Nil(t, err)
			} else {
				test.ErrorFunc(t, err)
			}
		})
	}
}
