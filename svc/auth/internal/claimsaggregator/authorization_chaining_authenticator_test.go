package claimsaggregator

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"io"
	"net/http"
	"strings"
	"testing"

	"github.com/golang-jwt/jwt/v5"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/svc/auth/contract"
	contractMock "gitlab.com/n-id/core/svc/auth/contract/mock"
	"gitlab.com/n-id/core/svc/auth/models"
)

func TestAuthenticateAuthorizationChainingIAAuthenticator(t *testing.T) {
	defaultTokenClaims := models.TokenClaims{
		RegisteredClaims: jwt.RegisteredClaims{
			Subject:  "subject",
			Audience: []string{"audience"},
		},
		Scopes: []string{"scope"},
	}

	signTokenError := errors.New("sign error")
	doAuthRequestError := errors.New("do auth request error")

	providerURL := "https://provider.com"
	tests := []struct {
		name          string
		config        *Config
		tokenClaims   models.TokenClaims
		providerURL   string
		mockSetup     func(*contractMock.JWTClient, *contractMock.HTTPClient)
		expectedToken string
		expectedError error
	}{
		{
			name:        "successful authentication",
			config:      &Config{Issuer: "test-issuer", IssuingAuthorityConfigs: map[string]IssuingAuthorityConfig{"https://provider.com": {Credentials: "base64encodedcreds"}}},
			tokenClaims: defaultTokenClaims,
			providerURL: providerURL,
			mockSetup: func(jwtClient *contractMock.JWTClient, httpClient *contractMock.HTTPClient) {
				jwtClient.EXPECT().SignToken(mock.Anything).Return("", nil).Once()
				httpClient.EXPECT().Do(mock.Anything).Return(&http.Response{
					StatusCode: http.StatusOK,
					Body:       io.NopCloser(strings.NewReader(`{"access_token": "access.token", "token_type": "bearer"}`)),
				}, nil).Once()
			},
			expectedToken: "access.token",
			expectedError: nil,
		},
		{
			name:          "nil config",
			config:        nil,
			tokenClaims:   defaultTokenClaims,
			providerURL:   providerURL,
			expectedToken: "",
			expectedError: errNoConfig,
		},
		{
			name:        "sign token error",
			config:      &Config{Issuer: "test-issuer", IssuingAuthorityConfigs: map[string]IssuingAuthorityConfig{"https://provider.com": {Credentials: "base64encodedcreds"}}},
			tokenClaims: defaultTokenClaims,
			providerURL: providerURL,
			mockSetup: func(jwtClient *contractMock.JWTClient, _ *contractMock.HTTPClient) {
				jwtClient.EXPECT().SignToken(mock.Anything).Return("", signTokenError).Once()
			},
			expectedToken: "",
			expectedError: signTokenError,
		},
		{
			name:        "HTTP request error",
			config:      &Config{Issuer: "test-issuer", IssuingAuthorityConfigs: map[string]IssuingAuthorityConfig{"https://provider.com": {Credentials: "base64encodedcreds"}}},
			tokenClaims: defaultTokenClaims,
			providerURL: providerURL,
			mockSetup: func(jwtClient *contractMock.JWTClient, httpClient *contractMock.HTTPClient) {
				jwtClient.EXPECT().SignToken(mock.Anything).Return("signed.token", nil).Once()
				httpClient.EXPECT().Do(mock.Anything).Return(nil, doAuthRequestError).Once()
			},
			expectedToken: "",
			expectedError: doAuthRequestError,
		},
		{
			name:        "non-OK status code",
			config:      &Config{Issuer: "test-issuer", IssuingAuthorityConfigs: map[string]IssuingAuthorityConfig{"https://provider.com": {Credentials: "base64encodedcreds"}}},
			tokenClaims: defaultTokenClaims,
			providerURL: providerURL,
			mockSetup: func(jwtClient *contractMock.JWTClient, httpClient *contractMock.HTTPClient) {
				jwtClient.EXPECT().SignToken(mock.Anything).Return("signed.token", nil).Once()
				httpClient.EXPECT().Do(mock.Anything).Return(&http.Response{
					StatusCode: http.StatusBadRequest,
					Body:       io.NopCloser(strings.NewReader(`{"error": "bad request"}`)),
				}, nil).Once()
			},
			expectedToken: "",
			expectedError: errNonOkStatusCode,
		},
		{
			name:        "unauthorized status code",
			config:      &Config{Issuer: "test-issuer", IssuingAuthorityConfigs: map[string]IssuingAuthorityConfig{"https://provider.com": {Credentials: "base64encodedcreds"}}},
			tokenClaims: defaultTokenClaims,
			providerURL: providerURL,
			mockSetup: func(jwtClient *contractMock.JWTClient, httpClient *contractMock.HTTPClient) {
				jwtClient.EXPECT().SignToken(mock.Anything).Return("signed.token", nil).Once()
				httpClient.EXPECT().Do(mock.Anything).Return(&http.Response{
					StatusCode: http.StatusUnauthorized,
					Body:       io.NopCloser(strings.NewReader(`{"error": "unauthorized"}`)),
				}, nil).Once()
			},
			expectedToken: "",
			expectedError: contract.ErrUnauthorized,
		},
		{
			name: "scopes should be in access token request",
			config: &Config{
				Issuer: "test-issuer",
				IssuingAuthorityConfigs: map[string]IssuingAuthorityConfig{
					"https://provider.com": {Credentials: "base64encodedcreds"},
				},
			},
			tokenClaims: defaultTokenClaims,
			providerURL: providerURL,
			mockSetup: func(jwtClient *contractMock.JWTClient, httpClient *contractMock.HTTPClient) {
				jwtClient.EXPECT().SignToken(mock.Anything).Return("signed.token", nil).Once()
				httpClient.EXPECT().Do(mock.MatchedBy(func(req *http.Request) bool {
					bodyBytes, err := io.ReadAll(req.Body)
					if err != nil {
						return false
					}

					body := accessTokenRequest{}
					err = json.Unmarshal(bodyBytes, &body)
					if err != nil {
						return false
					}

					return body.Scope == "scope"
				})).Return(&http.Response{
					StatusCode: http.StatusOK,
					Body:       io.NopCloser(strings.NewReader(`{"access_token": "access.token", "token_type": "bearer"}`)),
				}, nil).Once()
			},
			expectedToken: "access.token",
			expectedError: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			jwtClientMock := contractMock.NewJWTClient(t)
			httpClientMock := contractMock.NewHTTPClient(t)

			if tt.mockSetup != nil {
				tt.mockSetup(jwtClientMock, httpClientMock)
			}

			authenticator := NewAuthorizationChainingIAAuthenticator(tt.config, jwtClientMock)
			authenticator.httpClient = httpClientMock

			token, err := authenticator.Authenticate(context.Background(), tt.providerURL, &tt.tokenClaims)

			if tt.expectedError != nil {
				assert.Error(t, err)
				assert.ErrorIs(t, err, tt.expectedError)
			} else {
				assert.NoError(t, err)
			}
			assert.Equal(t, tt.expectedToken, token)
		})
	}
}

func TestIssuingAuthorityConfig(t *testing.T) {
	tests := []struct {
		name          string
		config        *Config
		providerURL   string
		expectedAuth  *IssuingAuthorityConfig
		expectedError error
	}{
		{
			name: "valid provider credentials",
			config: &Config{IssuingAuthorityConfigs: map[string]IssuingAuthorityConfig{
				"https://provider.com": {Credentials: "base64encodedcreds"},
			}},
			providerURL:   "https://provider.com",
			expectedAuth:  &IssuingAuthorityConfig{Credentials: "base64encodedcreds"},
			expectedError: nil,
		},
		{
			name: "no issuing authority config",
			config: &Config{IssuingAuthorityConfigs: map[string]IssuingAuthorityConfig{
				"https://example.com": {Credentials: "base64encodedcreds"},
			}},
			providerURL:   "https://unknown-provider.com",
			expectedAuth:  nil,
			expectedError: errNoIssuingAuthorityConfig,
		},
		{
			name: "empty credentials",
			config: &Config{
				IssuingAuthorityConfigs: map[string]IssuingAuthorityConfig{
					"https://provider.com": {Credentials: ""},
				},
			},
			providerURL:   "https://provider.com",
			expectedAuth:  nil,
			expectedError: errEmptyCredentials,
		},
		{
			name: "custom token endpoint",
			config: &Config{
				IssuingAuthorityConfigs: map[string]IssuingAuthorityConfig{
					"https://provider.com": {
						Credentials:   "base64encodedcreds",
						TokenEndpoint: "/custom/token",
					},
				},
			},
			providerURL:   "https://provider.com",
			expectedAuth:  &IssuingAuthorityConfig{Credentials: "base64encodedcreds", TokenEndpoint: "/custom/token"},
			expectedError: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			authenticator := NewAuthorizationChainingIAAuthenticator(tt.config, nil)
			auth, err := authenticator.issuingAuthorityConfig(tt.providerURL)

			if tt.expectedError != nil {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), tt.expectedError.Error())
			} else {
				assert.NoError(t, err)
			}
			assert.Equal(t, tt.expectedAuth, auth)
		})
	}
}

func TestDoAuthenticationRequest(t *testing.T) {
	defaultAuth := base64.URLEncoding.EncodeToString([]byte("username:password"))

	tests := []struct {
		name           string
		providerURL    string
		body           interface{}
		config         *IssuingAuthorityConfig
		mockSetup      func(*contractMock.HTTPClient)
		expectedStatus int
		expectedError  error
	}{
		{
			name:        "successful request",
			providerURL: "https://provider.com",
			config:      &IssuingAuthorityConfig{Credentials: defaultAuth},
			body:        map[string]string{"key": "value"},
			mockSetup: func(httpClient *contractMock.HTTPClient) {
				httpClient.EXPECT().Do(mock.Anything).Return(&http.Response{
					StatusCode: http.StatusOK,
					Body:       io.NopCloser(strings.NewReader(`{"access_token": "access.token"}`)),
				}, nil)
			},
			expectedStatus: http.StatusOK,
			expectedError:  nil,
		},
		{
			name:        "non-OK status code",
			providerURL: "https://provider.com",
			config:      &IssuingAuthorityConfig{Credentials: defaultAuth},
			body:        map[string]string{"key": "value"},
			mockSetup: func(httpClient *contractMock.HTTPClient) {
				httpClient.EXPECT().Do(mock.Anything).Return(&http.Response{
					StatusCode: http.StatusBadRequest,
					Body:       io.NopCloser(strings.NewReader(`{"error": "bad request"}`)),
				}, nil)
			},
			expectedStatus: http.StatusBadRequest,
			expectedError:  errors.New("non-OK status code"),
		},
		{
			name:        "provider is opa",
			providerURL: "https://provider.com",
			config:      &IssuingAuthorityConfig{Credentials: defaultAuth, IsOPA: true},
			body:        map[string]string{"key": "value"},
			mockSetup: func(httpClient *contractMock.HTTPClient) {
				httpClient.EXPECT().Do(mock.MatchedBy(func(req *http.Request) bool {
					bodyBytes, err := io.ReadAll(req.Body)
					if err != nil {
						return false
					}

					return string(bodyBytes) == `{"input":{"key":"value"}}`
				})).Return(&http.Response{
					StatusCode: http.StatusOK,
					Body:       io.NopCloser(strings.NewReader(`{"access_token": "access.token"}`)),
				}, nil)
			},
			expectedStatus: http.StatusOK,
			expectedError:  nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			httpClientMock := contractMock.NewHTTPClient(t)
			tt.mockSetup(httpClientMock)

			authenticator := NewAuthorizationChainingIAAuthenticator(&Config{}, nil)
			authenticator.httpClient = httpClientMock

			resp, err := authenticator.doAuthenticationRequest(context.Background(), tt.providerURL, tt.config, tt.body)

			if tt.expectedError != nil {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), tt.expectedError.Error())
			} else {
				assert.NoError(t, err)
				defer resp.Body.Close()
				assert.Equal(t, tt.expectedStatus, resp.StatusCode)
			}
		})
	}
}

func TestGetAccessToken(t *testing.T) {
	tests := []struct {
		name          string
		responseBody  string
		expectedToken string
		expectedError error
	}{
		{
			name:          "valid response",
			responseBody:  `{"access_token": "access.token", "token_type": "bearer"}`,
			expectedToken: "access.token",
			expectedError: nil,
		},
		{
			name:          "invalid token type",
			responseBody:  `{"access_token": "access.token", "token_type": "invalid"}`,
			expectedToken: "",
			expectedError: errInvalidTokenType,
		},
		{
			name:          "invalid JSON",
			responseBody:  `{"invalid json"}`,
			expectedToken: "",
			expectedError: errors.New("unmarshalling response body"),
		},
		{
			name: "opa response",
			responseBody: `{
				"result": {
					"access_token": "access.token",
					"token_type": "bearer"
				}
			}`,
			expectedToken: "access.token",
			expectedError: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			authenticator := NewAuthorizationChainingIAAuthenticator(&Config{}, nil)
			response := &http.Response{
				Body: io.NopCloser(strings.NewReader(tt.responseBody)),
			}

			token, err := authenticator.getAccessToken(response)

			if tt.expectedError != nil {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), tt.expectedError.Error())
			} else {
				assert.NoError(t, err)
			}
			assert.Equal(t, tt.expectedToken, token)
		})
	}
}

func TestBuildTokenURL(t *testing.T) {
	tests := []struct {
		name        string
		providerURL string
		config      *IssuingAuthorityConfig
		expectedURL string
		expectError bool
	}{
		{
			name:        "valid URL with no custom token endpoint",
			providerURL: "https://example.com",
			config:      &IssuingAuthorityConfig{TokenEndpoint: ""},
			expectedURL: "https://example.com/token",
			expectError: false,
		},
		{
			name:        "valid URL with custom token endpoint",
			providerURL: "https://example.com",
			config:      &IssuingAuthorityConfig{TokenEndpoint: "/custom/token"},
			expectedURL: "https://example.com/custom/token",
			expectError: false,
		},
		{
			name:        "valid localhost URL with no custom token endpoint",
			providerURL: "http://localhost:8080",
			config:      &IssuingAuthorityConfig{TokenEndpoint: ""},
			expectedURL: "http://localhost:8080/token",
			expectError: false,
		},
		{
			name:        "valid localhost URL with custom token endpoint",
			providerURL: "http://localhost:8080",
			config:      &IssuingAuthorityConfig{TokenEndpoint: "/custom/token"},
			expectedURL: "http://localhost:8080/custom/token",
			expectError: false,
		},
		{
			name:        "invalid provider URL",
			providerURL: "invalid-url",
			config:      &IssuingAuthorityConfig{TokenEndpoint: ""},
			expectedURL: "",
			expectError: true,
		},
		{
			name:        "empty provider URL",
			providerURL: "",
			config:      &IssuingAuthorityConfig{TokenEndpoint: ""},
			expectedURL: "",
			expectError: true,
		},
		{
			name:        "invalid URL with missing scheme",
			providerURL: "example.com/path",
			config:      &IssuingAuthorityConfig{TokenEndpoint: ""},
			expectedURL: "",
			expectError: true,
		},
		{
			name:        "nil config",
			providerURL: "https://example.com",
			config:      nil,
			expectedURL: "https://example.com/token",
			expectError: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result, err := buildTokenURL(tt.providerURL, tt.config)

			if (err != nil) != tt.expectError {
				t.Errorf("expected error: %v, got: %v", tt.expectError, err)
			}

			if result != tt.expectedURL {
				t.Errorf("expected URL: %s, got: %s", tt.expectedURL, result)
			}
		})
	}
}
