package claimsaggregator

import (
	"context"
	"testing"

	"github.com/golang-jwt/jwt/v5"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	contractMock "gitlab.com/n-id/core/svc/auth/contract/mock"
	"gitlab.com/n-id/core/svc/auth/internal/opa"
	"gitlab.com/n-id/core/svc/auth/models"
)

func TestClaims(t *testing.T) {
	var uninitializedMap models.ClaimNamesMap
	claims := &models.TokenClaims{
		RegisteredClaims: jwt.RegisteredClaims{
			Subject:  "subject",
			Audience: []string{"audience"},
		},
	}

	tests := []struct {
		scenario    string
		expected    models.ClaimNamesMap
		opaResponse models.ClaimNamesMap
		error       error
	}{
		{
			scenario:    "no claim names",
			opaResponse: uninitializedMap,
			expected:    uninitializedMap,
			error:       nil,
		},
		{
			scenario: "claim name from one provider",
			opaResponse: models.ClaimNamesMap{
				"https://provider1.com": {"claim1"},
			},
			expected: models.ClaimNamesMap{
				"https://provider1.com": {"claim1"},
			},
			error: nil,
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			provider := NewOpaClaimNames(&opa.Config{})
			opaEval := contractMock.NewOpaEvaluator[models.ClaimNamesMap](t)
			provider.opaEval = opaEval

			opaEval.EXPECT().
				Eval(mock.Anything, mock.Anything).
				Return(&models.OpaResponse[models.ClaimNamesMap]{Result: test.opaResponse}, nil).
				Once()

			claims, err := provider.ClaimNames(context.Background(), claims)
			assert.Equal(t, test.expected, claims)
			assert.Equal(t, test.error, err)
		})
	}
}
