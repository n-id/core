package claimsaggregator

import (
	"errors"
)

var (
	// ErrOpaURINotSetOpaClaimProvider indicates an error when the OPA URI is not set for the claims provider.
	ErrOpaURINotSetOpaClaimProvider = errors.New("OPA URI not set for claims provider")
	// ErrOpaPathNotSetOpaClaimProvider indicates an error when the OPA path is not set for the claims provider.
	ErrOpaPathNotSetOpaClaimProvider = errors.New("OPA path not set for claims provider")
)

// OpaClaimNamesConfig contains the configuration for the OPA ClaimNames.
type OpaClaimNamesConfig struct {
	// Path is the path to the OPA policy.
	Path string

	// URL is the URL to the OPA server.
	URL string
}

// Validate validates the OpaClaimsSetProviderConfig.
func (c *OpaClaimNamesConfig) Validate() error {
	if c.Path == "" {
		return ErrOpaPathNotSetOpaClaimProvider
	}

	if c.URL == "" {
		return ErrOpaURINotSetOpaClaimProvider
	}

	return nil
}
