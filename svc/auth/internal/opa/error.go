package opa

import (
	"strings"
)

// ValidationError represents an error in the scope validation.
type ValidationError struct {
	Message string `json:"message"`
}

// FmtErrors formats the opa validation errors to a golang error string.
func FmtErrors(errors []ValidationError) string {
	var errorMsg strings.Builder

	for _, err := range errors {
		errorMsg.WriteString(err.Message)
		errorMsg.WriteString(", ")
	}

	return strings.TrimSuffix(errorMsg.String(), ", ")
}
