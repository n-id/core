package opa

import (
	"strings"

	"gitlab.com/n-id/core/pkg/utilities/errors"
)

var (
	// ErrInvalidOpaConfig indicates an error when the OpaConfig is invalid.
	ErrInvalidOpaConfig = errors.New("invalid OpaConfig")
)

// Config represents the configuration for Opa.
type Config struct {
	// Path is the path to the OPA policy.
	Path string

	// URL is the URL to the OPA server.
	URL string
}

// Validate validates the OpaConfig.
func (c *Config) Validate() error {
	if c.Path == "" {
		return errors.Wrap(ErrInvalidOpaConfig, "path is empty")
	}
	if c.URL == "" {
		return errors.Wrap(ErrInvalidOpaConfig, "URL is empty")
	}

	if strings.HasSuffix(c.URL, "/data") {
		return errors.Wrap(ErrInvalidOpaConfig, "URL should only contain the base URL, not the /data path")
	}

	return nil
}
