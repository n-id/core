// Package opa provides the implementation of the OPA evaluator.
package opa

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/models"
)

// Eval is an implementation of OpaEvaluator that uses OPA to evaluate input.
type Eval[T any] struct {
	conf       *Config
	httpClient contract.HTTPClient
}

// NewOpaEval creates a new instance of OpaEval.
func NewOpaEval[T any](conf *Config) *Eval[T] {
	return &Eval[T]{
		conf:       conf,
		httpClient: http.DefaultClient,
	}
}

// Eval evaluates the input against a OPA server according to opa configuration.
func (o *Eval[T]) Eval(ctx context.Context, input any) (*models.OpaResponse[T], error) {
	providerURL := fmt.Sprintf("%s/v1/data/%s", o.conf.URL, o.conf.Path)
	claimsJSON, err := json.Marshal(models.OpaRequest[interface{}]{Input: input})
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal claims")
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, providerURL, bytes.NewBuffer(claimsJSON))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create request")
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := o.httpClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get claims from OPA")
	}
	var deferError error
	defer func() {
		deferError = resp.Body.Close()
	}()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read response body from OPA")
	}

	out := new(models.OpaResponse[T])
	err = json.Unmarshal(body, out)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal claims from OPA")
	}

	return out, deferError
}
