package opa

import (
	"context"
	"errors"
	"io"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	contractMock "gitlab.com/n-id/core/svc/auth/contract/mock"
	"gitlab.com/n-id/core/svc/auth/models"
)

var errTest = errors.New("test error")

func TestEval(t *testing.T) {
	tests := []struct {
		scenario         string
		responseBody     string
		responseError    error
		input            []string
		expectedResponse models.OpaResponse[[]string]
		expectedError    error
	}{
		{
			scenario:     "no error response",
			responseBody: `{"result":["value"]}`,
			expectedResponse: models.OpaResponse[[]string]{
				Result: []string{"value"},
			},
		},
		{
			scenario:         "response body error",
			responseBody:     `{"result":{}}`,
			responseError:    errTest,
			expectedResponse: models.OpaResponse[[]string]{},
			expectedError:    errTest,
		},
	}

	opaEval := NewOpaEval[[]string](&Config{
		Path: "path",
		URL:  "http://localhost:8181",
	})
	ctx := context.Background()

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			httpClient := contractMock.NewHTTPClient(t)
			opaEval.httpClient = httpClient

			httpClient.EXPECT().
				Do(mock.Anything).
				Return(&http.Response{Body: io.NopCloser(strings.NewReader(test.responseBody))}, test.responseError).
				Once()

			decision, err := opaEval.Eval(ctx, test.input)
			if err != nil {
				assert.ErrorContains(t, err, test.expectedError.Error())
			} else {
				assert.Equal(t, test.expectedResponse, *decision)
			}
		})
	}
}
