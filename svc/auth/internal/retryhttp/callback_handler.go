// Package retryhttp contains the http implementation of the callback handler.
package retryhttp

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/n-id/core/pkg/interceptor/xrequestid"
	"gitlab.com/n-id/core/svc/auth/contract"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/propagation"

	"github.com/hashicorp/go-retryablehttp"
	"gitlab.com/n-id/core/pkg/utilities/errors"
)

const (
	authCodeParamKey  = "authorization_code"
	successStatusCode = http.StatusAccepted
	retryMaxWait      = 4 * time.Hour
	requestTimeout    = 15 * time.Second
)

// CallbackHandler implements te callback handler with a http client.
type CallbackHandler struct {
	Client contract.HTTPClient

	propagators propagation.TextMapPropagator
}

// NewCallbackHandler creates a new callback handler with a http retry client.
func NewCallbackHandler(retryMax int) *CallbackHandler {
	retryClient := retryablehttp.NewClient()
	retryClient.CheckRetry = checkRetry
	retryClient.RetryMax = retryMax
	retryClient.RetryWaitMax = retryMaxWait

	client := retryClient.StandardClient()
	client.Timeout = requestTimeout

	return &CallbackHandler{
		Client:      client,
		propagators: otel.GetTextMapPropagator(),
	}
}

// HandleCallback handles a callback request with a GET request.
func (c *CallbackHandler) HandleCallback(ctx context.Context, location string, authorizationCode string) error {
	req, err := http.NewRequestWithContext(ctx, "GET", location, nil)
	if err != nil {
		return errors.Wrapf(err, "creating request: %s", location)
	}

	c.propagators.Inject(ctx, propagation.HeaderCarrier(req.Header))
	if xRequestID, ok := ctx.Value(string(xrequestid.XRequestID)).(string); ok {
		req.Header.Set(string(xrequestid.XRequestID), xRequestID)
	}

	q := req.URL.Query()
	q.Add(authCodeParamKey, authorizationCode)
	req.URL.RawQuery = q.Encode()

	resp, err := c.Client.Do(req)
	if err != nil {
		return errors.Wrapf(err, "executing request for url %s", location)
	}

	if resp.StatusCode != successStatusCode {
		return errors.Errorf("request returned error status code: %d", resp.StatusCode)
	}

	err = resp.Body.Close()
	if err != nil {
		return errors.Wrap(err, "closing response body")
	}

	return nil
}

func checkRetry(ctx context.Context, resp *http.Response, _ error) (bool, error) {
	if ctx.Err() != nil {
		return false, ctx.Err()
	}

	if resp == nil {
		return false, contract.ErrCallbackNoResponseGiven
	}

	return resp.StatusCode != http.StatusAccepted, nil
}
