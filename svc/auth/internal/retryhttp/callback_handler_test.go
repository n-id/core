package retryhttp

import (
	"context"
	"errors"
	"io"
	"net/http"
	"testing"

	"go.opentelemetry.io/contrib/propagators/b3"
	"go.opentelemetry.io/otel"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	contractMock "gitlab.com/n-id/core/svc/auth/contract/mock"
)

const testDomain = "url.com"

var errTest = errors.New("http error")

type nopCloser struct {
	io.Reader
}

func (nopCloser) Close() error { return nil }
func (nopCloser) Read(_ []byte) (n int, err error) {
	return 0, io.EOF
}

type CallbackHandlerTestSuite struct {
	suite.Suite
}

func (s *CallbackHandlerTestSuite) TestHandleCallbackInvalidUrl() {
	httpClient := contractMock.NewHTTPClient(s.T())
	httpClient.EXPECT().Do(mock.Anything).Maybe()

	callbackHandler := &CallbackHandler{
		Client:      contractMock.NewHTTPClient(s.T()),
		propagators: otel.GetTextMapPropagator(),
	}

	err := callbackHandler.HandleCallback(context.Background(), "@#$%^&*(", "authcode===")

	s.Error(err)
}

func (s *CallbackHandlerTestSuite) TestHandleCallbackDoError() {
	httpClient := contractMock.NewHTTPClient(s.T())
	callbackHandler := &CallbackHandler{
		Client:      httpClient,
		propagators: otel.GetTextMapPropagator(),
	}

	httpClient.EXPECT().
		Do(mock.MatchedBy(func(req *http.Request) bool {
			return req.Host == testDomain
		})).
		Return(&http.Response{}, errTest).
		Once()

	err := callbackHandler.HandleCallback(context.Background(), "http://url.com", "authcode===")

	s.Equal("executing request for url http://url.com: http error", err.Error())
}

func (s *CallbackHandlerTestSuite) TestHandleCallbackErrorStatusCode() {
	httpClient := contractMock.NewHTTPClient(s.T())

	callbackHandler := &CallbackHandler{
		Client:      httpClient,
		propagators: otel.GetTextMapPropagator(),
	}
	httpClient.EXPECT().
		Do(mock.MatchedBy(func(req *http.Request) bool {
			return req.Host == testDomain
		})).
		Return(&http.Response{StatusCode: http.StatusNotFound}, nil).
		Once()

	err := callbackHandler.HandleCallback(context.Background(), "http://url.com", "authcode===")

	s.Equal("request returned error status code: 404", err.Error())
}

func (s *CallbackHandlerTestSuite) TestHandleCallback() {
	httpClient := contractMock.NewHTTPClient(s.T())

	callbackHandler := &CallbackHandler{
		Client:      httpClient,
		propagators: otel.GetTextMapPropagator(),
	}

	httpClient.EXPECT().
		Do(mock.MatchedBy(func(req *http.Request) bool {
			return req.Host == testDomain
		})).
		Return(&http.Response{Body: &nopCloser{}, StatusCode: http.StatusAccepted}, nil).
		Once()

	err := callbackHandler.HandleCallback(context.Background(), "http://url.com", "authcode===")

	s.NoError(err)
}

func TestCallbackHandlerTestSuite(t *testing.T) {
	p := b3.New(b3.WithInjectEncoding(b3.B3MultipleHeader))
	otel.SetTextMapPropagator(p)

	suite.Run(t, &CallbackHandlerTestSuite{})
}
