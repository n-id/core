// Package claimsvalidator provides implementations of ClaimsValidator.
package claimsvalidator

import (
	"context"

	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/internal/opa"
	"gitlab.com/n-id/core/svc/auth/models"
)

// OpaClaimsValidator is an implementation of ClaimsValidator that uses OPA to validate tokens.
type OpaClaimsValidator struct {
	conf    *opa.Config
	opaEval contract.OpaEvaluator[OpaClaimsValidationDecision]
}

// NewOpaClaimsValidator creates a new instance of OpaClaimsValidator.
func NewOpaClaimsValidator(conf *opa.Config) *OpaClaimsValidator {
	return &OpaClaimsValidator{
		conf:    conf,
		opaEval: opa.NewOpaEval[OpaClaimsValidationDecision](conf),
	}
}

// OpaClaimsValidationDecision represents the decision made by OPA for the scope validation.
type OpaClaimsValidationDecision struct {
	Errors []opa.ValidationError `json:"errors"`
}

// Validate checks if the claims are valid.
func (v *OpaClaimsValidator) Validate(ctx context.Context, claims models.TokenClaims) error {
	decision, err := v.opaEval.Eval(ctx, claims)
	if err != nil {
		return err
	}

	if len(decision.Result.Errors) != 0 {
		return errors.Wrap(contract.ErrInvalidArguments, opa.FmtErrors(decision.Result.Errors))
	}

	return nil
}
