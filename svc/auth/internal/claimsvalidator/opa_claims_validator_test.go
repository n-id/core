package claimsvalidator

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/n-id/core/svc/auth/contract"
	contractMock "gitlab.com/n-id/core/svc/auth/contract/mock"
	"gitlab.com/n-id/core/svc/auth/internal/opa"
	"gitlab.com/n-id/core/svc/auth/models"
)

func TestOpaClaimsValidator_Validate(t *testing.T) {
	tests := []struct {
		scenario        string
		claims          models.TokenClaims
		opaResponse     OpaClaimsValidationDecision
		expectedError   error
		expectedAllowed bool
	}{
		{
			scenario:        "no error response",
			claims:          models.TokenClaims{Scopes: []string{"scope1", "scope2"}, ClientID: "client-id"},
			opaResponse:     OpaClaimsValidationDecision{},
			expectedError:   nil,
			expectedAllowed: true,
		},
		{
			scenario:        "empty error response",
			claims:          models.TokenClaims{Scopes: []string{"scope1", "scope2"}, ClientID: "client-id"},
			opaResponse:     OpaClaimsValidationDecision{Errors: []opa.ValidationError{}},
			expectedError:   nil,
			expectedAllowed: true,
		},
		{
			scenario:        "errors response",
			claims:          models.TokenClaims{Scopes: []string{"scope1", "invalid-scope"}},
			opaResponse:     OpaClaimsValidationDecision{Errors: []opa.ValidationError{{Message: "some error"}}},
			expectedError:   contract.ErrInvalidArguments,
			expectedAllowed: false,
		},
	}

	for _, test := range tests {
		validator := NewOpaClaimsValidator(&opa.Config{})
		opaEval := contractMock.NewOpaEvaluator[OpaClaimsValidationDecision](t)
		validator.opaEval = opaEval

		opaEval.EXPECT().
			Eval(mock.Anything, mock.Anything).
			Return(&models.OpaResponse[OpaClaimsValidationDecision]{Result: test.opaResponse}, nil).
			Once()

		t.Run(test.scenario, func(t *testing.T) {
			err := validator.Validate(context.Background(), test.claims)

			assert.ErrorIs(t, err, test.expectedError)
		})
	}
}
