// Package config handles the configuration which is needed for the auth service.
package config

import (
	"fmt"
	"time"

	"github.com/vrischmann/envconfig"
	"gitlab.com/n-id/core/pkg/environment"
	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/pkg/utilities/log/v2"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/internal/claimsaggregator"
	"gitlab.com/n-id/core/svc/auth/internal/identityprovider/config"
	"gitlab.com/n-id/core/svc/auth/internal/opa"
	"gitlab.com/n-id/core/svc/auth/transport/grpc"
	"gitlab.com/n-id/core/svc/auth/transport/http"
)

// AuthConfig implements the used environment variables
type AuthConfig struct {
	environment.BaseConfig
	ServerHost          string `envconfig:"optional,SERVER_HOST"`
	ClusterHost         string `envconfig:"CLUSTER_HOST"`
	JWTPath             string `envconfig:"JWT_PATH_DIRECTORY"`
	JWKSURI             string `envconfig:"JWKS_URI"`
	Issuer              string `envconfig:"ISSUER"`
	AuthRequestURI      string `envconfig:"AUTH_REQUEST_URI"`
	PseudonymizationURI string `envconfig:"PSEUDONYMIZATION_URI"`
	// The OAuth 2.0 spec recommends a maximum lifetime of 10 minutes,
	// but in practice, most services set the expiration much shorter,
	// around 30-60 seconds
	AuthorizationCodeExpirationTime time.Duration `envconfig:"AUTHORIZATION_CODE_EXPIRATION_TIME"`
	// The authorization code itself can be of any length,
	// but the length of the codes should be documented.
	AuthorizationCodeLength   int    `envconfig:"AUTHORIZATION_CODE_LENGTH"`
	JWTExpirationHours        int    `envconfig:"default=24,JWT_EXPIRE_HOURS"`
	JWTRefreshExpirationHours int    `envconfig:"default=168,JWT_REFRESH_EXPIRE_HOURS"`
	WalletURI                 string `envconfig:"WALLET_URI,optional"`
	TestingClientPassword     string `envconfig:"TESTING_CLIENT_PASSWORD"`
	PilotClientPassword       string `envconfig:"PILOT_CLIENT_PASSWORD"`
	CallbackMaxRetryAttempts  int    `envconfig:"default=10,CALLBACK_MAX_RETRY_ATTEMPTS"`
	AllowMultipleAudiences    bool   `envconfig:"default=false,ALLOW_MULTIPLE_AUDIENCES"`
	ClaimsAggregationEnabled  bool   `envconfig:"default=true,CLAIMS_AGGREGATION_ENABLED"`
	DelegatedIdentityEnabled  bool   `envconfig:"default=true,DELEGATED_IDENTITY_ENABLED"`

	// AudienceProvider configs.
	AudienceProvider                    contract.AudienceProviderType
	MarshalSingleAudienceOrScopeAsArray bool

	// IdentityProvider configs.
	IdentityProvider            contract.IdentityProviderType
	CertificateIdentityProvider config.CertificateIdentityProviderConfig `envconfig:"optional"`

	// ScopeValidator configs.
	ScopeValidator    contract.ScopeValidatorType `envconfig:"optional"`
	OpaScopeValidator opa.Config                  `envconfig:"optional"`

	// ClaimsValidator configs.
	ClaimsValidationEnabled bool       `envconfig:"default=false,CLAIMS_VALIDATION_ENABLED"`
	ClaimsValidation        opa.Config `envconfig:"optional"`

	// ClaimsAggregator configs.
	ClaimsAggregation claimsaggregator.Config `envconfig:"optional"`
	ClaimNames        opa.Config              `envconfig:"optional"`

	Transport TransportConfig
}

// TransportConfig contains the configuration for the transport layer.
type TransportConfig struct {
	HTTP http.Config
	Grpc grpc.Config
}

// New takes its 'input' from environment variables and returns everything the microservice
// needs to serve requests.
func New() (*AuthConfig, error) {
	var conf AuthConfig
	if err := envconfig.Init(&conf); err != nil {
		log.WithError(err).Fatal("unable to load environment config")
	}

	conf.FillCertificateHeaders()

	if err := conf.Validate(); err != nil {
		return nil, errors.Wrap(err, "validating the configuration")
	}

	if err := conf.Loaders(); err != nil {
		return nil, errors.Wrap(err, "loading data from configuration loaders")
	}

	return &conf, nil
}

// Validate validates the AuthConfig.
func (c *AuthConfig) Validate() error {
	if c.ServerHost == "" {
		log.Warnf("server host is empty, will set it to localhost:%s", c.Transport.HTTP.Port)
		c.ServerHost = fmt.Sprintf("localhost:%s", c.Transport.HTTP.Port)
	}

	if err := c.AudienceProvider.Validate(); err != nil {
		return err
	}

	if err := c.IdentityProvider.Validate(); err != nil {
		return err
	}

	if c.IdentityProvider == contract.IdentityProviderTypeCertificate {
		if err := c.CertificateIdentityProvider.Validate(); err != nil {
			return err
		}
	}

	switch c.ScopeValidator {
	case contract.ScopeValidatorTypeOpa:
		if err := c.OpaScopeValidator.Validate(); err != nil {
			return err
		}
	case contract.ScopeValidatorTypeNone, contract.ScopeValidatorTypeDatabase:
		// No validation needed.
	default:
		return contract.ErrInvalidScopeValidator
	}

	if c.ClaimsAggregationEnabled {
		if err := c.ClaimNames.Validate(); err != nil {
			return err
		}
	}

	return nil
}

// FillCertificateHeaders fills the certificate header for the AuthConfig if the IdentityProvider is of type certificate.
// It sets the certificate header for both HTTP and gRPC transport if they are empty.
// Returns the updated AuthConfig.
func (c *AuthConfig) FillCertificateHeaders() *AuthConfig {
	if c.IdentityProvider != contract.IdentityProviderTypeCertificate {
		return c
	}

	if c.Transport.HTTP.CertificateHeader == "" {
		c.Transport.HTTP.CertificateHeader = c.CertificateIdentityProvider.Header
	}

	if c.Transport.Grpc.CertificateHeader == "" {
		c.Transport.Grpc.CertificateHeader = c.CertificateIdentityProvider.Header
	}

	return c
}

// Loaders executes all load functions, these functions load configuration from other sources then environment variables.
// Loaders should be executed with validated environment variables.
func (c *AuthConfig) Loaders() error {
	if c.ClaimsAggregationEnabled {
		if err := c.ClaimsAggregation.Load(); err != nil {
			return err
		}
	}

	return nil
}
