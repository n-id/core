package config

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/internal/identityprovider/config"
	"gitlab.com/n-id/core/svc/auth/transport/grpc"
	"gitlab.com/n-id/core/svc/auth/transport/http"
)

func TestAuthConfig_FillTransportCertificateHeader(t *testing.T) {
	tests := []struct {
		scenario                          string
		identityProvider                  contract.IdentityProviderType
		certificateIdentityProviderHeader string
		httpCertificateHeader             string
		grpcCertificateHeader             string
		httpExpectedCertificateHeader     string
		grpcExpectedCertificateHeader     string
	}{
		{
			scenario:                          "identity provider not certificate",
			identityProvider:                  contract.IdentityProviderTypeDatabase,
			certificateIdentityProviderHeader: "certificate-header",
			httpCertificateHeader:             "",
			grpcCertificateHeader:             "",
			httpExpectedCertificateHeader:     "",
			grpcExpectedCertificateHeader:     "",
		},
		{
			scenario:                          "identity provider certificate, transport certificate header not set",
			identityProvider:                  contract.IdentityProviderTypeCertificate,
			certificateIdentityProviderHeader: "certificate-header",
			httpCertificateHeader:             "",
			grpcCertificateHeader:             "",
			httpExpectedCertificateHeader:     "certificate-header",
			grpcExpectedCertificateHeader:     "certificate-header",
		},
		{
			scenario:                          "identity provider certificate, http certificate header set",
			identityProvider:                  contract.IdentityProviderTypeCertificate,
			certificateIdentityProviderHeader: "certificate-header",
			httpCertificateHeader:             "http-certificate-header",
			grpcCertificateHeader:             "",
			httpExpectedCertificateHeader:     "http-certificate-header",
			grpcExpectedCertificateHeader:     "certificate-header",
		},
		{
			scenario:                          "identity provider certificate, grpc certificate header set",
			identityProvider:                  contract.IdentityProviderTypeCertificate,
			certificateIdentityProviderHeader: "certificate-header",
			httpCertificateHeader:             "",
			grpcCertificateHeader:             "grpc-certificate-header",
			httpExpectedCertificateHeader:     "certificate-header",
			grpcExpectedCertificateHeader:     "grpc-certificate-header",
		},
		{
			scenario:                          "identity provider certificate, http and grpc certificate header set",
			identityProvider:                  contract.IdentityProviderTypeCertificate,
			certificateIdentityProviderHeader: "certificate-header",
			httpCertificateHeader:             "http-certificate-header",
			grpcCertificateHeader:             "grpc-certificate-header",
			httpExpectedCertificateHeader:     "http-certificate-header",
			grpcExpectedCertificateHeader:     "grpc-certificate-header",
		},
	}
	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			c := &AuthConfig{
				IdentityProvider:            test.identityProvider,
				CertificateIdentityProvider: config.CertificateIdentityProviderConfig{Header: test.certificateIdentityProviderHeader},
				Transport: TransportConfig{
					HTTP: http.Config{CertificateHeader: test.httpCertificateHeader},
					Grpc: grpc.Config{CertificateHeader: test.grpcCertificateHeader},
				},
			}

			c.FillCertificateHeaders()

			assert.Equal(t, test.httpExpectedCertificateHeader, c.Transport.HTTP.CertificateHeader)
			assert.Equal(t, test.grpcExpectedCertificateHeader, c.Transport.Grpc.CertificateHeader)
		})
	}
}
