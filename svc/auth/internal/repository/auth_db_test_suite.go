package repository

import (
	"github.com/stretchr/testify/suite"
	"gitlab.com/n-id/core/pkg/utilities/database/v2"
	"gitlab.com/n-id/core/svc/auth/models"
	"gorm.io/gorm"
)

// AuthDBSuite is the test-suite for the auth database.
type AuthDBSuite struct {
	suite.Suite

	DB   *gorm.DB
	Repo *AuthDB
	Tx   *gorm.DB
}

// SetupSuite is ran before the first in the test-suite.
func (s *AuthDBSuite) SetupSuite() {
	err := database.CreateTestDatabase("auth")
	if err != nil {
		s.T().Fatal(err)
	}

	s.DB = database.MustConnectTest("auth", nil)
}

// SetupTest is ran before each test in the test-suite.
func (s *AuthDBSuite) SetupTest() {
	s.Tx = s.DB.Begin()
	s.Require().NoError(s.Tx.AutoMigrate(models.GetModels()...))

	s.Repo = NewAuthDB(s.Tx)
}

// TearDownTest is ran after each test in the test-suite.
func (s *AuthDBSuite) TearDownTest() {
	s.Tx.Rollback()
}

// TearDownSuite is ran after the last test in the test-suite.
func (s *AuthDBSuite) TearDownSuite() {
	db, err := s.DB.DB()
	s.Require().NoError(err)
	s.NoError(db.Close())
}
