package audienceprovider

import (
	"context"

	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/models"
)

// RequestAudienceProvider provides the audience of the request body.
type RequestAudienceProvider struct{}

// GetAudience returns the audience of the request body.
func (r *RequestAudienceProvider) GetAudience(_ context.Context, req *models.TokenClientFlowRequest, _ interface{}) ([]string, error) {
	if req.Audience == "" {
		return nil, errors.Wrapf(contract.ErrInvalidArguments, "audience is required")
	}

	return []string{req.Audience}, nil
}
