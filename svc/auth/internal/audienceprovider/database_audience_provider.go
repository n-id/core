// Package audienceprovider provides an implementations of AudienceProvider, which provides
package audienceprovider

import (
	"context"

	"gitlab.com/n-id/core/pkg/sliceutil"
	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/internal/config"
	"gitlab.com/n-id/core/svc/auth/models"
)

// DatabaseAudienceProvider provides the audience of the provided scopes in the database.
type DatabaseAudienceProvider struct {
	conf *config.AuthConfig
}

// NewDatabaseAudienceProvider creates a new DatabaseAudienceProvider.
func NewDatabaseAudienceProvider(conf *config.AuthConfig) *DatabaseAudienceProvider {
	return &DatabaseAudienceProvider{
		conf: conf,
	}
}

// GetAudience returns the audience for the provided scopes in the database models.
func (r *DatabaseAudienceProvider) GetAudience(_ context.Context, req *models.TokenClientFlowRequest, scopes interface{}) ([]string, error) {
	// If scopes is not of type []*models.Scope, return an error.
	_scopes, ok := scopes.([]*models.Scope)
	if !ok {
		return nil, errors.Wrap(contract.ErrInvalidArguments, "Scopes is not of type []*models.Scope, is DatabaseScopeValidation enabled?")
	}

	var audiences []string

	// Get the audiences from the scopes.
	for _, scope := range _scopes {
		for _, audience := range scope.Audiences {
			audiences = append(audiences, audience.Audience)
		}
	}

	audiences = sliceutil.RemoveDuplicates(audiences)

	// Use the audience in the body if it is correct.
	if sliceutil.Contains(audiences, req.Audience) {
		audiences = []string{req.Audience}
	} else if len(audiences) > 1 && !r.conf.AllowMultipleAudiences {
		return nil, errors.Wrap(contract.ErrInvalidArguments, "Multiple audiences are not allowed, provide the audience in the request body")
	}

	return audiences, nil
}
