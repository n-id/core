package scopevalidator

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/n-id/core/svc/auth/contract"
	contractMock "gitlab.com/n-id/core/svc/auth/contract/mock"
	"gitlab.com/n-id/core/svc/auth/internal/opa"
	"gitlab.com/n-id/core/svc/auth/models"
)

func TestOpaScopeValidator_Validate(t *testing.T) {
	tests := []struct {
		scenario       string
		claims         models.TokenClaims
		opaResponse    OpaScopeValidationDecision
		expectedError  error
		expectedScopes []string
	}{
		{
			scenario:       "invalid request",
			claims:         models.TokenClaims{Scopes: []string{"scope1", "scope2"}},
			opaResponse:    OpaScopeValidationDecision{Allowed: false, ValidScopes: []string{"scope1", "scope2"}},
			expectedError:  contract.ErrInvalidArguments,
			expectedScopes: nil,
		},
		{
			scenario:       "allowed, all valid scopes",
			claims:         models.TokenClaims{Scopes: []string{"scope1", "scope2"}},
			opaResponse:    OpaScopeValidationDecision{Allowed: true, ValidScopes: []string{"scope1", "scope2"}},
			expectedError:  nil,
			expectedScopes: []string{"scope1", "scope2"},
		},
		{
			scenario:       "allowed, some valid scopes",
			claims:         models.TokenClaims{Scopes: []string{"scope1", "scope2"}},
			opaResponse:    OpaScopeValidationDecision{Allowed: true, ValidScopes: []string{"scope1"}},
			expectedError:  nil,
			expectedScopes: []string{"scope1"},
		},
		{
			scenario:       "allowed, no valid scopes",
			claims:         models.TokenClaims{Scopes: []string{"scope1", "scope2"}},
			opaResponse:    OpaScopeValidationDecision{Allowed: true, ValidScopes: []string{}},
			expectedError:  nil,
			expectedScopes: []string{},
		},
		{
			scenario:       "not allowed, some valid scopes",
			claims:         models.TokenClaims{Scopes: []string{"scope1", "scope2"}},
			opaResponse:    OpaScopeValidationDecision{Allowed: false, ValidScopes: []string{"scope1"}},
			expectedError:  contract.ErrInvalidArguments,
			expectedScopes: nil,
		},
		{
			scenario:       "errors response",
			claims:         models.TokenClaims{Scopes: []string{"scope1", "invalid-scope"}},
			opaResponse:    OpaScopeValidationDecision{Errors: []opa.ValidationError{{Message: "some error"}}},
			expectedError:  contract.ErrInvalidArguments,
			expectedScopes: nil,
		},
	}

	for _, test := range tests {
		validator := NewOpaScopeValidator(&opa.Config{})
		opaEval := contractMock.NewOpaEvaluator[OpaScopeValidationDecision](t)
		validator.opaEval = opaEval

		opaEval.EXPECT().
			Eval(mock.Anything, mock.Anything).
			Return(&models.OpaResponse[OpaScopeValidationDecision]{Result: test.opaResponse}, nil).
			Once()

		t.Run(test.scenario, func(t *testing.T) {
			validScopes, err := validator.Validate(context.Background(), test.claims)

			assert.Equal(t, test.expectedScopes, validScopes)
			assert.ErrorIs(t, err, test.expectedError)
		})
	}
}
