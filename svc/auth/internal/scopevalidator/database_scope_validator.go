// Package scopevalidator contains scope validators for token generation.
package scopevalidator

import (
	"context"

	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/models"
)

// DatabaseScopeValidator validates the scope of a database.
type DatabaseScopeValidator struct {
	repo contract.ScopeRepository
}

// NewDatabaseScopeValidator returns a new instance of DatabaseScopeValidator.
func NewDatabaseScopeValidator(repo contract.ScopeRepository) *DatabaseScopeValidator {
	return &DatabaseScopeValidator{
		repo: repo,
	}
}

// Validate validates the scopes against the database, all scopes provided must be found in the database.
func (v *DatabaseScopeValidator) Validate(ctx context.Context, claims models.TokenClaims) ([]string, error) {
	scopes, ok := claims.Scopes.([]string)
	if !ok {
		return nil, errors.Wrap(contract.ErrInvalidArguments, "")
	}

	// When scopes are validated against the database, the scopes are linked to audiences.
	validatedScopes, err := v.repo.ListAllMatching(ctx, scopes)
	if err != nil {
		return nil, errors.Wrapf(contract.ErrInvalidArguments, "validating scopes in client flow")
	}

	// Check if incorrect scopes are given.
	if len(scopes) != len(validatedScopes) && len(scopes) != 0 {
		return nil, errors.Wrapf(contract.ErrInvalidArguments, "invalid scope")
	}

	validScopes := []string{}
	for _, scope := range validatedScopes {
		validScopes = append(validScopes, scope.Scope)
	}

	return validScopes, nil
}
