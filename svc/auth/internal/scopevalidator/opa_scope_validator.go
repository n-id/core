package scopevalidator

import (
	"context"

	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/internal/opa"
	"gitlab.com/n-id/core/svc/auth/models"
)

// OpaScopeValidator is an implementation of ScopeValidator that uses OPA to validate scopes.
type OpaScopeValidator struct {
	conf    *opa.Config
	opaEval contract.OpaEvaluator[OpaScopeValidationDecision]
}

// NewOpaScopeValidator creates a new instance of OpaScopeValidator.
func NewOpaScopeValidator(conf *opa.Config) *OpaScopeValidator {
	return &OpaScopeValidator{
		conf:    conf,
		opaEval: opa.NewOpaEval[OpaScopeValidationDecision](conf),
	}
}

// Validate validates the provided claims against the OPA policy.
func (v *OpaScopeValidator) Validate(ctx context.Context, claims models.TokenClaims) ([]string, error) {
	_, ok := claims.Scopes.([]string)
	if !ok {
		return nil, errors.Wrap(contract.ErrInvalidArguments, "")
	}

	decision, err := v.opaEval.Eval(ctx, claims)
	if err != nil {
		return nil, errors.Wrap(err, "failed to evaluate claims")
	}

	if !decision.Result.Allowed {
		// Errors response is optional, so we need to check if it exists.
		if decision.Result.Errors == nil {
			return nil, errors.Wrap(contract.ErrInvalidArguments, "scope is not allowed")
		}

		return nil, errors.Wrap(contract.ErrInvalidArguments, opa.FmtErrors(decision.Result.Errors))
	}

	return decision.Result.ValidScopes, nil
}

// OpaScopeValidationDecision represents the decision made by OPA for the scope validation.
type OpaScopeValidationDecision struct {
	ValidScopes []string              `json:"valid_scopes"`
	Allowed     bool                  `json:"allowed"`
	Errors      []opa.ValidationError `json:"errors"`
}
