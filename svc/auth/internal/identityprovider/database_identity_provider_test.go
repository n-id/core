package identityprovider

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/n-id/core/pkg/password/mock"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/internal/repository"
	"gitlab.com/n-id/core/svc/auth/models"
)

type DatabaseProviderTestSuite struct {
	repository.AuthDBSuite
}

func (s *DatabaseProviderTestSuite) SetupSuite()    { s.AuthDBSuite.SetupSuite() }
func (s *DatabaseProviderTestSuite) SetupTest()     { s.AuthDBSuite.SetupTest() }
func (s *DatabaseProviderTestSuite) TearDownTest()  { s.AuthDBSuite.TearDownTest() }
func (s *DatabaseProviderTestSuite) TearDownSuite() { s.AuthDBSuite.TearDownSuite() }

func TestDatabaseProviderTestSuite(t *testing.T) {
	suite.Run(t, new(DatabaseProviderTestSuite))
}

func (s *DatabaseProviderTestSuite) TestDatabaseProviderGetIdentity() {
	testClient := &models.Client{
		Name:     "testclient",
		Password: "testpassword",
		Color:    "blue",
	}
	err := s.Tx.Create(testClient).Error
	s.Require().NoError(err)

	tests := []struct {
		Scenario               string
		Metadata               *models.TokenRequestMetadata
		ComparePasswordMatches bool
		ComparePasswordError   error
		Expected               string
		Error                  error
	}{
		{
			Scenario: "Client not found",
			Metadata: &models.TokenRequestMetadata{
				Username: "4777ceea-38a6-44a2-9258-f10decbd1d53",
				Password: "password",
			},
			Expected: "",
			Error:    contract.ErrUnauthorized,
		},
		{
			Scenario: "password empty",
			Metadata: &models.TokenRequestMetadata{
				Password: "",
				Username: "hello",
			},
			Expected: "",
			Error:    contract.ErrUnauthorized,
		},
		{
			Scenario: "Username empty",
			Metadata: &models.TokenRequestMetadata{
				Password: "hello",
				Username: "",
			},
			Expected: "",
			Error:    contract.ErrUnauthorized,
		},
		{
			Scenario: "Password does not match",
			Metadata: &models.TokenRequestMetadata{
				Username: testClient.ID.String(),
				Password: "wrong",
			},
			ComparePasswordMatches: false,
			ComparePasswordError:   nil,
			Expected:               "",
			Error:                  contract.ErrUnauthorized,
		},
		{
			Scenario: "Password manager error",
			Metadata: &models.TokenRequestMetadata{
				Username: testClient.ID.String(),
				Password: "wrong",
			},
			ComparePasswordMatches: false,
			ComparePasswordError:   contract.ErrInternalError,
			Expected:               "",
			Error:                  contract.ErrUnauthorized,
		},
		{
			Scenario: "Password matches",
			Metadata: &models.TokenRequestMetadata{
				Username: testClient.ID.String(),
				Password: testClient.Password,
			},
			ComparePasswordMatches: true,
			ComparePasswordError:   nil,
			Expected:               testClient.ID.String(),
			Error:                  nil,
		},
	}

	for _, test := range tests {
		passwordManagerMock := mock.NewIManager(s.T())
		passwordManagerMock.EXPECT().
			ComparePassword(test.Metadata.Password, testClient.Password).
			Return(test.ComparePasswordMatches, test.ComparePasswordError).
			Maybe()

		s.T().Run(test.Scenario, func(t *testing.T) {
			provider := NewDatabaseIdentityProvider(s.Repo.ClientDB, passwordManagerMock)
			result, err := provider.GetIdentity(context.Background(), test.Metadata)

			assert.Equal(t, test.Expected, result)
			assert.ErrorIs(t, err, test.Error)
		})
	}
}
