// Package identityprovider This package contains implementations of the IdentityProvider interface. The IdentityProvider interface provides the identity of request by using metadata.
package identityprovider

import (
	"context"

	"gitlab.com/n-id/core/pkg/password"
	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/models"
	"gorm.io/gorm"
)

// DatabaseIdentityProvider returns the identity from the database.
type DatabaseIdentityProvider struct {
	clientDB        *models.ClientDB
	passwordManager password.IManager
}

// NewDatabaseIdentityProvider returns a new instance of DatabaseIdentityProvider.
func NewDatabaseIdentityProvider(clientDB *models.ClientDB, passwordManager password.IManager) *DatabaseIdentityProvider {
	return &DatabaseIdentityProvider{
		clientDB:        clientDB,
		passwordManager: passwordManager,
	}
}

// GetIdentity returns the identity from the database.
func (p *DatabaseIdentityProvider) GetIdentity(_ context.Context, metadata *models.TokenRequestMetadata) (string, error) {
	if metadata.Password == "" || metadata.Username == "" {
		return "", errors.Wrapf(contract.ErrUnauthorized, "empty username or password")
	}

	client, err := p.clientDB.GetClientByID(metadata.Username)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return "", errors.Wrapf(contract.ErrUnauthorized, "invalid username")
		}

		return "", errors.Wrapf(contract.ErrInternalError, "unable to get client: %s", metadata.Username)
	}

	passwordMatches, err := p.passwordManager.ComparePassword(metadata.Password, client.Password)
	if err != nil {
		return "", errors.Wrapf(contract.ErrUnauthorized, "unable to compare password")
	}
	if !passwordMatches {
		return "", errors.Wrapf(contract.ErrUnauthorized, "incorrect password")
	}

	return metadata.Username, nil
}
