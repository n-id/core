// Package config provides the configuration for the identity providers.
package config

import "gitlab.com/n-id/core/svc/auth/contract"

// CertificateIdentityProviderConfig contains the configuration for the CertificateIdentityProvider.
type CertificateIdentityProviderConfig struct {
	// ClientIDPattern is a regex pattern that should match the identity header.
	// The first capture group will be used as the identity.
	// If no capture groups are present, the full match will be used as the identity.
	ClientIDPattern string `envconfig:"optional"`

	// Header is the header that contains the identity.
	// Will be used by default for HTTP and GRPC, and be overwritten in the corresponding config.
	Header string `envconfig:"optional"`
}

// Validate validates the CertificateIdentityProviderConfig.
func (c *CertificateIdentityProviderConfig) Validate() error {
	if c.Header == "" {
		return contract.ErrCertificateHeaderNotSet
	}

	return nil
}
