package contract

// PasswordManager password manager interface
type PasswordManager interface {
	GenerateHash(password string) (string, error)
	ComparePassword(password, hash string) (bool, error)
}
