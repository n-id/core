package contract

import (
	"context"

	"gitlab.com/n-id/core/svc/auth/models"
)

// ScopeValidator defines the interface for validating scopes.
type ScopeValidator interface {
	// Validate validates the given claims.
	Validate(ctx context.Context, claims models.TokenClaims) ([]string, error)
}

// ScopeValidatorType is an enum type of valid ScopeValidators.
type ScopeValidatorType string

const (
	// ScopeValidatorTypeNone registers no scope validator.
	ScopeValidatorTypeNone ScopeValidatorType = ""
	// ScopeValidatorTypeOpa represents a scope validator based on OPA.
	ScopeValidatorTypeOpa ScopeValidatorType = "opa"
	// ScopeValidatorTypeDatabase represents a scope validator based on a database.
	ScopeValidatorTypeDatabase ScopeValidatorType = "database"
)

// Validate checks if the ScopeValidatorType is valid.
func (t *ScopeValidatorType) Validate() error {
	if t == nil {
		return ErrIncorrectEnvironmentConfig
	}

	switch *t {
	case ScopeValidatorTypeNone, ScopeValidatorTypeOpa, ScopeValidatorTypeDatabase:
		return nil
	default:
		return ErrIncorrectEnvironmentConfig
	}
}

// Unmarshal parses a string and assigns the corresponding ScopeValidatorType.
func (t *ScopeValidatorType) Unmarshal(s string) error {
	switch s {
	case string(ScopeValidatorTypeNone):
		*t = ScopeValidatorTypeNone
	case string(ScopeValidatorTypeOpa):
		*t = ScopeValidatorTypeOpa
	case string(ScopeValidatorTypeDatabase):
		*t = ScopeValidatorTypeDatabase
	default:
		return ErrInvalidScopeValidator
	}

	return nil
}
