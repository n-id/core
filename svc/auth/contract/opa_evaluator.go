package contract

import (
	"context"

	"gitlab.com/n-id/core/svc/auth/models"
)

// OpaEvaluator defines the interface for interacting with OPA.
type OpaEvaluator[T any] interface {
	Eval(ctx context.Context, input any) (*models.OpaResponse[T], error)
}
