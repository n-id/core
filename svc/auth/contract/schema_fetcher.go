package contract

import (
	"context"

	"gitlab.com/n-id/core/pkg/gqlutil"
)

// SchemaFetcher makes the FetchSchema function testable
type SchemaFetcher interface {
	FetchSchema(ctx context.Context, url string) (*gqlutil.Schema, error)
}
