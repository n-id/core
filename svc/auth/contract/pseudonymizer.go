package contract

import "context"

// Pseudonymizer translates and generates pseudonyms
type Pseudonymizer interface {
	GetPseudonym(ctx context.Context, myPseudo, targetNamespace string) (string, error)
}
