package contract

// JWTParser interface for JWT parser
type JWTParser interface {
	DecodeSegment(seg string) ([]byte, error)
}
