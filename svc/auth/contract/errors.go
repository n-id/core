package contract

import (
	"github.com/pkg/errors"
)

var (
	// ErrInvalidArguments indicates an error when wrong arguments were used.
	ErrInvalidArguments = errors.New("Invalid Argument")
	// ErrInternalError represents an internal error.
	ErrInternalError = errors.New("Internal Error")
	// ErrNotFound indicates a resource not found error.
	ErrNotFound = errors.New("Not Found")
	// ErrUnauthorized indicates that the client is unauthorized.
	ErrUnauthorized = errors.New("unauthorized")
	// ErrDeadlineExceeded indicates that the context deadline was exceeded.
	ErrDeadlineExceeded = errors.New("deadline exceeded")

	// ErrUnableToRetrieveTokenExpiration indicates an error when getting a token outside the expiration time.
	ErrUnableToRetrieveTokenExpiration = errors.New("error getting token, token request outside expiration time")
	// ErrUnableToRetrieveTokenInvalidState indicates an error when getting a token with incorrect session state.
	ErrUnableToRetrieveTokenInvalidState = errors.New("error getting token, session in incorrect state")

	// ErrInvalidQueryModelType indicates an error when the query model does not have a related specific query model.
	ErrInvalidQueryModelType = errors.New("query model does not have a related specific query model")

	// ErrIncorrectBasicAuthPrefix indicates an error when the basic auth prefix is incorrect.
	ErrIncorrectBasicAuthPrefix = errors.New("incorrect basic auth prefix")
	// ErrIncorrectBasicAuthFormat indicates an error when the basic auth format is incorrect.
	ErrIncorrectBasicAuthFormat = errors.New("incorrect basic auth format")

	// ErrMultipleAudiencesNotAllowed indicates an error when multiple audiences are not allowed.
	ErrMultipleAudiencesNotAllowed = errors.New("multiple audiences are not allowed")

	// ErrIncorrectEnvironmentConfig indicates an error when the environment configuration is incorrect.
	ErrIncorrectEnvironmentConfig = errors.New("incorrect environment config")

	// ErrInvalidAudienceProvider indicates an error when the audience provider is invalid.
	ErrInvalidAudienceProvider = errors.New("invalid audience provider")
	// ErrInvalidIdentityProvider indicates an error when the identity provider is invalid.
	ErrInvalidIdentityProvider = errors.New("invalid identity provider")
	// ErrInvalidScopeValidator indicates an error when the scope validator is invalid.
	ErrInvalidScopeValidator = errors.New("invalid scope validator")

	// ErrCertificateHeaderNotSet indicates an error when the certificate header is not set but the identity provider is set to certificate.
	ErrCertificateHeaderNotSet = errors.New("identity provider set to certificate, but certificate header not set")

	// ErrCallbackNoResponseGiven indicates an error when no response is received from the callback server.
	ErrCallbackNoResponseGiven = errors.New("no response given from callback server")

	// ErrMarshallingRequestToClaimsProvider indicates an error when marshalling request to the claims provider fails.
	ErrMarshallingRequestToClaimsProvider = errors.New("error marshalling request to claims provider")

	// ErrRequestNil indicates an error when the request unexpectedly is nil.
	ErrRequestNil = errors.New("request is nil")
)
