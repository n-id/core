package contract

import (
	"context"

	"gitlab.com/n-id/core/svc/auth/models"
)

// AudienceProvider provides the audience of the provided scopes.
type AudienceProvider interface {
	GetAudience(ctx context.Context, req *models.TokenClientFlowRequest, scopes interface{}) ([]string, error)
}

// AudienceProviderType is an enum type of valid AudienceProviders.
type AudienceProviderType string

const (
	// AudienceProviderTypeDatabase represents an audience provider based on a database.
	AudienceProviderTypeDatabase AudienceProviderType = "database"
	// AudienceProviderTypeRequest represents an audience provider based on a request.
	AudienceProviderTypeRequest AudienceProviderType = "request"
)

// Validate checks if the AudienceProviderType is valid.
func (t *AudienceProviderType) Validate() error {
	if t == nil {
		return ErrIncorrectEnvironmentConfig
	}

	switch *t {
	case AudienceProviderTypeDatabase, AudienceProviderTypeRequest:
		return nil
	default:
		return ErrIncorrectEnvironmentConfig
	}
}

// Unmarshal parses a string and assigns the corresponding AudienceProviderType.
func (t *AudienceProviderType) Unmarshal(s string) error {
	switch s {
	case string(AudienceProviderTypeDatabase):
		*t = AudienceProviderTypeDatabase
	case string(AudienceProviderTypeRequest):
		*t = AudienceProviderTypeRequest
	default:
		return ErrInvalidAudienceProvider
	}

	return nil
}
