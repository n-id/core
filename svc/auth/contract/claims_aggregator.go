package contract

import (
	"context"

	"gitlab.com/n-id/core/svc/auth/models"
)

// ClaimsAggregator defines the interface for aggregating claims.
type ClaimsAggregator interface {
	// Aggregate aggregates claims for the provided token claims.
	Aggregate(ctx context.Context, claims *models.TokenClaims) (*models.AggregateClaims, error)
}
