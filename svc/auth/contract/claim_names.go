package contract

import (
	"context"

	"gitlab.com/n-id/core/svc/auth/models"
)

// ClaimNames is an interface for retrieving _claim_names from a set provider.
type ClaimNames interface {
	ClaimNames(ctx context.Context, claims *models.TokenClaims) (models.ClaimNamesMap, error)
}
