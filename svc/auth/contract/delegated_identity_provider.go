package contract

import (
	"context"

	"gitlab.com/n-id/core/svc/auth/models"
)

// DelegatedIdentityProvider provides the identity the actor and the subject of a request.
type DelegatedIdentityProvider interface {
	GetDelegatedIdentity(ctx context.Context, req *models.TokenClientFlowRequest) (string, error)
}
