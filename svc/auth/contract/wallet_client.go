package contract

import (
	"context"

	walletPB "gitlab.com/n-id/core/svc/wallet-rpc/proto"
	"google.golang.org/grpc"
)

// WalletClient is the client API for Wallet service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type WalletClient interface {
	// CreateConsent
	//
	// Will create a new consent
	CreateConsent(ctx context.Context, in *walletPB.CreateConsentRequest, opts ...grpc.CallOption) (*walletPB.ConsentResponse, error)
}
