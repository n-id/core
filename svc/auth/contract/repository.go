package contract

import (
	"context"

	"github.com/gofrs/uuid"
	"gitlab.com/n-id/core/svc/auth/models"
)

// ScopeRepository defines the interface for interacting with scopes.
type ScopeRepository interface {
	// Get retrieves a scope by its ID.
	Get(ctx context.Context, id uuid.UUID) (*models.Scope, error)

	// ListAllMatching retrieves all scopes matching the provided list of scope names.
	ListAllMatching(ctx context.Context, scopes []string) ([]*models.Scope, error)

	// List retrieves all scopes.
	List(ctx context.Context) ([]*models.Scope, error)

	// Add adds a new scope.
	Add(ctx context.Context, model *models.Scope) error

	// Update updates an existing scope.
	Update(ctx context.Context, model *models.Scope) error

	// Delete deletes a scope by its ID.
	Delete(ctx context.Context, id uuid.UUID) error
}
