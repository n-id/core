package contract

import "github.com/golang-jwt/jwt/v5"

// JWTClient interface for JWT client
type JWTClient interface {
	ValidateAndParseClaims(bearer string, claims jwt.Claims) error
	SignToken(claims jwt.Claims) (string, error)
	ParseWithClaims(token string) (*jwt.Token, *jwt.RegisteredClaims, error)
}
