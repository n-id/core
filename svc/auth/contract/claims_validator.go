package contract

import (
	"context"

	"gitlab.com/n-id/core/svc/auth/models"
)

// ClaimsValidator is the interface for token validation.
type ClaimsValidator interface {
	Validate(context.Context, models.TokenClaims) error
}
