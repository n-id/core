package contract

import "net/http"

// HTTPClient is a simple http client interface
type HTTPClient interface {
	Do(*http.Request) (*http.Response, error)
}
