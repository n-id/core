// Code generated by mockery v2.44.1. DO NOT EDIT.

package mock

import (
	context "context"

	mock "github.com/stretchr/testify/mock"

	models "gitlab.com/n-id/core/svc/auth/models"
)

// ClaimNames is an autogenerated mock type for the ClaimNames type
type ClaimNames struct {
	mock.Mock
}

type ClaimNames_Expecter struct {
	mock *mock.Mock
}

func (_m *ClaimNames) EXPECT() *ClaimNames_Expecter {
	return &ClaimNames_Expecter{mock: &_m.Mock}
}

// ClaimNames provides a mock function with given fields: ctx, claims
func (_m *ClaimNames) ClaimNames(ctx context.Context, claims *models.TokenClaims) (models.ClaimNamesMap, error) {
	ret := _m.Called(ctx, claims)

	if len(ret) == 0 {
		panic("no return value specified for ClaimNames")
	}

	var r0 models.ClaimNamesMap
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *models.TokenClaims) (models.ClaimNamesMap, error)); ok {
		return rf(ctx, claims)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *models.TokenClaims) models.ClaimNamesMap); ok {
		r0 = rf(ctx, claims)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(models.ClaimNamesMap)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *models.TokenClaims) error); ok {
		r1 = rf(ctx, claims)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ClaimNames_ClaimNames_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'ClaimNames'
type ClaimNames_ClaimNames_Call struct {
	*mock.Call
}

// ClaimNames is a helper method to define mock.On call
//   - ctx context.Context
//   - claims *models.TokenClaims
func (_e *ClaimNames_Expecter) ClaimNames(ctx interface{}, claims interface{}) *ClaimNames_ClaimNames_Call {
	return &ClaimNames_ClaimNames_Call{Call: _e.mock.On("ClaimNames", ctx, claims)}
}

func (_c *ClaimNames_ClaimNames_Call) Run(run func(ctx context.Context, claims *models.TokenClaims)) *ClaimNames_ClaimNames_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(*models.TokenClaims))
	})
	return _c
}

func (_c *ClaimNames_ClaimNames_Call) Return(_a0 models.ClaimNamesMap, _a1 error) *ClaimNames_ClaimNames_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *ClaimNames_ClaimNames_Call) RunAndReturn(run func(context.Context, *models.TokenClaims) (models.ClaimNamesMap, error)) *ClaimNames_ClaimNames_Call {
	_c.Call.Return(run)
	return _c
}

// NewClaimNames creates a new instance of ClaimNames. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewClaimNames(t interface {
	mock.TestingT
	Cleanup(func())
}) *ClaimNames {
	mock := &ClaimNames{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
