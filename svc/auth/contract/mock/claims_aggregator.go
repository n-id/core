// Code generated by mockery v2.44.1. DO NOT EDIT.

package mock

import (
	context "context"

	mock "github.com/stretchr/testify/mock"

	models "gitlab.com/n-id/core/svc/auth/models"
)

// ClaimsAggregator is an autogenerated mock type for the ClaimsAggregator type
type ClaimsAggregator struct {
	mock.Mock
}

type ClaimsAggregator_Expecter struct {
	mock *mock.Mock
}

func (_m *ClaimsAggregator) EXPECT() *ClaimsAggregator_Expecter {
	return &ClaimsAggregator_Expecter{mock: &_m.Mock}
}

// Aggregate provides a mock function with given fields: ctx, claims
func (_m *ClaimsAggregator) Aggregate(ctx context.Context, claims *models.TokenClaims) (*models.AggregateClaims, error) {
	ret := _m.Called(ctx, claims)

	if len(ret) == 0 {
		panic("no return value specified for Aggregate")
	}

	var r0 *models.AggregateClaims
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *models.TokenClaims) (*models.AggregateClaims, error)); ok {
		return rf(ctx, claims)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *models.TokenClaims) *models.AggregateClaims); ok {
		r0 = rf(ctx, claims)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.AggregateClaims)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *models.TokenClaims) error); ok {
		r1 = rf(ctx, claims)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ClaimsAggregator_Aggregate_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Aggregate'
type ClaimsAggregator_Aggregate_Call struct {
	*mock.Call
}

// Aggregate is a helper method to define mock.On call
//   - ctx context.Context
//   - claims *models.TokenClaims
func (_e *ClaimsAggregator_Expecter) Aggregate(ctx interface{}, claims interface{}) *ClaimsAggregator_Aggregate_Call {
	return &ClaimsAggregator_Aggregate_Call{Call: _e.mock.On("Aggregate", ctx, claims)}
}

func (_c *ClaimsAggregator_Aggregate_Call) Run(run func(ctx context.Context, claims *models.TokenClaims)) *ClaimsAggregator_Aggregate_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(*models.TokenClaims))
	})
	return _c
}

func (_c *ClaimsAggregator_Aggregate_Call) Return(_a0 *models.AggregateClaims, _a1 error) *ClaimsAggregator_Aggregate_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *ClaimsAggregator_Aggregate_Call) RunAndReturn(run func(context.Context, *models.TokenClaims) (*models.AggregateClaims, error)) *ClaimsAggregator_Aggregate_Call {
	_c.Call.Return(run)
	return _c
}

// NewClaimsAggregator creates a new instance of ClaimsAggregator. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewClaimsAggregator(t interface {
	mock.TestingT
	Cleanup(func())
}) *ClaimsAggregator {
	mock := &ClaimsAggregator{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
