// Code generated by mockery v2.44.1. DO NOT EDIT.

package mock

import (
	context "context"

	mock "github.com/stretchr/testify/mock"

	models "gitlab.com/n-id/core/svc/auth/models"
)

// ClaimsValidator is an autogenerated mock type for the ClaimsValidator type
type ClaimsValidator struct {
	mock.Mock
}

type ClaimsValidator_Expecter struct {
	mock *mock.Mock
}

func (_m *ClaimsValidator) EXPECT() *ClaimsValidator_Expecter {
	return &ClaimsValidator_Expecter{mock: &_m.Mock}
}

// Validate provides a mock function with given fields: _a0, _a1
func (_m *ClaimsValidator) Validate(_a0 context.Context, _a1 models.TokenClaims) error {
	ret := _m.Called(_a0, _a1)

	if len(ret) == 0 {
		panic("no return value specified for Validate")
	}

	var r0 error
	if rf, ok := ret.Get(0).(func(context.Context, models.TokenClaims) error); ok {
		r0 = rf(_a0, _a1)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ClaimsValidator_Validate_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Validate'
type ClaimsValidator_Validate_Call struct {
	*mock.Call
}

// Validate is a helper method to define mock.On call
//   - _a0 context.Context
//   - _a1 models.TokenClaims
func (_e *ClaimsValidator_Expecter) Validate(_a0 interface{}, _a1 interface{}) *ClaimsValidator_Validate_Call {
	return &ClaimsValidator_Validate_Call{Call: _e.mock.On("Validate", _a0, _a1)}
}

func (_c *ClaimsValidator_Validate_Call) Run(run func(_a0 context.Context, _a1 models.TokenClaims)) *ClaimsValidator_Validate_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(context.Context), args[1].(models.TokenClaims))
	})
	return _c
}

func (_c *ClaimsValidator_Validate_Call) Return(_a0 error) *ClaimsValidator_Validate_Call {
	_c.Call.Return(_a0)
	return _c
}

func (_c *ClaimsValidator_Validate_Call) RunAndReturn(run func(context.Context, models.TokenClaims) error) *ClaimsValidator_Validate_Call {
	_c.Call.Return(run)
	return _c
}

// NewClaimsValidator creates a new instance of ClaimsValidator. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewClaimsValidator(t interface {
	mock.TestingT
	Cleanup(func())
}) *ClaimsValidator {
	mock := &ClaimsValidator{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
