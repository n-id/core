// Package authscopes scopes for the auth public endpoint
package authscopes

// AuthClaim contains the scope for Claim in the Auth service
const AuthClaim = "claim"

// AuthAccept contains the scope for Accept in the Auth service
const AuthAccept = "accept"

// AuthReject contains the scope for Reject in the Auth service
const AuthReject = "reject"
