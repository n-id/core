// Code generated by mockery v2.44.1. DO NOT EDIT.

package mock

import (
	context "context"

	grpc "google.golang.org/grpc"
	emptypb "google.golang.org/protobuf/types/known/emptypb"

	mock "github.com/stretchr/testify/mock"

	proto "gitlab.com/n-id/core/svc/auth/transport/grpc/proto"
)

// AuthClient is an autogenerated mock type for the AuthClient type
type AuthClient struct {
	mock.Mock
}

type AuthClient_Expecter struct {
	mock *mock.Mock
}

func (_m *AuthClient) EXPECT() *AuthClient_Expecter {
	return &AuthClient_Expecter{mock: &_m.Mock}
}

// Accept provides a mock function with given fields: ctx, in, opts
func (_m *AuthClient) Accept(ctx context.Context, in *proto.AcceptRequest, opts ...grpc.CallOption) (*proto.SessionResponse, error) {
	_va := make([]interface{}, len(opts))
	for _i := range opts {
		_va[_i] = opts[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx, in)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	if len(ret) == 0 {
		panic("no return value specified for Accept")
	}

	var r0 *proto.SessionResponse
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *proto.AcceptRequest, ...grpc.CallOption) (*proto.SessionResponse, error)); ok {
		return rf(ctx, in, opts...)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *proto.AcceptRequest, ...grpc.CallOption) *proto.SessionResponse); ok {
		r0 = rf(ctx, in, opts...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*proto.SessionResponse)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *proto.AcceptRequest, ...grpc.CallOption) error); ok {
		r1 = rf(ctx, in, opts...)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// AuthClient_Accept_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Accept'
type AuthClient_Accept_Call struct {
	*mock.Call
}

// Accept is a helper method to define mock.On call
//   - ctx context.Context
//   - in *proto.AcceptRequest
//   - opts ...grpc.CallOption
func (_e *AuthClient_Expecter) Accept(ctx interface{}, in interface{}, opts ...interface{}) *AuthClient_Accept_Call {
	return &AuthClient_Accept_Call{Call: _e.mock.On("Accept",
		append([]interface{}{ctx, in}, opts...)...)}
}

func (_c *AuthClient_Accept_Call) Run(run func(ctx context.Context, in *proto.AcceptRequest, opts ...grpc.CallOption)) *AuthClient_Accept_Call {
	_c.Call.Run(func(args mock.Arguments) {
		variadicArgs := make([]grpc.CallOption, len(args)-2)
		for i, a := range args[2:] {
			if a != nil {
				variadicArgs[i] = a.(grpc.CallOption)
			}
		}
		run(args[0].(context.Context), args[1].(*proto.AcceptRequest), variadicArgs...)
	})
	return _c
}

func (_c *AuthClient_Accept_Call) Return(_a0 *proto.SessionResponse, _a1 error) *AuthClient_Accept_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *AuthClient_Accept_Call) RunAndReturn(run func(context.Context, *proto.AcceptRequest, ...grpc.CallOption) (*proto.SessionResponse, error)) *AuthClient_Accept_Call {
	_c.Call.Return(run)
	return _c
}

// AuthorizeHeadless provides a mock function with given fields: ctx, in, opts
func (_m *AuthClient) AuthorizeHeadless(ctx context.Context, in *proto.AuthorizeHeadlessRequest, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	_va := make([]interface{}, len(opts))
	for _i := range opts {
		_va[_i] = opts[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx, in)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	if len(ret) == 0 {
		panic("no return value specified for AuthorizeHeadless")
	}

	var r0 *emptypb.Empty
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *proto.AuthorizeHeadlessRequest, ...grpc.CallOption) (*emptypb.Empty, error)); ok {
		return rf(ctx, in, opts...)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *proto.AuthorizeHeadlessRequest, ...grpc.CallOption) *emptypb.Empty); ok {
		r0 = rf(ctx, in, opts...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*emptypb.Empty)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *proto.AuthorizeHeadlessRequest, ...grpc.CallOption) error); ok {
		r1 = rf(ctx, in, opts...)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// AuthClient_AuthorizeHeadless_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'AuthorizeHeadless'
type AuthClient_AuthorizeHeadless_Call struct {
	*mock.Call
}

// AuthorizeHeadless is a helper method to define mock.On call
//   - ctx context.Context
//   - in *proto.AuthorizeHeadlessRequest
//   - opts ...grpc.CallOption
func (_e *AuthClient_Expecter) AuthorizeHeadless(ctx interface{}, in interface{}, opts ...interface{}) *AuthClient_AuthorizeHeadless_Call {
	return &AuthClient_AuthorizeHeadless_Call{Call: _e.mock.On("AuthorizeHeadless",
		append([]interface{}{ctx, in}, opts...)...)}
}

func (_c *AuthClient_AuthorizeHeadless_Call) Run(run func(ctx context.Context, in *proto.AuthorizeHeadlessRequest, opts ...grpc.CallOption)) *AuthClient_AuthorizeHeadless_Call {
	_c.Call.Run(func(args mock.Arguments) {
		variadicArgs := make([]grpc.CallOption, len(args)-2)
		for i, a := range args[2:] {
			if a != nil {
				variadicArgs[i] = a.(grpc.CallOption)
			}
		}
		run(args[0].(context.Context), args[1].(*proto.AuthorizeHeadlessRequest), variadicArgs...)
	})
	return _c
}

func (_c *AuthClient_AuthorizeHeadless_Call) Return(_a0 *emptypb.Empty, _a1 error) *AuthClient_AuthorizeHeadless_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *AuthClient_AuthorizeHeadless_Call) RunAndReturn(run func(context.Context, *proto.AuthorizeHeadlessRequest, ...grpc.CallOption) (*emptypb.Empty, error)) *AuthClient_AuthorizeHeadless_Call {
	_c.Call.Return(run)
	return _c
}

// Claim provides a mock function with given fields: ctx, in, opts
func (_m *AuthClient) Claim(ctx context.Context, in *proto.SessionRequest, opts ...grpc.CallOption) (*proto.SessionResponse, error) {
	_va := make([]interface{}, len(opts))
	for _i := range opts {
		_va[_i] = opts[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx, in)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	if len(ret) == 0 {
		panic("no return value specified for Claim")
	}

	var r0 *proto.SessionResponse
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *proto.SessionRequest, ...grpc.CallOption) (*proto.SessionResponse, error)); ok {
		return rf(ctx, in, opts...)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *proto.SessionRequest, ...grpc.CallOption) *proto.SessionResponse); ok {
		r0 = rf(ctx, in, opts...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*proto.SessionResponse)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *proto.SessionRequest, ...grpc.CallOption) error); ok {
		r1 = rf(ctx, in, opts...)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// AuthClient_Claim_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Claim'
type AuthClient_Claim_Call struct {
	*mock.Call
}

// Claim is a helper method to define mock.On call
//   - ctx context.Context
//   - in *proto.SessionRequest
//   - opts ...grpc.CallOption
func (_e *AuthClient_Expecter) Claim(ctx interface{}, in interface{}, opts ...interface{}) *AuthClient_Claim_Call {
	return &AuthClient_Claim_Call{Call: _e.mock.On("Claim",
		append([]interface{}{ctx, in}, opts...)...)}
}

func (_c *AuthClient_Claim_Call) Run(run func(ctx context.Context, in *proto.SessionRequest, opts ...grpc.CallOption)) *AuthClient_Claim_Call {
	_c.Call.Run(func(args mock.Arguments) {
		variadicArgs := make([]grpc.CallOption, len(args)-2)
		for i, a := range args[2:] {
			if a != nil {
				variadicArgs[i] = a.(grpc.CallOption)
			}
		}
		run(args[0].(context.Context), args[1].(*proto.SessionRequest), variadicArgs...)
	})
	return _c
}

func (_c *AuthClient_Claim_Call) Return(_a0 *proto.SessionResponse, _a1 error) *AuthClient_Claim_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *AuthClient_Claim_Call) RunAndReturn(run func(context.Context, *proto.SessionRequest, ...grpc.CallOption) (*proto.SessionResponse, error)) *AuthClient_Claim_Call {
	_c.Call.Return(run)
	return _c
}

// Finalise provides a mock function with given fields: ctx, in, opts
func (_m *AuthClient) Finalise(ctx context.Context, in *proto.FinaliseRequest, opts ...grpc.CallOption) (*proto.FinaliseResponse, error) {
	_va := make([]interface{}, len(opts))
	for _i := range opts {
		_va[_i] = opts[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx, in)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	if len(ret) == 0 {
		panic("no return value specified for Finalise")
	}

	var r0 *proto.FinaliseResponse
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *proto.FinaliseRequest, ...grpc.CallOption) (*proto.FinaliseResponse, error)); ok {
		return rf(ctx, in, opts...)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *proto.FinaliseRequest, ...grpc.CallOption) *proto.FinaliseResponse); ok {
		r0 = rf(ctx, in, opts...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*proto.FinaliseResponse)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *proto.FinaliseRequest, ...grpc.CallOption) error); ok {
		r1 = rf(ctx, in, opts...)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// AuthClient_Finalise_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Finalise'
type AuthClient_Finalise_Call struct {
	*mock.Call
}

// Finalise is a helper method to define mock.On call
//   - ctx context.Context
//   - in *proto.FinaliseRequest
//   - opts ...grpc.CallOption
func (_e *AuthClient_Expecter) Finalise(ctx interface{}, in interface{}, opts ...interface{}) *AuthClient_Finalise_Call {
	return &AuthClient_Finalise_Call{Call: _e.mock.On("Finalise",
		append([]interface{}{ctx, in}, opts...)...)}
}

func (_c *AuthClient_Finalise_Call) Run(run func(ctx context.Context, in *proto.FinaliseRequest, opts ...grpc.CallOption)) *AuthClient_Finalise_Call {
	_c.Call.Run(func(args mock.Arguments) {
		variadicArgs := make([]grpc.CallOption, len(args)-2)
		for i, a := range args[2:] {
			if a != nil {
				variadicArgs[i] = a.(grpc.CallOption)
			}
		}
		run(args[0].(context.Context), args[1].(*proto.FinaliseRequest), variadicArgs...)
	})
	return _c
}

func (_c *AuthClient_Finalise_Call) Return(_a0 *proto.FinaliseResponse, _a1 error) *AuthClient_Finalise_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *AuthClient_Finalise_Call) RunAndReturn(run func(context.Context, *proto.FinaliseRequest, ...grpc.CallOption) (*proto.FinaliseResponse, error)) *AuthClient_Finalise_Call {
	_c.Call.Return(run)
	return _c
}

// GenerateSessionFinaliseToken provides a mock function with given fields: ctx, in, opts
func (_m *AuthClient) GenerateSessionFinaliseToken(ctx context.Context, in *proto.SessionRequest, opts ...grpc.CallOption) (*proto.SessionAuthorization, error) {
	_va := make([]interface{}, len(opts))
	for _i := range opts {
		_va[_i] = opts[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx, in)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	if len(ret) == 0 {
		panic("no return value specified for GenerateSessionFinaliseToken")
	}

	var r0 *proto.SessionAuthorization
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *proto.SessionRequest, ...grpc.CallOption) (*proto.SessionAuthorization, error)); ok {
		return rf(ctx, in, opts...)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *proto.SessionRequest, ...grpc.CallOption) *proto.SessionAuthorization); ok {
		r0 = rf(ctx, in, opts...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*proto.SessionAuthorization)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *proto.SessionRequest, ...grpc.CallOption) error); ok {
		r1 = rf(ctx, in, opts...)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// AuthClient_GenerateSessionFinaliseToken_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'GenerateSessionFinaliseToken'
type AuthClient_GenerateSessionFinaliseToken_Call struct {
	*mock.Call
}

// GenerateSessionFinaliseToken is a helper method to define mock.On call
//   - ctx context.Context
//   - in *proto.SessionRequest
//   - opts ...grpc.CallOption
func (_e *AuthClient_Expecter) GenerateSessionFinaliseToken(ctx interface{}, in interface{}, opts ...interface{}) *AuthClient_GenerateSessionFinaliseToken_Call {
	return &AuthClient_GenerateSessionFinaliseToken_Call{Call: _e.mock.On("GenerateSessionFinaliseToken",
		append([]interface{}{ctx, in}, opts...)...)}
}

func (_c *AuthClient_GenerateSessionFinaliseToken_Call) Run(run func(ctx context.Context, in *proto.SessionRequest, opts ...grpc.CallOption)) *AuthClient_GenerateSessionFinaliseToken_Call {
	_c.Call.Run(func(args mock.Arguments) {
		variadicArgs := make([]grpc.CallOption, len(args)-2)
		for i, a := range args[2:] {
			if a != nil {
				variadicArgs[i] = a.(grpc.CallOption)
			}
		}
		run(args[0].(context.Context), args[1].(*proto.SessionRequest), variadicArgs...)
	})
	return _c
}

func (_c *AuthClient_GenerateSessionFinaliseToken_Call) Return(_a0 *proto.SessionAuthorization, _a1 error) *AuthClient_GenerateSessionFinaliseToken_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *AuthClient_GenerateSessionFinaliseToken_Call) RunAndReturn(run func(context.Context, *proto.SessionRequest, ...grpc.CallOption) (*proto.SessionAuthorization, error)) *AuthClient_GenerateSessionFinaliseToken_Call {
	_c.Call.Return(run)
	return _c
}

// Status provides a mock function with given fields: ctx, in, opts
func (_m *AuthClient) Status(ctx context.Context, in *proto.SessionRequest, opts ...grpc.CallOption) (*proto.StatusResponse, error) {
	_va := make([]interface{}, len(opts))
	for _i := range opts {
		_va[_i] = opts[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx, in)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	if len(ret) == 0 {
		panic("no return value specified for Status")
	}

	var r0 *proto.StatusResponse
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *proto.SessionRequest, ...grpc.CallOption) (*proto.StatusResponse, error)); ok {
		return rf(ctx, in, opts...)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *proto.SessionRequest, ...grpc.CallOption) *proto.StatusResponse); ok {
		r0 = rf(ctx, in, opts...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*proto.StatusResponse)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *proto.SessionRequest, ...grpc.CallOption) error); ok {
		r1 = rf(ctx, in, opts...)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// AuthClient_Status_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Status'
type AuthClient_Status_Call struct {
	*mock.Call
}

// Status is a helper method to define mock.On call
//   - ctx context.Context
//   - in *proto.SessionRequest
//   - opts ...grpc.CallOption
func (_e *AuthClient_Expecter) Status(ctx interface{}, in interface{}, opts ...interface{}) *AuthClient_Status_Call {
	return &AuthClient_Status_Call{Call: _e.mock.On("Status",
		append([]interface{}{ctx, in}, opts...)...)}
}

func (_c *AuthClient_Status_Call) Run(run func(ctx context.Context, in *proto.SessionRequest, opts ...grpc.CallOption)) *AuthClient_Status_Call {
	_c.Call.Run(func(args mock.Arguments) {
		variadicArgs := make([]grpc.CallOption, len(args)-2)
		for i, a := range args[2:] {
			if a != nil {
				variadicArgs[i] = a.(grpc.CallOption)
			}
		}
		run(args[0].(context.Context), args[1].(*proto.SessionRequest), variadicArgs...)
	})
	return _c
}

func (_c *AuthClient_Status_Call) Return(_a0 *proto.StatusResponse, _a1 error) *AuthClient_Status_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *AuthClient_Status_Call) RunAndReturn(run func(context.Context, *proto.SessionRequest, ...grpc.CallOption) (*proto.StatusResponse, error)) *AuthClient_Status_Call {
	_c.Call.Return(run)
	return _c
}

// SwapToken provides a mock function with given fields: ctx, in, opts
func (_m *AuthClient) SwapToken(ctx context.Context, in *proto.SwapTokenRequest, opts ...grpc.CallOption) (*proto.TokenResponse, error) {
	_va := make([]interface{}, len(opts))
	for _i := range opts {
		_va[_i] = opts[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx, in)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	if len(ret) == 0 {
		panic("no return value specified for SwapToken")
	}

	var r0 *proto.TokenResponse
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *proto.SwapTokenRequest, ...grpc.CallOption) (*proto.TokenResponse, error)); ok {
		return rf(ctx, in, opts...)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *proto.SwapTokenRequest, ...grpc.CallOption) *proto.TokenResponse); ok {
		r0 = rf(ctx, in, opts...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*proto.TokenResponse)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *proto.SwapTokenRequest, ...grpc.CallOption) error); ok {
		r1 = rf(ctx, in, opts...)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// AuthClient_SwapToken_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'SwapToken'
type AuthClient_SwapToken_Call struct {
	*mock.Call
}

// SwapToken is a helper method to define mock.On call
//   - ctx context.Context
//   - in *proto.SwapTokenRequest
//   - opts ...grpc.CallOption
func (_e *AuthClient_Expecter) SwapToken(ctx interface{}, in interface{}, opts ...interface{}) *AuthClient_SwapToken_Call {
	return &AuthClient_SwapToken_Call{Call: _e.mock.On("SwapToken",
		append([]interface{}{ctx, in}, opts...)...)}
}

func (_c *AuthClient_SwapToken_Call) Run(run func(ctx context.Context, in *proto.SwapTokenRequest, opts ...grpc.CallOption)) *AuthClient_SwapToken_Call {
	_c.Call.Run(func(args mock.Arguments) {
		variadicArgs := make([]grpc.CallOption, len(args)-2)
		for i, a := range args[2:] {
			if a != nil {
				variadicArgs[i] = a.(grpc.CallOption)
			}
		}
		run(args[0].(context.Context), args[1].(*proto.SwapTokenRequest), variadicArgs...)
	})
	return _c
}

func (_c *AuthClient_SwapToken_Call) Return(_a0 *proto.TokenResponse, _a1 error) *AuthClient_SwapToken_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *AuthClient_SwapToken_Call) RunAndReturn(run func(context.Context, *proto.SwapTokenRequest, ...grpc.CallOption) (*proto.TokenResponse, error)) *AuthClient_SwapToken_Call {
	_c.Call.Return(run)
	return _c
}

// Token provides a mock function with given fields: ctx, in, opts
func (_m *AuthClient) Token(ctx context.Context, in *proto.TokenRequest, opts ...grpc.CallOption) (*proto.TokenResponse, error) {
	_va := make([]interface{}, len(opts))
	for _i := range opts {
		_va[_i] = opts[_i]
	}
	var _ca []interface{}
	_ca = append(_ca, ctx, in)
	_ca = append(_ca, _va...)
	ret := _m.Called(_ca...)

	if len(ret) == 0 {
		panic("no return value specified for Token")
	}

	var r0 *proto.TokenResponse
	var r1 error
	if rf, ok := ret.Get(0).(func(context.Context, *proto.TokenRequest, ...grpc.CallOption) (*proto.TokenResponse, error)); ok {
		return rf(ctx, in, opts...)
	}
	if rf, ok := ret.Get(0).(func(context.Context, *proto.TokenRequest, ...grpc.CallOption) *proto.TokenResponse); ok {
		r0 = rf(ctx, in, opts...)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*proto.TokenResponse)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, *proto.TokenRequest, ...grpc.CallOption) error); ok {
		r1 = rf(ctx, in, opts...)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// AuthClient_Token_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'Token'
type AuthClient_Token_Call struct {
	*mock.Call
}

// Token is a helper method to define mock.On call
//   - ctx context.Context
//   - in *proto.TokenRequest
//   - opts ...grpc.CallOption
func (_e *AuthClient_Expecter) Token(ctx interface{}, in interface{}, opts ...interface{}) *AuthClient_Token_Call {
	return &AuthClient_Token_Call{Call: _e.mock.On("Token",
		append([]interface{}{ctx, in}, opts...)...)}
}

func (_c *AuthClient_Token_Call) Run(run func(ctx context.Context, in *proto.TokenRequest, opts ...grpc.CallOption)) *AuthClient_Token_Call {
	_c.Call.Run(func(args mock.Arguments) {
		variadicArgs := make([]grpc.CallOption, len(args)-2)
		for i, a := range args[2:] {
			if a != nil {
				variadicArgs[i] = a.(grpc.CallOption)
			}
		}
		run(args[0].(context.Context), args[1].(*proto.TokenRequest), variadicArgs...)
	})
	return _c
}

func (_c *AuthClient_Token_Call) Return(_a0 *proto.TokenResponse, _a1 error) *AuthClient_Token_Call {
	_c.Call.Return(_a0, _a1)
	return _c
}

func (_c *AuthClient_Token_Call) RunAndReturn(run func(context.Context, *proto.TokenRequest, ...grpc.CallOption) (*proto.TokenResponse, error)) *AuthClient_Token_Call {
	_c.Call.Return(run)
	return _c
}

// NewAuthClient creates a new instance of AuthClient. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewAuthClient(t interface {
	mock.TestingT
	Cleanup(func())
}) *AuthClient {
	mock := &AuthClient{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
