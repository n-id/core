package grpc

// Config configuration for the grpc layer of auth service
type Config struct {
	Port int
	// CertificateHeader is the header that contains the identity.
	// When this is set, the Header of CertificateIdentityProviderConfig will be overwritten.
	CertificateHeader string `envconfig:"optional"`
}
