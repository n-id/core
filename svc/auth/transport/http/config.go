package http

// Config configuration for the http layer of the auth app.
type Config struct {
	Port string
	// CertificateHeader is the header that contains the identity.
	// When this is set, the Header of CertificateIdentityProviderConfig will be overwritten.
	CertificateHeader string `envconfig:"optional"`
}
