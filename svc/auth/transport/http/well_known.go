package http

import (
	"github.com/gin-gonic/gin"
)

// WellKnown holds the data for the openID configuration.
type WellKnown struct {
	Issuer                string `json:"issuer"`
	JwksURI               string `json:"jwks_uri"`
	TokenEndpoint         string `json:"token_endpoint"`
	AuthorizationEndpoint string `json:"authorization_endpoint"`
}

// NewWellKnown returns a new instance of WellKnown.
func NewWellKnown(issuer, jwksURI, tokenEndpoint, authzEndpoint string) *WellKnown {
	wellKnown := &WellKnown{
		Issuer:                issuer,
		JwksURI:               jwksURI,
		TokenEndpoint:         tokenEndpoint,
		AuthorizationEndpoint: authzEndpoint,
	}

	return wellKnown
}

// Handler returns a body with the OpenID specification.
func (w *WellKnown) Handler(c *gin.Context) {
	c.JSON(200, w)
}
