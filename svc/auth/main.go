// Package main initiates the auth service
package main

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/n-id/core/pkg/gqlutil"
	"gitlab.com/n-id/core/pkg/interceptor/xrequestid"
	"gitlab.com/n-id/core/pkg/jwtconfig"
	"gitlab.com/n-id/core/pkg/password"
	"gitlab.com/n-id/core/pkg/pseudonym"
	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/pkg/utilities/grpcserver"
	"gitlab.com/n-id/core/pkg/utilities/grpcserver/headers"
	"gitlab.com/n-id/core/pkg/utilities/grpcserver/metrics"
	"gitlab.com/n-id/core/pkg/utilities/grpcserver/servicebase"
	"gitlab.com/n-id/core/pkg/utilities/jwt/v3"
	"gitlab.com/n-id/core/pkg/utilities/log/v2"
	"gitlab.com/n-id/core/svc/auth/app"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/internal/audienceprovider"
	"gitlab.com/n-id/core/svc/auth/internal/claimsaggregator"
	"gitlab.com/n-id/core/svc/auth/internal/claimsvalidator"
	"gitlab.com/n-id/core/svc/auth/internal/config"
	"gitlab.com/n-id/core/svc/auth/internal/identityprovider"
	"gitlab.com/n-id/core/svc/auth/internal/repository"
	"gitlab.com/n-id/core/svc/auth/internal/retryhttp"
	"gitlab.com/n-id/core/svc/auth/internal/scopevalidator"
	"gitlab.com/n-id/core/svc/auth/internal/stats"
	grpcTransport "gitlab.com/n-id/core/svc/auth/transport/grpc"
	pb "gitlab.com/n-id/core/svc/auth/transport/grpc/proto"
	"gitlab.com/n-id/core/svc/auth/transport/http"
	walletPB "gitlab.com/n-id/core/svc/wallet-rpc/proto"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.opentelemetry.io/contrib/propagators/b3"
	"go.opentelemetry.io/otel"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func initialiseRegistry(conf *config.AuthConfig, app contract.App) *AuthServiceRegistry {
	err := log.SetFormat(log.Format(conf.LogFormat))
	if err != nil {
		log.WithError(err).Fatal("unable to set log format")
	}

	log.WithField("level", conf.GetLogLevel()).Info("Setting log level")

	err = log.SetLevel(log.Level(conf.LogLevel))
	if err != nil {
		log.WithError(err).Fatal("unable to set log level")
	}

	registry := &AuthServiceRegistry{
		authClient: grpcTransport.New(app, &headers.GRPCMetadataHelper{}, &conf.Transport.Grpc),
	}

	return registry
}

func getJWTClient(conf *config.AuthConfig) *jwt.Client {
	jwtKey, err := jwtconfig.Read(conf.JWTPath)
	if err != nil {
		log.WithError(err).Fatal("unable to load additional config from files")
	}

	jwtClientOpts := jwt.DefaultOpts()
	jwtClientOpts.HeaderOpts.KID = jwtKey.ID
	jwtClientOpts.MarshalSingleStringAsArray = conf.MarshalSingleAudienceOrScopeAsArray
	jwtClient := jwt.NewJWTClientWithOpts(jwtKey.PrivateKey, &jwtKey.PublicKey, jwtClientOpts)
	return jwtClient
}

func main() {
	conf, err := config.New()
	if err != nil {
		log.WithError(err).Fatal("invalid config")
	}

	jwtClient := getJWTClient(conf)
	authApp := getAuthApp(conf, jwtClient)
	registry := initialiseRegistry(conf, authApp)

	p := b3.New(b3.WithInjectEncoding(b3.B3MultipleHeader))
	otel.SetTextMapPropagator(p)

	grpcConfig := grpcserver.NewDefaultConfig()
	grpcConfig.Port = conf.Transport.Grpc.Port
	grpcConfig.LogLevel = conf.GetLogLevel()
	grpcConfig.LogFormatter = conf.GetLogFormatter()
	grpcConfig.AdditionalInterceptors = []grpc.UnaryServerInterceptor{
		xrequestid.AddXRequestID,
		otelgrpc.UnaryServerInterceptor(), //nolint:staticcheck
	}
	go func() {
		err := grpcserver.InitWithConf(registry, &grpcConfig)
		if err != nil {
			log.WithError(err).Fatal("initialising grpc server")
		}
	}()

	wk := http.NewWellKnown(conf.Issuer, conf.JWKSURI, conf.ServerHost+"/token", conf.ServerHost+"/authorize")

	httpServer := http.New(authApp, wk, &conf.Transport.HTTP)
	if err = httpServer.Run(conf.Transport.HTTP.Port); err != nil {
		log.WithError(err).Fatal("running http server")
	}
}

func getAuthApp(conf *config.AuthConfig, jwtClient *jwt.Client) *app.App {
	db, err := repository.InitDB(conf)
	if err != nil {
		log.Warn("unable to connect to database, continuing without database")
	}

	passwordManager := password.NewDefaultManager()
	walletClient := getWalletClient(conf)
	scope := metrics.NewPromScope(prometheus.DefaultRegisterer, "auth")

	identityProvider, err := getIdentityProvider(conf, db, passwordManager)
	if err != nil {
		log.WithError(err).Fatal("unable to get identity provider")
	}

	authApp := app.New(conf,
		db,
		gqlutil.NewSchemaFetcher(gqlutil.DefaultGraphQLClient),
		stats.CreateStats(scope),
		retryhttp.NewCallbackHandler(conf.CallbackMaxRetryAttempts),
		passwordManager,
		jwtClient,
		pseudonym.NewPseudonymizer(conf.PseudonymizationURI),
		walletClient,
		identityProvider,
	)

	return withTokenClientFlowOptions(authApp, conf, identityProvider, db, jwtClient)
}

func withTokenClientFlowOptions(app *app.App, conf *config.AuthConfig, identityProvider contract.IdentityProvider, db *repository.AuthDB, jwtClient contract.JWTClient) *app.App {
	app.WithIdentityProvider(identityProvider)

	if conf.DelegatedIdentityEnabled {
		delegatedIdentityProvider, err := getDelegatedIdentityProvider(identityProvider)
		if err != nil {
			log.WithError(err).Fatal("unable to get delegated identity provider")
		}

		app.WithDelegatedIdentityProvider(delegatedIdentityProvider)
	}

	if scopeValidator, err := getScopeValidator(conf, db); err != nil {
		log.WithError(err).Fatal("unable to get scope validator")
	} else if scopeValidator != nil {
		app.WithScopeValidator(scopeValidator)
	}

	if audienceProvider, err := getAudienceProvider(conf, db); err != nil {
		log.WithError(err).Fatal("unable to get audience provider")
	} else {
		app.WithAudienceProvider(audienceProvider)
	}

	if conf.ClaimsAggregationEnabled {
		claimsAggregator := getClaimsAggregator(conf, jwtClient)
		app.WithAggregateClaims(claimsAggregator)
	}

	if conf.ClaimsValidationEnabled {
		claimsValidator := claimsvalidator.NewOpaClaimsValidator(&conf.ClaimsValidation)
		app.WithClaimsValidator(claimsValidator)
	}

	return app
}

// AuthServiceRegistry implementation of grpc service registry
type AuthServiceRegistry struct {
	servicebase.Registry

	authClient *grpcTransport.Server
}

// RegisterServices register auth service
func (a AuthServiceRegistry) RegisterServices(grpcServer *grpc.Server) {
	pb.RegisterAuthServer(grpcServer, a.authClient)
}

func getWalletClient(conf *config.AuthConfig) walletPB.WalletClient {
	var walletClient walletPB.WalletClient
	if conf.WalletURI != "" {
		walletConnection, err := grpc.NewClient(conf.WalletURI, grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			log.WithError(err).WithField("url", conf.WalletURI).Fatal("unable to dial wallet service")
		}

		walletClient = walletPB.NewWalletClient(walletConnection)
	}

	return walletClient
}

func getAudienceProvider(conf *config.AuthConfig, _ *repository.AuthDB) (contract.AudienceProvider, error) {
	switch conf.AudienceProvider {
	case contract.AudienceProviderTypeRequest:
		return &audienceprovider.RequestAudienceProvider{}, nil
	case contract.AudienceProviderTypeDatabase:
		return audienceprovider.NewDatabaseAudienceProvider(conf), nil
	default:
		return nil, contract.ErrInvalidAudienceProvider
	}
}

func getIdentityProvider(conf *config.AuthConfig, db *repository.AuthDB, passwordManager password.IManager) (contract.IdentityProvider, error) {
	switch conf.IdentityProvider {
	case contract.IdentityProviderTypeCertificate:
		return identityprovider.NewCertificateIdentityProvider(&conf.CertificateIdentityProvider), nil
	case contract.IdentityProviderTypeDatabase:
		return identityprovider.NewDatabaseIdentityProvider(db.ClientDB, passwordManager), nil
	default:
		return nil, contract.ErrInvalidIdentityProvider
	}
}

func getClaimsAggregator(conf *config.AuthConfig, jwtClient contract.JWTClient) contract.ClaimsAggregator {
	if !conf.ClaimsAggregationEnabled {
		return nil
	}

	conf.ClaimsAggregation.Issuer = conf.Issuer

	authenticator := claimsaggregator.NewAuthorizationChainingIAAuthenticator(&conf.ClaimsAggregation, jwtClient)

	return claimsaggregator.NewClaimsAggregator(
		claimsaggregator.NewOpaClaimNames(&conf.ClaimNames),
		&conf.ClaimsAggregation,
		authenticator,
	)
}

// getScopeValidator returns a contract.ScopeValidator or an err. If err is nil then the validator isn't nil.
func getScopeValidator(conf *config.AuthConfig, db *repository.AuthDB) (contract.ScopeValidator, error) {
	switch conf.ScopeValidator {
	case contract.ScopeValidatorTypeOpa:
		return scopevalidator.NewOpaScopeValidator(&conf.OpaScopeValidator), nil
	case contract.ScopeValidatorTypeDatabase:
		return scopevalidator.NewDatabaseScopeValidator(db.ScopeDB), nil
	case contract.ScopeValidatorTypeNone:
		return nil, nil
	default:
		return nil, contract.ErrInvalidScopeValidator
	}
}

func getDelegatedIdentityProvider(identityProvider contract.IdentityProvider) (contract.DelegatedIdentityProvider, error) {
	// Is the identity provider a delegated identity provider.
	if delegatedIdentityProvider, ok := identityProvider.(contract.DelegatedIdentityProvider); ok {
		return delegatedIdentityProvider, nil
	}

	return nil, errors.Wrap(contract.ErrInvalidIdentityProvider, "delegated identity provider not supported")
}
