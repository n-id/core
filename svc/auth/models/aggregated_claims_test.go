package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreateProviderName(t *testing.T) {
	tests := []struct {
		Scenario       string
		ProviderURL    string
		ExpectedResult string
		ExpectedError  error
	}{
		{
			Scenario:       "Valid provider URL",
			ProviderURL:    "https://example.com/provider/123",
			ExpectedResult: "example",
			ExpectedError:  nil,
		},
		{
			Scenario:       "Valid provider URL (with query)",
			ProviderURL:    "https://another-example.com/provider/123?query=123",
			ExpectedResult: "another-example",
			ExpectedError:  nil,
		},
		{
			Scenario:       "Valid provider, same provider already present",
			ProviderURL:    "https://example.com/provider/123",
			ExpectedResult: "example-2",
			ExpectedError:  nil,
		},
		{
			Scenario:       "Valid provider, same provider already present twice",
			ProviderURL:    "https://example.com/provider/123",
			ExpectedResult: "example-3",
			ExpectedError:  nil,
		},
		{
			Scenario:       "Invalid provider URL",
			ProviderURL:    "invalid-url",
			ExpectedResult: "",
			ExpectedError:  ErrInvalidClaimsProviderURL,
		},
		{
			Scenario:       "Invalid provider URL (empty)",
			ProviderURL:    "",
			ExpectedResult: "",
			ExpectedError:  ErrInvalidClaimsProviderURL,
		},
		{
			Scenario:       "Invalid provider URL (no scheme)",
			ProviderURL:    "example.com/provider/123",
			ExpectedResult: "",
			ExpectedError:  ErrInvalidClaimsProviderURL,
		},
	}

	aggregateClaims := &AggregateClaims{
		ClaimSources: make(map[string]*ClaimSource),
	}

	for _, test := range tests {
		t.Run(test.Scenario, func(t *testing.T) {
			result, err := aggregateClaims.CreateProviderName(test.ProviderURL)
			assert.Equal(t, test.ExpectedResult, result)
			assert.ErrorIs(t, err, test.ExpectedError)

			aggregateClaims.ClaimSources[result] = &ClaimSource{}
		})
	}
}
