package models

// OpaResponse is a generic wrapper for an response from OPA.
type OpaResponse[T any] struct {
	DecisionID string `json:"decision_id"`
	Result     T      `json:"result"`
}
