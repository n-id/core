package models

import (
	"net/url"
	"regexp"
	"strings"

	"gitlab.com/n-id/core/pkg/utilities/errors"
)

// ErrInvalidClaimsProviderURL represents an error indicating an invalid claims provider URL.
var ErrInvalidClaimsProviderURL = errors.New("invalid claims provider url")

// AggregateClaims represents a structure for aggregate claims.
type AggregateClaims struct {
	ClaimNames   map[string]string       `json:"_claim_names"`
	ClaimSources map[string]*ClaimSource `json:"_claim_sources"`
}

// ClaimSource represents a structure for a claim source.
type ClaimSource struct {
	JWT string `json:"jwt"`
}

// providerNameRegex is a regular expression pattern used to match provider names ending with a numeric suffix.
var providerNameRegex = regexp.MustCompile(`-\d$`)

// CreateProviderName creates a unique provider name based on the providerURL.
// If the provider name already exists, it appends a numeric suffix to ensure uniqueness.
// TODO: When a provider ends with a -<number>, the number is incremented, which would not be fully correct.
func (a *AggregateClaims) CreateProviderName(providerURL string) (string, error) {
	// Extract domain name from providerUrl
	u, err := url.ParseRequestURI(providerURL)
	if err != nil {
		return "", errors.Wrapf(ErrInvalidClaimsProviderURL, "parsing provider url: %v", err)
	}

	providerName := strings.Split(u.Hostname(), ".")[0]

	// If providerUrl already exists, add a number to the providerName.
	for {
		if _, ok := a.ClaimSources[providerName]; !ok {
			break
		}

		// Check if providerName ends with a "-" followed by a digit
		match := providerNameRegex.MatchString(providerName)
		if match {
			// Extract the last digit from providerName
			lastDigit := providerName[len(providerName)-1]
			// Increment the last digit
			newLastDigit := string(lastDigit + 1)
			// Replace the last digit in providerName
			providerName = providerName[:len(providerName)-1] + newLastDigit
		} else {
			// Append "-2" to providerName
			providerName += "-2"
		}
	}

	return providerName, nil
}
