package models

type OpaRequest[T any] struct {
	Input T `json:"input"`
}
