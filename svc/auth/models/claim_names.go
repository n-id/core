package models

// ClaimNamesMap represents the type returned by a ClaimNames implementation.
// It is a mapping where the keys represent ProviderURLs and the values represent lists of claims associated with each ProviderURL.
type ClaimNamesMap map[string][]string
