package models

import (
	"testing"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/suite"
	"gorm.io/gorm"

	"gitlab.com/n-id/core/pkg/utilities/database/v2"
	"gitlab.com/n-id/core/pkg/utilities/grpctesthelpers"
)

type ClientAddOnTestSuite struct {
	grpctesthelpers.GrpcTestSuite
	db       *gorm.DB
	tx       *gorm.DB
	ClientDB *ClientDB
}

func (s *ClientAddOnTestSuite) SetupTest() {
	s.GrpcTestSuite.SetupTest()
	s.tx = s.db.Begin()
	s.Require().NoError(s.tx.AutoMigrate(GetModels()...))

	s.ClientDB = NewClientDB(s.tx)
}

func (s *ClientAddOnTestSuite) TearDownTest() {
	s.tx.Rollback()
}

func (s *ClientAddOnTestSuite) TearDownSuite() {
	db, err := s.db.DB()
	s.Require().NoError(err)
	s.NoError(db.Close())
}

func (s *ClientAddOnTestSuite) TestGetClientByID() {
	clientID := uuid.Must(uuid.NewV4())
	_, err := s.ClientDB.GetClientByID(clientID.String())
	s.Error(err)
	s.EqualError(err, gorm.ErrRecordNotFound.Error())

	client := &Client{
		ID: clientID,
	}
	err = s.tx.Create(client).Error
	s.Require().NoError(err)

	insertedClient, err := s.ClientDB.GetClientByID(clientID.String())
	s.NoError(err)
	s.Equal(insertedClient.ID, client.ID)
}

func TestClientAddOnTestSuite(t *testing.T) {
	suite.Run(t, &ClientAddOnTestSuite{
		// Intentionally do not supply models to automigrate, this should be done inside the transaction
		db: database.MustConnectTest(databaseName, nil),
	})
}
