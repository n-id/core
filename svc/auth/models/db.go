package models

import (
	"gitlab.com/n-id/core/pkg/generator"
)

func GetModels() []interface{} {
	return []interface{}{
		generator.JWT{},
		AccessModel{},
		Audience{},
		Client{},
		GqlAccessModel{},
		RedirectTarget{},
		RefreshToken{},
		RestAccessModel{},
		Session{},
		User{},
		Scope{},
	}
}
