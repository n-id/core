// Package models provides database functionality
package models

import (
	"strings"

	"github.com/gofrs/uuid"
	"gorm.io/datatypes"
)

// GetOnEmail get user on email
func (m *UserDB) GetOnEmail(email string) (*User, error) {
	var native User
	err := m.Db.Table(m.TableName()).Where("email = ?", strings.ToLower(email)).First(&native).Error
	if err != nil {
		return nil, err
	}

	return &native, nil
}

// WalletUserModel returns the wallet user model
func (m *UserDB) WalletUserModel() *User {
	return &User{
		ID:     uuid.Must(uuid.FromString("7e880dfe-d77a-4477-ade7-66a96f76c0b2")),
		Scopes: datatypes.JSON([]byte("[\"api:access\",\"api:clients:read\"]")),
	}
}
