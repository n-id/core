package app

import (
	"context"
	"testing"

	"gitlab.com/n-id/core/pkg/utilities/errors"
	contractMock "gitlab.com/n-id/core/svc/auth/contract/mock"

	"github.com/golang-jwt/jwt/v5"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/n-id/core/svc/auth/models"
)

func TestWithIdentityProvider(t *testing.T) {
	app := &App{}
	mockIdentityProvider := contractMock.NewIdentityProvider(t)
	clientID := "clientID"

	mockIdentityProvider.EXPECT().GetIdentity(mock.Anything, mock.Anything).Return(clientID, nil).Once()
	app.WithIdentityProvider(mockIdentityProvider)
	assert.Equal(t, 1, len(app.tokenClientFlowOptions))

	claims := &models.TokenClaims{}
	err := app.tokenClientFlowOptions[0](context.Background(), &models.TokenClientFlowRequest{}, claims)
	assert.NoError(t, err)
	assert.Equal(t, clientID, claims.ClientID)
	assert.Equal(t, clientID, claims.Subject)
}

func TestWithIdentityProvider_Error(t *testing.T) {
	app := &App{}
	mockIdentityProvider := contractMock.NewIdentityProvider(t)

	mockIdentityProvider.EXPECT().GetIdentity(mock.Anything, mock.Anything).Return("", errors.New("error")).Once()
	app.WithIdentityProvider(mockIdentityProvider)
	assert.Equal(t, 1, len(app.tokenClientFlowOptions))

	claims := &models.TokenClaims{}
	err := app.tokenClientFlowOptions[0](context.Background(), &models.TokenClientFlowRequest{}, claims)
	assert.Error(t, err)
}

func TestWithDelegateIdentityProvider(t *testing.T) {
	app := &App{}
	mockDelegatedIdentityProvider := contractMock.NewDelegatedIdentityProvider(t)
	delegatedIdentity := "delegated-identity"
	actor := "actor"

	mockDelegatedIdentityProvider.EXPECT().GetDelegatedIdentity(mock.Anything, mock.Anything).Return(delegatedIdentity, nil).Once()
	app.WithDelegatedIdentityProvider(mockDelegatedIdentityProvider)
	assert.Equal(t, 1, len(app.tokenClientFlowOptions))

	claims := &models.TokenClaims{
		RegisteredClaims: jwt.RegisteredClaims{
			// The subject is set to the actor since this is done by the identity provider.
			Subject: actor,
		},
	}
	err := app.tokenClientFlowOptions[0](context.Background(), &models.TokenClientFlowRequest{
		DelegationClaims: models.DelegationClaims{
			Subject: delegatedIdentity, // Delegation.
		},
	}, claims)

	assert.NoError(t, err)
	assert.Equal(t, delegatedIdentity, claims.ClientID)
	assert.Equal(t, delegatedIdentity, claims.Subject)
	assert.Equal(t, actor, claims.Actor.Subject)
}

func TestWithDelegateIdentityProvider_Error(t *testing.T) {
	app := &App{}
	mockDelegatedIdentityProvider := contractMock.NewDelegatedIdentityProvider(t)

	mockDelegatedIdentityProvider.EXPECT().GetDelegatedIdentity(mock.Anything, mock.Anything).Return("", errors.New("error")).Once()
	app.WithDelegatedIdentityProvider(mockDelegatedIdentityProvider)
	assert.Equal(t, 1, len(app.tokenClientFlowOptions))

	claims := &models.TokenClaims{}
	err := app.tokenClientFlowOptions[0](context.Background(), &models.TokenClientFlowRequest{
		DelegationClaims: models.DelegationClaims{
			Subject: "delegated-identity", // Delegation.
		},
	}, claims)
	assert.Error(t, err)
}

func TestWithDelegateIdentityProvider_NoDelegation(t *testing.T) {
	app := &App{}
	mockDelegatedIdentityProvider := contractMock.NewDelegatedIdentityProvider(t)
	app.WithDelegatedIdentityProvider(mockDelegatedIdentityProvider)

	subject := "non-delegated-identity"
	claims := &models.TokenClaims{
		RegisteredClaims: jwt.RegisteredClaims{
			Subject: subject,
		},
	}
	err := app.tokenClientFlowOptions[0](context.Background(), &models.TokenClientFlowRequest{
		DelegationClaims: models.DelegationClaims{
			Subject: "", // No delegation.
		},
	}, claims)
	assert.NoError(t, err)
	assert.Equal(t, subject, claims.Subject)
	assert.Nil(t, err)
}

func TestWithAudienceProvider(t *testing.T) {
	app := &App{}
	mockAudienceProvider := contractMock.NewAudienceProvider(t)
	audience := []string{"audience1"}

	mockAudienceProvider.EXPECT().GetAudience(mock.Anything, mock.Anything, mock.Anything).Return(audience, nil).Once()
	app.WithAudienceProvider(mockAudienceProvider)

	claims := &models.TokenClaims{}
	err := app.tokenClientFlowOptions[0](context.Background(), &models.TokenClientFlowRequest{}, claims)
	assert.NoError(t, err)
	assert.Equal(t, audience[0], claims.Audience[0])
}

func TestWithAudienceProvider_Error(t *testing.T) {
	app := &App{}
	mockAudienceProvider := contractMock.NewAudienceProvider(t)

	mockAudienceProvider.EXPECT().GetAudience(mock.Anything, mock.Anything, mock.Anything).Return(nil, errors.New("error")).Once()
	app.WithAudienceProvider(mockAudienceProvider)
	assert.Equal(t, 1, len(app.tokenClientFlowOptions))

	claims := &models.TokenClaims{}
	err := app.tokenClientFlowOptions[0](context.Background(), &models.TokenClientFlowRequest{}, claims)
	assert.Error(t, err)
}

func TestWithAggregateClaims(t *testing.T) {
	app := &App{}
	mockAggregator := contractMock.NewClaimsAggregator(t)

	aggregateClaims := &models.AggregateClaims{
		ClaimNames: map[string]string{"claim1": "source1"},
		ClaimSources: map[string]*models.ClaimSource{
			"source1": {
				JWT: "jwt1",
			},
		},
	}
	mockAggregator.EXPECT().Aggregate(mock.Anything, mock.Anything).Return(aggregateClaims, nil).Once()
	app.WithAggregateClaims(mockAggregator)
	assert.Equal(t, 1, len(app.tokenClientFlowOptions))

	claims := &models.TokenClaims{}
	err := app.tokenClientFlowOptions[0](context.Background(), &models.TokenClientFlowRequest{}, claims)
	assert.NoError(t, err)
	assert.Equal(t, aggregateClaims, claims.AggregateClaims)
}

func TestWithAggregateClaims_Error(t *testing.T) {
	app := &App{}
	mockAggregator := contractMock.NewClaimsAggregator(t)

	mockAggregator.EXPECT().Aggregate(mock.Anything, mock.Anything).Return(nil, errors.New("error")).Once()
	app.WithAggregateClaims(mockAggregator)
	assert.Equal(t, 1, len(app.tokenClientFlowOptions))

	claims := &models.TokenClaims{}
	err := app.tokenClientFlowOptions[0](context.Background(), &models.TokenClientFlowRequest{}, claims)
	assert.Error(t, err)
}

func TestWithScopeValidator(t *testing.T) {
	tests := []struct {
		scenario       string
		scopes         []string
		validatorError error
		expectError    bool
		expectedScopes []string
	}{
		{
			scenario:       "error response",
			scopes:         []string{"scope1", "scope2"},
			validatorError: errors.New("error"),
			expectError:    true,
			expectedScopes: nil,
		},
		{
			scenario:       "no error, all valid scopes",
			scopes:         []string{"scope1", "scope2"},
			validatorError: nil,
			expectError:    false,
			expectedScopes: []string{"scope1", "scope2"},
		},
		{
			scenario:       "no error, some valid scopes",
			scopes:         []string{"scope1", "scope2"},
			validatorError: nil,
			expectError:    false,
			expectedScopes: []string{"scope1"},
		},
		{
			scenario:       "no error, no valid scopes",
			scopes:         []string{"scope1", "scope2"},
			validatorError: nil,
			expectError:    false,
			expectedScopes: []string{},
		},
		{
			scenario:       "error, some valid scopes",
			scopes:         []string{"scope1", "scope2"},
			validatorError: errors.New("error"),
			expectError:    true,
			expectedScopes: nil,
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			app := &App{}
			mockValidator := contractMock.NewScopeValidator(t)
			mockValidator.EXPECT().Validate(mock.Anything, mock.Anything).Return(test.expectedScopes, test.validatorError).Once()
			app.WithScopeValidator(mockValidator)
			assert.Equal(t, 1, len(app.tokenClientFlowOptions))

			claims := &models.TokenClaims{Scopes: test.scopes}
			err := app.tokenClientFlowOptions[0](context.Background(), &models.TokenClientFlowRequest{}, claims)

			if test.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}

			if test.expectedScopes != nil {
				assert.Equal(t, test.expectedScopes, claims.Scopes)
			} else {
				assert.Nil(t, claims.Scopes)
			}
		})
	}
}

func TestWithClaimsValidator(t *testing.T) {
	tests := []struct {
		scenario       string
		validatorError error
		expectError    bool
	}{
		{
			scenario:       "error response",
			validatorError: errors.New("error"),
			expectError:    true,
		},
		{
			scenario:       "no error",
			validatorError: nil,
			expectError:    false,
		},
	}

	for _, test := range tests {
		t.Run(test.scenario, func(t *testing.T) {
			app := &App{}
			mockValidator := contractMock.NewClaimsValidator(t)

			mockValidator.EXPECT().Validate(mock.Anything, mock.Anything).Return(test.validatorError).Once()
			app.WithClaimsValidator(mockValidator)
			assert.Equal(t, 1, len(app.tokenClientFlowOptions))

			claims := &models.TokenClaims{}
			err := app.tokenClientFlowOptions[0](context.Background(), &models.TokenClientFlowRequest{}, claims)

			if test.expectError {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}
