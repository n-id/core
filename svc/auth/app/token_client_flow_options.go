package app

import (
	"context"

	"gitlab.com/n-id/core/pkg/utilities/log/v2"
	"gitlab.com/n-id/core/svc/auth/contract"
	"gitlab.com/n-id/core/svc/auth/models"
)

// TokenClientFlowOption is a functional option type that allows us to configure the models.TokenClaims.
type TokenClientFlowOption func(ctx context.Context, req *models.TokenClientFlowRequest, claims *models.TokenClaims) error

// WithIdentityProvider adds the given identity provider to the tokenClientFlowOptions to set the
// ClientID and Subject in the claims of the Client Credentials Flow.
func (a *App) WithIdentityProvider(identityProvider contract.IdentityProvider) {
	a.tokenClientFlowOptions = append(a.tokenClientFlowOptions,
		func(ctx context.Context, req *models.TokenClientFlowRequest, claims *models.TokenClaims) error {
			clientID, err := identityProvider.GetIdentity(ctx, &req.Metadata)
			if err != nil {
				log.WithError(err).Error("validating identity")
				return err
			}

			claims.ClientID = clientID
			claims.Subject = clientID

			return nil
		},
	)
}

// WithDelegatedIdentityProvider adds the given delegated identity provider to the tokenClientFlowOptions
// to set the Subject and Actor in the claims of the Client Credentials Flow.
// This requires an identity provider to set the ClientID and Subject.
//
// More information about the Delegation can be found in the Delegation documentation and auth README.
func (a *App) WithDelegatedIdentityProvider(identityProvider contract.DelegatedIdentityProvider) {
	a.tokenClientFlowOptions = append(a.tokenClientFlowOptions,
		func(ctx context.Context, req *models.TokenClientFlowRequest, claims *models.TokenClaims) error {
			// Check if delegation is requested.
			if req.DelegationClaims.Subject == "" {
				return nil
			}

			delegatedIdentity, err := identityProvider.GetDelegatedIdentity(ctx, req)
			if err != nil {
				log.WithError(err).Error("validating identity")
				return err
			}

			claims.Actor = &models.DelegationClaims{
				Subject: claims.Subject,
			}
			claims.ClientID = delegatedIdentity
			claims.Subject = delegatedIdentity

			return nil
		},
	)
}

// WithAudienceProvider adds the given audience provider to the tokenClientFlowOptions to set the
// Audience in the claims of the Client Credentials Flow.
func (a *App) WithAudienceProvider(audienceProvider contract.AudienceProvider) {
	a.tokenClientFlowOptions = append(a.tokenClientFlowOptions,
		func(ctx context.Context, req *models.TokenClientFlowRequest, claims *models.TokenClaims) error {
			audiences, err := audienceProvider.GetAudience(ctx, req, claims.Scopes)
			if err != nil {
				return err
			}

			claims.Audience = audiences

			return nil
		},
	)
}

// WithAggregateClaims adds the given claims aggregator to the tokenClientFlowOptions to set the
// AggregateClaims in the claims of the Client Credentials Flow.
//
// More information about the claims aggregation can be found in the Aggregation documentation.
func (a *App) WithAggregateClaims(aggregateClaimsProvider contract.ClaimsAggregator) {
	a.tokenClientFlowOptions = append(a.tokenClientFlowOptions,
		func(ctx context.Context, _ *models.TokenClientFlowRequest, claims *models.TokenClaims) error {
			aggregateClaims, err := aggregateClaimsProvider.Aggregate(ctx, claims)
			if err != nil {
				return err
			}

			claims.AggregateClaims = aggregateClaims

			return nil
		},
	)
}

// WithScopeValidator adds the given scope validator to the tokenClientFlowOptions to validate the
// scopes in the claims of the Client Credentials Flow.
func (a *App) WithScopeValidator(scopeValidator contract.ScopeValidator) {
	a.tokenClientFlowOptions = append(a.tokenClientFlowOptions,
		func(ctx context.Context, _ *models.TokenClientFlowRequest, claims *models.TokenClaims) error {
			validScopes, err := scopeValidator.Validate(ctx, *claims)
			if err != nil {
				claims.Scopes = nil
				return err
			}

			claims.Scopes = validScopes

			return nil
		},
	)
}

// WithClaimsValidator adds the given token validator to the tokenClientFlowOptions to validate the
// token before it's returned in the Client Credentials Flow.
func (a *App) WithClaimsValidator(claimsValidator contract.ClaimsValidator) {
	a.tokenClientFlowOptions = append(a.tokenClientFlowOptions,
		func(ctx context.Context, _ *models.TokenClientFlowRequest, claims *models.TokenClaims) error {
			err := claimsValidator.Validate(ctx, *claims)
			if err != nil {
				return err
			}

			return nil
		},
	)
}
