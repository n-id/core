package app

import (
	"encoding/json"

	"gitlab.com/n-id/core/svc/auth/models"
)

func setClientMetadataToClaims(session *models.Session) (map[string]interface{}, error) {
	clientMetadataJSON := session.Client.Metadata
	clientMetadata := make(map[string]interface{})
	var err error
	if len(clientMetadataJSON) > 0 {
		err = json.Unmarshal(clientMetadataJSON, &clientMetadata)
	}
	return clientMetadata, err
}
