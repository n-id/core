# Authentication and Authorization (Auth) Service

## Installation

The Auth service can be installed via the [Helm chart](../../charts/auth/README.md) in the `charts` directory.
This is the recommended way to install the Auth service.

This documentation together with the helm chart documentation forms the complete documentation for deploying and configuring the Auth service.

If you wish to run this service without the Helm chart, you can utilize the .env.example file. This file includes all the necessary environment variables needed for configuration.

## Dependencies

The following dependencies are needed for the Auth service to function fully:

- wallet-rpc
- postgres

The Auth service can be deployed without any dependencies but this wil limit the functionality of the service.
Only the following endpoints will be available:

- [[POST] /Token](#client-credentials-flow) - Client Credentials flow

## Delegation

Delegation is a feature that allows a client to request an access token for a specific user.
This is useful when a client wants to act on behalf of a user, but does not have the user's credentials
The client can request an access token for the user, and use that token to access resources on behalf of the user.

> **Note:** Currently delegation is only available for the `certificate` identity provider.

To use delegation, the `DELEGATED_IDENTITY_ENABLED` environment variable must be set to `true`.

As delegation is a feature that allows a client to request an access token for a specific user,
the client must provide the `sub` parameter in the [`access_token`](#request-an-access-token) object of the token request.

## Claims Aggregation

Aggregation is a feature that allows the service to aggregate claims from multiple sources.
nID does not provide any claims itself, but aggregation allows it to fetch claims from an issuing authority.

To use aggregation, the `CLAIMS_AGGREGATION_ENABLED` environment variable must be set to `true`.
Along with setting the values for the claim names provider to know which claims to fetch from which authority, set the `CLAIM_NAMES_URL` and the `CLAIM_NAMES_PATH` environment variables to the url and path to the claim names provider.

To authenticate with the issuing authority, nID uses the [client credentials](#authentication-configuration-for-issuing-authorities) with a self signed nID JWT token to request an `access_token` from the issuing authority using [Authorization Chaining Across Domains](https://datatracker.ietf.org/doc/draft-ietf-oauth-identity-chaining/).

### Authentication configuration for Issuing Authorities

An issuing authority typically needs some kind of authentication before nID can fetch claims from it. Currently, nID supports the OAuth Client Credential flow to fetch an `access_token` from the issuing authority. This access_token is used for successive calls to the `/claims` endpoint. The `token_endpoint` field allows for specifying a custom token endpoint, if the issuing authority does not use the default `/token` endpoint.

For mocking and testing purposes, nID can be configured to use OPA as an issuing authority.
Since OPA inputs need to be formatted in a specific way, the `is_opa` field can be set to `true` to format the input correctly.

To Configure the credentials used per "issuing authority", nID needs the following file `/provider-credentials/provider-credentials.yaml`, the configuration in this file should look like:

```yaml
a-domain.com:
  credentials: [base64 encoded username:password]
  token_endpoint: [issuing authority's token endpoint, defaults to '/token']
  is_opa: [true/false]
another-domain.nl:
  credentials: [base64 encoded username:password]
```

## Client Credentials flow

This flow allows connected clients to request a token for the available scopes/audiences registered
in the system. This flow is mainly intended for Machine-to-Machine communication and is fully
compatible with the [OAuth 2.0 specification](https://datatracker.ietf.org/doc/html/rfc6749#section-3.2).

### Request an access token

An access token can be requested via a POST request to auth.{{host}}/token.
The following parameters must be provided in the body of the token request:

| Parameter    | type                                          | required                       | Description                                   |
| ------------ | --------------------------------------------- | ------------------------------ | --------------------------------------------- |
| grant_type   | string                                        | required                       | Must be the value 'client_credentials'.       |
| scopes       | string                                        | (optional)                     | A space separated list of required scopes.    |
| audience     | string                                        | [(optional)](#database-scopes) | The audience for which the token is intended. |
| access_token | [DelegationParameters](#delegationparameters) | [(optional)](#delegation)      | Delegation parameters.                        |

#### DelegationParameters

Delegation parameters can only be used delegation is enabled.
The following parameters can be provided in the `access_token` object of the token request:

| Parameter | type   | required | Description                       |
| --------- | ------ | -------- | --------------------------------- |
| sub       | string | required | The access token to be validated. |

#### Audience and Scopes

> **Note:** The audience claim is a string that identifies the recipient the JWT is intended for.

Depending on the configuration, the audience claim can be set to a single audience or multiple audiences.
The audience claim is set based on the scopes that are requested in the body of the request, or audience parameter in the body of the request.

> **Note:** Scope is a parameter used to specify the scope of access being requested.

Scopes are set up by the system administrator and can be used to limit the access of a client to certain resources. As nID is only the identity provider, nID does not have any scopes by itself.

##### Database scopes

> **Note:** Database scopes are only enabled when `SCOPE_VALIDATOR` is set to `database`.

Each scope is linked to 1 or more audience(s) in the database. The table below provides an overview of what response can be expected when requesting one or more scopes based on configuration.

| ALLOW_MULTIPLE_AUDIENCES | Scope audience count | Response        |
| ------------------------ | -------------------- | --------------- |
| (default) false          | 1                    | 200 OK          |
| (default) false          | > 1                  | 400 Bad Request |
| true                     | 1                    | 200 OK          |
| true                     | > 1                  | 200 OK          |

If only 1 audience is allowed in a token, and multiple scopes are provided, the client must determine which audience will be included in the token via the `audience` parameter in the body of the request.

## Scope validation

Scope validation is a feature that allows the service to validate the scopes requested by the client.
There are two ways to validate scopes:

- Database scope validation
- OPA scope validation

If no scope validation is required, set the `SCOPE_VALIDATOR` environment variable to an empty string.

## Claims validation

The last step in the token issuance process is the validation of the claims.

OPA is currently the only way to validate claims. To enable OPA claims validation, set the `CLAIMS_VALIDATION_ENABLED` environment variable to `true`, and configure the path and url of the claims validation policy:

- `CLAIMS_VALIDATION_PATH` - The path to the claims validation policy.
- `CLAIMS_VALIDATION_URL` - The URL to the OPA server containing the policy.

### Database scope validation

Database scope validation is a feature that allows the service to validate the scopes requested by the client based on the scopes stored in the database. All requested scopes must be present in the database for the token to be issued.

To enable database scope validation, set the `SCOPE_VALIDATOR` environment variable to `database`.

### OPA scope validation

OPA scope validation is a feature that allows the service to validate the scopes requested by the client based on the policies stored in the OPA server. The Policy decides whether the requested scopes are allowed or not, which scopes are valid and issues any errors if necessary.

To enable OPA scope validation, set the `SCOPE_VALIDATOR` environment variable to `opa`.

## Feature testing

This service employs [Godog](https://github.com/cucumber/godog) for conducting end-to-end tests on its features. Godog, a Go implementation of Cucumber, is utilized for Behavior-Driven Development (BDD). New features can be defined within the `features` folder. If a feature test requires additional functionality, it can be added to the `main_test.go` file, which houses all the necessary code to execute the scenarios within the features.

### Executing the tests

- Start a Postgres database and create a database named `auth`.

```bash
docker run -it -p 5432:5432 -e POSTGRES_PASSWORD=postgres postgres:alpine3.18
```

- Build the features bundle and start the OPA server.

```bash
opa build features/bundle && opa run --server -b bundle.tar.gz
```

- Run the auth server with the `features/.auth.env.example` environment variables.

```bash
    go run ./
    go test
```

These commands will initiate the necessary tests. The test code utilizes the CLUSTER_HOST and TRANSPORT_HTTP_PORT environment variables to perform API requests. Make sure these environment variables are properly set before running the tests.
