package http

// Config contains the configuration for the http transport.
type Config struct {
	Port int
}
