// Package http contains the http transport layer for the jwks service.
package http

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/n-id/core/pkg/interceptor/xrequestid"
	"gitlab.com/n-id/core/pkg/utilities/log/v2"
	"gitlab.com/n-id/core/svc/jwks/contract"
)

// Server contains the http server for the jwks service.
type Server struct {
	app  contract.App
	conf *Config
}

// New returns a new instance of the http server.
func New(app contract.App, conf *Config) *Server {
	return &Server{
		app:  app,
		conf: conf,
	}
}

// Run runs the http server.
func (s *Server) Run() error {
	app := gin.New()
	app.Use(
		gin.LoggerWithWriter(gin.DefaultWriter, "/v1/health"),
		gin.Recovery(),
		func(c *gin.Context) {
			if c.Request.URL.Path == "/v1/health" {
				c.Next()
				return
			}

			xRequestID := c.GetHeader("x-request-id")
			ctx := context.WithValue(c.Request.Context(), xrequestid.XRequestID, xRequestID)
			c.Request = c.Request.WithContext(ctx)

			c.Next()
		},
	)

	app.GET("/jwks", s.jwks).
		GET("/v1/health", func(c *gin.Context) {
			c.Status(http.StatusOK)
		})

	return app.Run(fmt.Sprintf(":%d", s.conf.Port))
}

func (s *Server) jwks(c *gin.Context) {
	jwks, err := s.app.Jwks(c.Request.Context())
	if err != nil {
		log.Error(err)
		_ = c.AbortWithError(500, contract.ErrInternalError) //nolint:errcheck
		return
	}

	c.JSON(200, jwks)
}
