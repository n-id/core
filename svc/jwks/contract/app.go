// Package contract defines the interfaces for the auth service
package contract

import (
	"context"

	"github.com/lestrrat-go/jwx/v2/jwk"
)

// App represents the application interface for the jwks service.
type App interface {
	Jwks(context.Context) (*jwk.Set, error)
}
