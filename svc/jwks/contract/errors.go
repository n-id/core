package contract

import "errors"

// Common errors used in the contract package.
var (
	// ErrInternalError represents an internal error.
	ErrInternalError = errors.New("internal error")
	// ErrReadingPublicKeyDirectory indicates an error while reading the public key directory.
	ErrReadingPublicKeyDirectory = errors.New("error reading public key directory")
	// ErrParsingPublicKey indicates an error while parsing the public key.
	ErrParsingPublicKey = errors.New("error parsing public key")
	// ErrJwksNotInitialized indicates that the JSON Web Key Set (JWKS) is not initialised.
	ErrJwksNotInitialized = errors.New("jwks not initialised")
)
