// Package app app domain for the jwks service
package app

import (
	"context"

	"github.com/lestrrat-go/jwx/v2/jwk"
	"gitlab.com/n-id/core/svc/jwks/contract"
	"gitlab.com/n-id/core/svc/jwks/internal/config"
)

// Jwks jwks service struct
type Jwks struct {
	conf *config.Config
	jwks *jwk.Set
}

// NewJwks creates a new jwks app, and reads the public keys from the configured directory.
func NewJwks(conf *config.Config, jwks *jwk.Set) *Jwks {
	return &Jwks{
		conf: conf,
		jwks: jwks,
	}
}

// Jwks reads the public keys from the configured directory and returns them as a jwk set.
func (a *Jwks) Jwks(_ context.Context) (*jwk.Set, error) {
	if a.jwks == nil {
		return nil, contract.ErrJwksNotInitialized
	}

	return a.jwks, nil
}
