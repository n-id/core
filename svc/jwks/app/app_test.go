package app

import (
	"context"
	"testing"

	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/stretchr/testify/assert"
	"gitlab.com/n-id/core/svc/jwks/contract"
)

func TestJwks_Jwks_nil(t *testing.T) {
	jwks := Jwks{
		conf: nil,
		jwks: nil,
	}

	got, err := jwks.Jwks(context.Background())
	assert.Nil(t, got)
	assert.ErrorIs(t, err, contract.ErrJwksNotInitialized)
}

func TestJwks_Jwks_not_nil(t *testing.T) {
	set := jwk.NewSet()
	jwks := Jwks{
		conf: nil,
		jwks: &set,
	}

	got, err := jwks.Jwks(context.Background())
	assert.NotNil(t, got)
	assert.Nil(t, err)
}
