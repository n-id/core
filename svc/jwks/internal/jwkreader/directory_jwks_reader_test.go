package jwkreader

import (
	"log"
	"os"
	"path/filepath"
	"syscall"
	"testing"

	"github.com/lestrrat-go/jwx/v2/jwk"
	"github.com/stretchr/testify/assert"
	"gitlab.com/n-id/core/svc/jwks/contract"
)

func Test_readKeyFiles_error(t *testing.T) {
	tests := map[string]struct {
		dirPath string
		err     error
	}{
		"Invalid directory": {
			dirPath: "invalid_directory",
			err:     contract.ErrReadingPublicKeyDirectory,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			_, err := readKeyFiles(test.dirPath)
			assert.ErrorIs(t, err, test.err)
		})
	}
}

func Test_readKeyFiles(t *testing.T) {
	tests := map[string]struct {
		files    []string
		expected map[string]string
		err      error
	}{
		"No files": {
			files:    []string{},
			expected: map[string]string{},
			err:      nil,
		},
		"Single file": {
			files: []string{
				"test_file",
			},
			expected: map[string]string{},
			err:      nil,
		},
		"Multiple files": {
			files: []string{
				"test_file",
				"test_file_2",
			},
			expected: map[string]string{},
			err:      nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			dir, err := os.MkdirTemp("", "readKeyFiles")
			assert.NoError(t, err)

			// Create files in temp directory.
			for _, file := range test.files {
				file1, err := os.CreateTemp(dir, file)
				assert.NoError(t, err)

				defer func(path string) {
					err := syscall.Unlink(path)
					if err != nil {
						log.Fatal(err)
					}
				}(file1.Name())

				if test.expected != nil {
					// Replace key in expected map.
					test.expected[filepath.Base(file1.Name())] = test.expected[file]
					delete(test.expected, file)
				}
			}

			got, err := readKeyFiles(dir)

			assert.ErrorIs(t, err, test.err)
			assert.Equal(t, test.expected, got)
		})
	}
}

func Test_parsePublicKeys(t *testing.T) {
	validKey1 := "-----BEGIN PUBLIC KEY-----\nMIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgGTGkFFaMyVLIiM+0TvxJkwHRFBP\nL//7HU39p6b7U/nCKWk2++VbY0ajWGzLtXixKCOivaVy/1z/9RmiomGAY3UhTmq9\nV0NNZ75QKfevv0HMlc4eaYSOY+nEcBjBL9IxykJQUiUXjBpV+5MNcbc/aT+GW32d\nnoNVn8rkqGDnHnL9AgMBAAE=\n-----END PUBLIC KEY-----"
	validKey2 := "-----BEGIN PUBLIC KEY-----\nMIGeMA0GCSqGSIb3DQEBAQUAA4GMADCBiAKBgG6emG/R5ETKE1ubqHTA6wfP30Dg\nIAy529jhsKkYpsbYN7YjUvuZ19uVfkniAD2Zl78k71ng/EJdslftY44wFwvn0ax0\nhkOJmzZqLDipZ9Svyda0CBUChZU/cszHwlHLIepBBtXqnLNMqHOmjKkNozuUvxl6\nKfBXCu/0JcXwnqf3AgMBAAE=\n-----END PUBLIC KEY-----"

	parsedKey1, err := jwk.ParseKey([]byte(validKey1), jwk.WithPEM(true))
	assert.NoError(t, err)
	parsedKey1.Set(jwk.KeyIDKey, "key1")
	parsedKey1.Set(jwk.AlgorithmKey, "RS256")

	parsedKey2, err := jwk.ParseKey([]byte(validKey2), jwk.WithPEM(true))
	assert.NoError(t, err)
	parsedKey2.Set(jwk.KeyIDKey, "key2")
	parsedKey2.Set(jwk.AlgorithmKey, "RS256")

	tests := map[string]struct {
		keys map[string]string
		want map[string]jwk.Key
		err  error
	}{
		"No keys": {
			keys: map[string]string{},
			want: map[string]jwk.Key{},
			err:  nil,
		},
		"Valid keys": {
			keys: map[string]string{
				"key1": validKey1,
			},
			want: map[string]jwk.Key{
				"key1": parsedKey1,
			},
			err: nil,
		},
		"Invalid key": {
			keys: map[string]string{
				"key1": "invalid key",
			},
			want: nil,
			err:  contract.ErrParsingPublicKey,
		},
		"Multiple valid keys": {
			keys: map[string]string{
				"key1": validKey1,
				"key2": validKey2,
			},
			want: map[string]jwk.Key{
				"key1": parsedKey1,
				"key2": parsedKey2,
			},
			err: nil,
		},
		"Multiple keys, one invalid": {
			keys: map[string]string{
				"key1": validKey1,
				"key2": "invalid key",
			},
			want: nil,
			err:  contract.ErrParsingPublicKey,
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := parsePublicKeys(test.keys)

			assert.ErrorIs(t, err, test.err)
			assert.Equal(t, test.want, got)
		})
	}
}
