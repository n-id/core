// Package jwkreader reader for the jwk data
package jwkreader

import (
	"os"
	"path/filepath"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwk"
	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/svc/jwks/contract"
)

// DirectoryJwksReader reads the public keys from the configured directory.
type DirectoryJwksReader struct {
	conf *DirectoryJwksReaderConfig
}

// DirectoryJwksReaderConfig contains the configuration for the DirectoryJwksReader.
type DirectoryJwksReaderConfig struct {
	// Path contains the path to the directory containing the public keys.
	Path string
}

// NewDirectoryJwksReader creates a new DirectoryJwksReader.
func NewDirectoryJwksReader(conf *DirectoryJwksReaderConfig) *DirectoryJwksReader {
	return &DirectoryJwksReader{
		conf: conf,
	}
}

// Jwks reads the public keys from the configured directory and returns them as a jwk set.
func (r *DirectoryJwksReader) Jwks() (*jwk.Set, error) {
	keys, err := readKeyFiles(r.conf.Path)
	if err != nil {
		return nil, err
	}

	parsedKeys, err := parsePublicKeys(keys)
	if err != nil {
		return nil, err
	}

	jwks := jwk.NewSet()
	for _, key := range parsedKeys {
		_ = jwks.AddKey(key) //nolint:errcheck
	}

	return &jwks, nil
}

// readKeyFiles reads the public key files from the configured directory.
func readKeyFiles(dirPath string) (map[string]string, error) {
	files, err := os.ReadDir(dirPath)
	if err != nil {
		return nil, contract.ErrReadingPublicKeyDirectory
	}

	keys := make(map[string]string)
	for _, file := range files {
		filePath := filepath.Join(dirPath, file.Name())
		stat, err := os.Stat(filePath)
		if err != nil {
			return nil, err
		}

		if stat.IsDir() {
			continue
		}

		keyID := filepath.Base(file.Name())
		keyBytes, err := os.ReadFile(filepath.Clean(filePath))
		if err != nil {
			return nil, err
		}

		keys[keyID] = string(keyBytes)
	}

	return keys, nil
}

// parsePublicKeys parses the public keys from the key files.
func parsePublicKeys(keys map[string]string) (map[string]jwk.Key, error) {
	parsedKeys := make(map[string]jwk.Key)
	for keyID, keyBytes := range keys {
		key, err := jwk.ParseKey([]byte(keyBytes), jwk.WithPEM(true))
		if err != nil {
			return nil, errors.WithSecondaryError(contract.ErrParsingPublicKey, err)
		}

		alg := key.Algorithm()
		if alg.String() != "" {
			_ = key.Set(jwk.AlgorithmKey, alg) //nolint:errcheck
		} else {
			switch key.KeyType() { //nolint:exhaustive
			case jwa.RSA:
				_ = key.Set(jwk.AlgorithmKey, jwa.RS256) //nolint:errcheck
			case jwa.EC:
				_ = key.Set(jwk.AlgorithmKey, jwa.ES256) //nolint:errcheck
			}
		}

		_ = key.Set(jwk.KeyIDKey, keyID) //nolint:errcheck
		parsedKeys[keyID] = key
	}

	return parsedKeys, nil
}
