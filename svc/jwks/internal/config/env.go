// Package config contains the configuration for the jwks service.
package config

import (
	"gitlab.com/n-id/core/svc/jwks/internal/jwkreader"
	"gitlab.com/n-id/core/svc/jwks/transport/http"
)

// Config configuration for the jwks service
type Config struct {
	DirectoryReader jwkreader.DirectoryJwksReaderConfig
	// Transport contains the configuration for the transport.
	Transport TransportConfig
}

// TransportConfig contains the configuration for the transport.
type TransportConfig struct {
	HTTP http.Config
}
