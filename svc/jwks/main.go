// Package main instantiates the jwks service
package main

import (
	"github.com/vrischmann/envconfig"
	"gitlab.com/n-id/core/pkg/utilities/log/v2"
	"gitlab.com/n-id/core/svc/jwks/app"
	"gitlab.com/n-id/core/svc/jwks/internal/config"
	"gitlab.com/n-id/core/svc/jwks/internal/jwkreader"
	"gitlab.com/n-id/core/svc/jwks/transport/http"
)

func main() {
	var conf config.Config
	err := envconfig.Init(&conf)
	if err != nil {
		log.WithError(err).Fatal("unable to load config from environment")
	}

	reader := jwkreader.NewDirectoryJwksReader(&conf.DirectoryReader)
	jwks, err := reader.Jwks()
	if err != nil {
		log.WithError(err).Fatal("unable to read jwks")
	}

	app := app.NewJwks(&conf, jwks)

	httpServer := http.New(app, &conf.Transport.HTTP)
	err = httpServer.Run()
	if err != nil {
		log.WithError(err).Fatal("unable to run http server")
	}
}
