// Package graphql
package graphql

import (
	"context"
	"time"

	"gitlab.com/n-id/core/pkg/generator"
	"gitlab.com/n-id/core/pkg/jwtconfig"

	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/extension"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"gorm.io/gorm"
)

type Resolver struct {
	DB     *gorm.DB
	JWTKey *jwtconfig.JWTKey
}

var _ ResolverRoot = &Resolver{}

type Directive interface {
	HasFieldAccess(ctx context.Context, obj interface{}, next graphql.Resolver) (interface{}, error)
}

func NewResolver(db *gorm.DB, jwtKey *jwtconfig.JWTKey) *Resolver {
	return &Resolver{DB: db, JWTKey: jwtKey}
}

func (r *Resolver) DefaultConfig() Config {
	config := Config{Resolvers: r}
	clientDir := r.Client().(Directive)
	config.Directives.HasClientAccess = clientDir.HasFieldAccess
	return config
}

func (c Config) NewDefaultHandler() *handler.Server {
	return c.NewDefaultHandlerWithExtraOptions(NewDefaultExtraOptions())
}

type ExtraOptions struct {
	Logger generator.ErrorLogger
}

func NewDefaultExtraOptions() *ExtraOptions {
	return &ExtraOptions{
		Logger: &generator.LogrusErrorLogger{},
	}
}

func (c Config) NewDefaultHandlerWithExtraOptions(extraOptions *ExtraOptions) *handler.Server {
	schema := NewExecutableSchema(c)
	srv := handler.New(schema)
	presenter := generator.NewPresenter(extraOptions.Logger)
	srv.SetErrorPresenter(presenter.Present)
	srv.Use(extension.Introspection{})
	srv.AddTransport(transport.Websocket{
		KeepAlivePingInterval: 10 * time.Second,
	})
	srv.AddTransport(transport.Options{})
	srv.AddTransport(transport.GET{})
	srv.AddTransport(transport.POST{})
	srv.AddTransport(transport.MultipartForm{})
	return srv
}

func (r *Resolver) Mutation() MutationResolver {
	return &mutationResolver{r}
}

func (r *Resolver) Query() QueryResolver {
	return &queryResolver{r}
}

func (r *Resolver) Client() ClientResolver {
	return &clientResolver{r, &CustomClientHooks{r}}
}

type mutationResolver struct{ *Resolver }

var _ MutationResolver = &mutationResolver{nil}

type queryResolver struct{ *Resolver }

var _ QueryResolver = &queryResolver{nil}
