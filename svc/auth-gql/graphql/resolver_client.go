package graphql

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/n-id/core/pkg/generator"

	"github.com/99designs/gqlgen/graphql"
	"github.com/gofrs/uuid"
	"gitlab.com/n-id/core/svc/auth-gql/auth"
	"gitlab.com/n-id/core/svc/auth/models"
	"gorm.io/gorm"
)

type clientResolver struct {
	*Resolver
	Hooks *CustomClientHooks
}

var _ ClientResolver = &clientResolver{nil, nil}

type customClientHooks interface{}

type CustomClientHooks struct{ *Resolver }

var _ customClientHooks = &CustomClientHooks{nil}

func (r *queryResolver) constructFilterClient(ctx context.Context, joins map[string]string) (filter string, values []interface{}, restrictedFields []string) {
	fieldCtx := graphql.GetFieldContext(ctx)
	filters := []string{fmt.Sprintf("/*path: %v*/ /*fallback*/false", fieldCtx.Path())}

	// Scope: 'api:clients:read', Relation: None
	if auth.UserHasScope(ctx, "api:clients:read") {
		filters = append(filters, "/*api:clients:read None*/ true")
	}

	return strings.Join(filters, " OR "), values, restrictedFields
}

func (r *queryResolver) readFilterClient(ctx context.Context, filter *ClientFilterInput, joins map[string]string) (*gorm.DB, error) {
	db := r.Resolver.DB.Model(&models.Client{})

	for _, v := range joins {
		db = db.Joins(v)
	}

	filters, values, restrictedFields := r.constructFilterClient(ctx, joins)
	db = db.Where(filters, values...)

	if filter != nil {
		for _, f := range restrictedFields {
			if filter.containsField(f) {
				return nil, fmt.Errorf("%w: %s", generator.ErrFieldAccessDenied, generator.GraphFieldName(f))
			}
		}
		expr, args := filter.parse()
		db = db.Where(expr, args...)
		if filter.Unscoped != nil && *filter.Unscoped {
			db = db.Unscoped()
		}
	}

	return db, nil
}

func (r *queryResolver) Client(ctx context.Context, id uuid.UUID) (*models.Client, error) {
	m := models.Client{}
	db, err := r.readFilterClient(ctx, nil, nil)
	if err != nil {
		return nil, err
	}
	err = db.Where(`"clients"."id" = ?`, id).First(&m).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, generator.ErrRecordNotFound
		}
		return nil, generator.WrapAsInternal(err, fmt.Sprintf("getting Client record with id %s from db", id))
	}
	return &m, nil
}

func (r *queryResolver) Clients(ctx context.Context, limit *int, offset *int, filter *ClientFilterInput, orderBy *string, order ClientFieldName, orderDirection OrderDirection) ([]*models.Client, error) {
	var m []*models.Client
	db, err := r.readFilterClient(ctx, filter, nil)
	if err != nil {
		return nil, err
	}

	orderString, err := generator.ParseOrderBy(ctx, orderBy, &order, &orderDirection)
	if err != nil {
		return nil, generator.WrapAsInternal(err, "parsing order by")
	}

	err = db.Limit(*limit).Offset(*offset).Order(orderString).Find(&m).Error
	if err != nil {
		return nil, generator.WrapAsInternal(err, "getting Clients from db")
	}
	return m, nil
}

func (r *clientResolver) ID(_ context.Context, obj *models.Client) (uuid.UUID, error) {
	return obj.ID, nil
}

var _ Directive = &clientResolver{nil, nil}

func (r *clientResolver) HasFieldAccess(ctx context.Context, obj interface{}, next graphql.Resolver) (interface{}, error) {
	model := castClient(obj)
	if model == nil {
		return nil, generator.ErrAccessDenied
	}
	fieldName := graphql.GetFieldContext(ctx).Field.Name

	// Scope: 'api:clients:read', Relation: None
	if auth.UserHasScope(ctx, "api:clients:read") {
		if map[string]bool{
			"color":           true,
			"createdAt":       true,
			"icon":            true,
			"id":              true,
			"logo":            true,
			"metadata":        true,
			"name":            true,
			"password":        true,
			"redirectTargets": true,
			"updatedAt":       true,
		}[fieldName] {
			return next(ctx)
		}
	}

	return nil, generator.ErrAccessDenied
}

func castClient(obj interface{}) *models.Client {
	switch res := obj.(type) {
	case **models.Client:
		return *res
	case *models.Client:
		return res
	case models.Client:
		return &res
	default:
		return nil
	}
}
