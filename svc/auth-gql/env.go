package main

import (
	"gitlab.com/n-id/core/pkg/environment"
)

// AuthGQLConfig implements the used environment variables
type AuthGQLConfig struct {
	environment.BaseConfig
	GqlPlaygroundEnabled bool   `envconfig:"GQL_PLAYGROUND_ENABLED"`
	JWTKeyPath           string `envconfig:"JWT_PATH_DIRECTORY"`
}
