// Package auth-gql
package main

import (
	"strconv"

	"gitlab.com/n-id/core/pkg/jwtconfig"
	"gorm.io/gorm"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gin-gonic/gin"
	"github.com/vrischmann/envconfig"

	"gitlab.com/n-id/core/pkg/httpserver"
	"gitlab.com/n-id/core/pkg/utilities/database/v2"
	"gitlab.com/n-id/core/pkg/utilities/log/v2"
	"gitlab.com/n-id/core/svc/auth-gql/auth"
	"gitlab.com/n-id/core/svc/auth-gql/graphql"
	"gitlab.com/n-id/core/svc/auth/models"
)

func main() {
	log.Info("Auth-gql initialising")

	conf := getConf()

	jwtKey := getJWTKey(conf)
	db := getDatabase(conf)

	resolver := graphql.NewResolver(db, jwtKey)

	runGinHandler(resolver, conf)
}

func runGinHandler(resolver *graphql.Resolver, conf *AuthGQLConfig) {
	serverOpts := httpserver.DefaultServerOptions()
	serverOpts.UseLogMiddleware = false

	r := httpserver.NewGinServerWithOpts(serverOpts)
	h := handler.NewDefaultServer(graphql.NewExecutableSchema(resolver.DefaultConfig()))
	ginHandler := auth.CheckServiceAccount(resolver.DB, h, models.NewUserDB(resolver.DB).WalletUserModel(), "wallet", conf.Namespace)

	r.POST("/gql", ginHandler)
	r.GET("/gql", ginHandler)
	if conf.GqlPlaygroundEnabled {
		r.GET("/", playgroundHandler())
	}

	log.Infof("Auth-gql running on port %d", conf.Port)
	log.Fatal(r.Run(":" + strconv.Itoa(conf.Port)))
}

func getDatabase(conf *AuthGQLConfig) *gorm.DB {
	db := database.MustConnectCustomWithCustomLogger(
		&database.DBConfig{
			TestMode:       database.TestModeOff,
			AutoMigrate:    false,
			RetryOnFailure: true,
			Extensions:     nil,
			LogMode:        conf.LogMode,
			User:           conf.PGUser,
			Host:           conf.PGHost,
			Port:           conf.PGPort,
			Pass:           conf.PGPass,
			DBName:         "auth",
		},
		models.GetModels(),
		log.GetLogger(),
	)
	return db
}

func getJWTKey(conf *AuthGQLConfig) *jwtconfig.JWTKey {
	jwtKey, err := jwtconfig.Read(conf.JWTKeyPath)
	if err != nil {
		log.WithError(err).Fatal("reading JWT key")
	}

	return jwtKey
}

func getConf() *AuthGQLConfig {
	conf := &AuthGQLConfig{}
	if err := envconfig.Init(&conf); err != nil {
		log.WithError(err).Fatal("unable to load environment config")
	}

	err := log.SetFormat(log.Format(conf.LogFormat))
	if err != nil {
		log.WithError(err).Fatal("unable to set log format")
	}

	return conf
}

// Defining the Playground handler
func playgroundHandler() gin.HandlerFunc {
	h := playground.Handler("GraphQL", "/gql")

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}
