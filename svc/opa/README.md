# Open Policy Agent (OPA) Service

This service extends the default [OPA](https://www.openpolicyagent.org/docs/latest/) service.
By extending [OPA](https://www.openpolicyagent.org/docs/latest/extensions/) we can add custom functionality to the OPA service for both logging and custom functions for the policies.

## Custom Functions

### HTTP Send Many

The `http.send_many` function is a custom function that allows the policy to send multiple HTTP requests in parallel.
This function is useful when you need to send multiple requests to different services and you don't want to wait for each request to complete before sending the next one.
The api for this function is an array of the same input as the `http.send` function.

```rego
http.send_many([
    {
        "method": "GET",
        "url": "http://service1:8080",
        "headers": {
            "x-custom-header": "value"
        }
    },
    {
        "method": "POST",
        "url": "http://service2:8080",
        "body": {
            "key": "value"
        }
    }
])
```
