// Package extensions contains the extensions for the opa service.
package extensions

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"sync"

	"github.com/open-policy-agent/opa/ast"
	"github.com/open-policy-agent/opa/rego"
	"github.com/open-policy-agent/opa/topdown"
	"github.com/open-policy-agent/opa/topdown/builtins"
	"github.com/open-policy-agent/opa/types"
	"github.com/open-policy-agent/opa/util"
	"golang.org/x/sync/errgroup"
)

func httpSendManyFuncDecl() *rego.Function {
	return &rego.Function{
		Name: "http.send_many",
		Decl: types.NewFunction(
			types.Args(
				types.Named("requests", types.NewAny(
					types.NewSet(types.NewObject(nil, types.NewDynamicProperty(types.S, types.A))),
					types.NewArray(nil, types.NewObject(nil, types.NewDynamicProperty(types.S, types.A))),
				)),
			),
			types.Named("response",
				types.NewArray(nil, types.NewObject(nil, types.NewDynamicProperty(types.A, types.A))),
			),
		),
		Description:      "Returns a HTTP response to the given HTTP request.",
		Nondeterministic: true,
	}
}

// RegisterHTTPSendMany Registers `http.send_many([...])` to the rego runtime.
func RegisterHTTPSendMany() {
	rego.RegisterBuiltin1(httpSendManyFuncDecl(), httpSendMany)
}

func httpSendMany(bctx rego.BuiltinContext, requests *ast.Term) (*ast.Term, error) {
	arr, err := builtins.ArrayOperand(requests.Value, 1)
	if err != nil {
		return nil, err
	}

	opaCtx := bctx.Context
	res := []*ast.Term{}
	var mutex sync.Mutex
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	g, ctx := errgroup.WithContext(ctx)
	bctx.Context = ctx

	arr.Foreach(func(t *ast.Term) {
		g.Go(func() error {
			obj, err := builtins.ObjectOperand(t.Value, 0)
			if err != nil {
				return handleHTTPSendErr(bctx, err)
			}

			req, err := buildRequest(bctx, obj)
			if err != nil {
				return handleHTTPSendErr(bctx, err)
			}

			// Send the request.
			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				return handleHTTPSendErr(bctx, err)
			}
			var deferErr error
			defer func() {
				deferErr = resp.Body.Close()
			}()

			respValue, err := formatHTTPResponseToAST(bctx, obj, resp)
			if err != nil {
				return err
			}

			mutex.Lock()
			res = append(res, ast.NewTerm(respValue))
			mutex.Unlock()

			return deferErr
		})
	})

	if err := g.Wait(); err != nil {
		bctx.Context = opaCtx
		cancel()
		return nil, err
	}

	return ast.NewTerm(ast.NewArray(res...)), err
}

func buildRequest(bctx rego.BuiltinContext, obj ast.Object) (*http.Request, error) {
	var url string
	var method string
	var body *bytes.Buffer
	var customHeaders map[string]interface{}

	for _, val := range obj.Keys() {
		key, err := ast.JSON(val.Value)
		if err != nil {
			return nil, err
		}

		key = key.(string)

		var strVal string

		if s, ok := obj.Get(val).Value.(ast.String); ok {
			strVal = strings.Trim(string(s), "\"")
		} else {
			// Most parameters are strings, so consolidate the type checking.
			switch key {
			case "method",
				"url",
				"raw_body":
				return nil, fmt.Errorf("%q must be a string", key)
			}
		}

		switch key {
		case "method":
			method = strings.ToUpper(strVal)
		case "url":
			err := verifyURLHost(bctx, strVal)
			if err != nil {
				return nil, err
			}
			url = strVal
		case "body":
			bodyVal := obj.Get(val).Value
			bodyValInterface, err := ast.JSON(bodyVal)
			if err != nil {
				return nil, err
			}

			bodyValBytes, err := json.Marshal(bodyValInterface)
			if err != nil {
				return nil, err
			}
			body = bytes.NewBuffer(bodyValBytes)
		case "headers":
			headersVal := obj.Get(val).Value
			headersValInterface, err := ast.JSON(headersVal)
			if err != nil {
				return nil, err
			}
			var ok bool
			customHeaders, ok = headersValInterface.(map[string]interface{})
			if !ok {
				return nil, fmt.Errorf("invalid type for headers key")
			}
		}
	}

	if body == nil {
		body = bytes.NewBufferString("")
	}

	req, err := http.NewRequestWithContext(bctx.Context, method, url, body)
	if err != nil {
		return nil, err
	}
	// Add custom headers
	if len(customHeaders) != 0 {
		customHeaders = canonicalizeHeaders(customHeaders)

		for k, v := range customHeaders {
			header, ok := v.(string)
			if !ok {
				return nil, fmt.Errorf("invalid type for headers value %q", v)
			}

			req.Header.Add(k, header)
		}

		// Don't overwrite or append to one that was set in the custom headers
		if _, hasUA := customHeaders["User-Agent"]; !hasUA {
			req.Header.Add("User-Agent", "Open Policy Agent")
		}

		// If the caller specifies the Host header, use it for the HTTP
		// request host and the TLS server name.
		if host, hasHost := customHeaders["Host"]; hasHost {
			host := host.(string) // We already checked that it's a string.
			req.Host = host
		}
	}

	return req, nil
}

func verifyURLHost(bctx rego.BuiltinContext, unverifiedURL string) error {
	// Eager return to avoid unnecessary URL parsing
	if bctx.Capabilities == nil || bctx.Capabilities.AllowNet == nil {
		return nil
	}

	parsedURL, err := url.Parse(unverifiedURL)
	if err != nil {
		return err
	}

	host := strings.Split(parsedURL.Host, ":")[0]

	return verifyHost(bctx, host)
}

func verifyHost(bctx rego.BuiltinContext, host string) error {
	if bctx.Capabilities == nil || bctx.Capabilities.AllowNet == nil {
		return nil
	}

	for _, allowed := range bctx.Capabilities.AllowNet {
		if allowed == host {
			return nil
		}
	}

	return fmt.Errorf("unallowed host: %s", host)
}

// canonicalizeHeaders returns a copy of the headers where the keys are in
// canonical HTTP form.
func canonicalizeHeaders(headers map[string]interface{}) map[string]interface{} {
	canonicalized := map[string]interface{}{}

	for k, v := range headers {
		canonicalized[http.CanonicalHeaderKey(k)] = v
	}

	return canonicalized
}

func formatHTTPResponseToAST(bctx rego.BuiltinContext, req ast.Object, resp *http.Response) (ast.Value, error) {
	forceJSONDecode, err := getBoolValFromReqObj(req, ast.StringTerm("force_json_decode"))
	if err != nil {
		return nil, handleHTTPSendErr(bctx, err)
	}
	forceYAMLDecode, err := getBoolValFromReqObj(req, ast.StringTerm("force_yaml_decode"))
	if err != nil {
		return nil, handleHTTPSendErr(bctx, err)
	}

	resultRawBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return prepareASTResult(resp.Header, forceJSONDecode, forceYAMLDecode, resultRawBody, resp.Status, resp.StatusCode)
}

func handleHTTPSendErr(bctx rego.BuiltinContext, err error) error {
	// Return HTTP client timeout errors in a generic error message to avoid confusion about what happened.
	// Do not do this if the builtin context was cancelled and is what caused the request to stop.
	if urlErr, ok := err.(*url.Error); ok && urlErr.Timeout() && bctx.Context.Err() == nil {
		err = fmt.Errorf("%s %s: request timed out", urlErr.Op, urlErr.URL)
	}
	if err := bctx.Context.Err(); err != nil {
		return topdown.Halt{
			Err: &topdown.Error{
				Code:    topdown.CancelErr,
				Message: fmt.Sprintf("http.send_many: timed out (%s)", err.Error()),
			},
		}
	}
	return handleBuiltinErr(ast.HTTPSend.Name, bctx.Location, err)
}

func handleBuiltinErr(name string, loc *ast.Location, err error) error {
	switch err := err.(type) {
	case nil:
		return nil
	case *topdown.Error, topdown.Halt:
		return err
	case builtins.ErrOperand:
		e := &topdown.Error{
			Code:     topdown.TypeErr,
			Message:  fmt.Sprintf("%v: %v", name, err.Error()),
			Location: loc,
		}
		return e.Wrap(err)
	default:
		e := &topdown.Error{
			Code:     topdown.BuiltinErr,
			Message:  fmt.Sprintf("%v: %v", name, err.Error()),
			Location: loc,
		}
		return e.Wrap(err)
	}
}

func getBoolValFromReqObj(req ast.Object, key *ast.Term) (bool, error) {
	var b ast.Boolean
	var ok bool
	if v := req.Get(key); v != nil {
		if b, ok = v.Value.(ast.Boolean); !ok {
			return false, fmt.Errorf("invalid value for %v field", key.String())
		}
	}
	return bool(b), nil
}

func prepareASTResult(headers http.Header, forceJSONDecode, forceYAMLDecode bool, body []byte, status string, statusCode int) (ast.Value, error) {
	var resultBody interface{}

	// If the response body cannot be JSON/YAML decoded,
	// an error will not be returned. Instead, the "body" field
	// in the result will be null.
	switch {
	case forceJSONDecode || isContentType(headers, "application/json"):
		_ = util.UnmarshalJSON(body, &resultBody) //nolint:errcheck
	case forceYAMLDecode || isContentType(headers, "application/yaml", "application/x-yaml"):
		_ = util.Unmarshal(body, &resultBody) //nolint:errcheck
	}

	result := make(map[string]interface{})
	result["status"] = status
	result["status_code"] = statusCode
	result["body"] = resultBody
	result["raw_body"] = string(body)
	result["headers"] = getResponseHeaders(headers)

	resultObj, err := ast.InterfaceToValue(result)
	if err != nil {
		return nil, err
	}

	return resultObj, nil
}

func getResponseHeaders(headers http.Header) map[string]interface{} {
	respHeaders := map[string]interface{}{}
	for headerName, values := range headers {
		var respValues []interface{}
		for _, v := range values {
			respValues = append(respValues, v)
		}
		respHeaders[strings.ToLower(headerName)] = respValues
	}
	return respHeaders
}

func isContentType(header http.Header, typ ...string) bool {
	for _, t := range typ {
		if strings.Contains(header.Get("Content-Type"), t) {
			return true
		}
	}
	return false
}
