package extensions

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/open-policy-agent/opa/rego"
	"github.com/stretchr/testify/assert"
	"gitlab.com/n-id/core/pkg/utilities/errors"
)

type testResponse struct {
	method  string
	status  int
	body    []byte
	headers map[string]string
}

type expectedResponse struct {
	status  string
	body    interface{}
	rawBody string
	headers map[string]string
}

func TestA(t *testing.T) {
	module := `
		package http_send_many_test
		
		import rego.v1

		hostname := "%s"

		requests := [req | 
			some input_req in input.requests
			req := {
				"url": concat("", [hostname, input_req.url]),
				"method": input_req.method,
				"body": input_req.body,
				"headers": input_req.headers
			}
		]
	
		resp_many := http.send_many(requests)
	`

	tests := map[string]struct {
		input     map[string]interface{}
		responses map[string]testResponse
		expected  []expectedResponse
	}{
		"no_requests": {
			input: map[string]interface{}{
				"requests": []map[string]string{},
			},
			responses: map[string]testResponse{},
			expected:  []expectedResponse{},
		},
		"one_valid_request": {
			input: map[string]interface{}{
				"requests": []map[string]interface{}{
					{
						"method":  "GET",
						"url":     "/one-valid-request",
						"headers": map[string]string{},
						"body":    "valid request one",
					},
				},
			},
			responses: map[string]testResponse{
				"/one-valid-request": {
					method:  "GET",
					status:  200,
					body:    []byte("valid request one"),
					headers: map[string]string{},
				},
			},
			expected: []expectedResponse{
				{
					status:  "200 OK",
					rawBody: "valid request one",
					headers: map[string]string{
						"req-id": "/one-valid-request",
					},
				},
			},
		},
		"two_valid_requests": {
			input: map[string]interface{}{
				"requests": []map[string]interface{}{
					{
						"method":  "GET",
						"url":     "/one-valid-request",
						"headers": map[string]string{},
						"body":    "",
					},
					{
						"method":  "POST",
						"url":     "/two-valid-request",
						"headers": map[string]string{},
						"body":    "",
					},
				},
			},
			responses: map[string]testResponse{
				"/one-valid-request": {
					status:  200,
					method:  "GET",
					body:    []byte("request 1"),
					headers: map[string]string{},
				},
				"/two-valid-request": {
					status:  200,
					method:  "POST",
					body:    []byte("request 2"),
					headers: map[string]string{},
				},
			},
			expected: []expectedResponse{
				{
					status:  "200 OK",
					rawBody: "request 1",
					headers: map[string]string{
						"req-id": "/one-valid-request",
					},
				},
				{
					status:  "200 OK",
					rawBody: "request 2",
					headers: map[string]string{
						"req-id": "/two-valid-request",
					},
				},
			},
		},
		"one_valid_one_not_ok_request": {
			input: map[string]interface{}{
				"requests": []map[string]interface{}{
					{
						"method":  "GET",
						"url":     "/one-valid-request",
						"headers": map[string]string{},
						"body":    "",
					},
					{
						"method":  "GET",
						"url":     "/two-not-ok-request",
						"headers": map[string]string{},
						"body":    "",
					},
				},
			},
			responses: map[string]testResponse{
				"/one-valid-request": {
					method:  "GET",
					status:  200,
					body:    []byte("request 1"),
					headers: map[string]string{},
				},
				"/two-not-ok-request": {
					method:  "GET",
					status:  500,
					body:    []byte("request 2"),
					headers: map[string]string{},
				},
			},
			expected: []expectedResponse{
				{
					status:  "200 OK",
					rawBody: "request 1",
					headers: map[string]string{
						"req-id": "/one-valid-request",
					},
				},
				{
					status:  "500 Internal Server Error",
					rawBody: "request 2",
					headers: map[string]string{
						"req-id": "/two-not-ok-request",
					},
				},
			},
		},
		"one_invalid_request": {
			input: map[string]interface{}{
				"requests": []map[string]interface{}{
					{
						"method":  "GET",
						"url":     "+(this is not right)+/invalid-request",
						"headers": map[string]string{},
						"body":    "",
					},
				},
			},
			responses: map[string]testResponse{},
			expected:  []expectedResponse{},
		},
		"one_valid_one_invalid_request": {
			input: map[string]interface{}{
				"requests": []map[string]interface{}{
					{
						"method":  "GET",
						"url":     "/valid-request",
						"headers": map[string]string{},
						"body":    "",
					},
					{
						"method":  "GET",
						"url":     "+(this is not right)+/invalid-request",
						"headers": map[string]string{},
						"body":    "",
					},
				},
			},
			responses: map[string]testResponse{
				"/valid-request": {
					method:  "GET",
					status:  200,
					body:    []byte("request 1"),
					headers: map[string]string{},
				},
			},
			expected: []expectedResponse{},
		},
		"one_valid_request_json_response": {
			input: map[string]interface{}{
				"requests": []map[string]interface{}{
					{
						"method":  "GET",
						"url":     "/one-valid-request",
						"headers": map[string]string{},
						"body":    "valid request one",
					},
				},
			},
			responses: map[string]testResponse{
				"/one-valid-request": {
					method: "GET",
					status: 200,
					body:   []byte("{\"hello\":\"world\"}"),
					headers: map[string]string{
						"Content-Type": "application/json",
					},
				},
			},
			expected: []expectedResponse{
				{
					status:  "200 OK",
					rawBody: "{\"hello\":\"world\"}",
					body: map[string]interface{}{
						"hello": "world",
					},
					headers: map[string]string{
						"req-id": "/one-valid-request",
					},
				},
			},
		},
	}

	for name, test := range tests {
		server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			value, found := test.responses[r.URL.String()]
			if !assert.True(t, found, "unrecognised request to test server") {
				t.FailNow()
			}
			assert.Equal(t, value.method, r.Method)

			for header, value := range value.headers {
				w.Header().Set(header, value)
			}

			w.Header().Set("req-id", r.URL.String())
			w.WriteHeader(value.status)
			w.Write(value.body)
		}))

		t.Run(name, func(t *testing.T) {
			responses, err := testEval(fmt.Sprintf(module, server.URL), test.input)

			assert.NoError(t, err)
			assert.Len(t, responses, len(test.expected))
			for _, expected := range test.expected {
				res := responseInSlice(expected.headers["req-id"], responses)
				if !assert.NotNil(t, res) {
					t.FailNow()
				}
				headers := getHeadersMap(res)

				assert.Equal(t, res["status"], expected.status)
				if rawBody, ok := res["raw_body"].(string); ok {
					assert.Equal(t, rawBody, expected.rawBody)
				}

				for header, value := range expected.headers {
					assert.Contains(t, headers, header)
					assert.Equal(t, headers[header], value)
				}

				if expected.body != nil {
					assert.Equal(t, res["body"], expected.body)
				}
			}
		})
	}
}

func getHeadersMap(h map[string]interface{}) map[string]string {
	h, ok := h["headers"].(map[string]interface{})
	if !ok {
		return nil
	}

	res := map[string]string{}
	for key, value := range h {
		items, ok := value.([]interface{})
		if !ok {
			continue
		}
		res[key] = items[0].(string)
	}

	return res
}

func responseInSlice(id string, responses []map[string]interface{}) map[string]interface{} {
	for _, r := range responses {
		headers := getHeadersMap(r)
		if headers == nil {
			continue
		}
		if headers["req-id"] == id {
			return r
		}
	}

	return nil
}

func testEval(module string, input map[string]interface{}) ([]map[string]interface{}, error) {
	ctx := context.TODO()
	query, err := rego.New(
		rego.Query("resp_many = data.http_send_many_test.resp_many"),
		rego.Module("http_send_many_test.rego", module),
		rego.Function1(httpSendManyFuncDecl(), httpSendMany),
	).PrepareForEval(ctx)

	if err != nil {
		return nil, errors.Wrap(err, "preparing evaluation")
	}

	results, err := query.Eval(ctx, rego.EvalInput(input))
	if err != nil {
		return nil, errors.Wrap(err, "evaluating module")
	} else if len(results) == 0 {
		return []map[string]interface{}{}, nil
	}
	resultSlice, ok := results[0].Bindings["resp_many"].([]interface{})
	if !ok {
		return []map[string]interface{}{}, nil
	}

	result := []map[string]interface{}{}
	for _, resp := range resultSlice {
		res, ok := resp.(map[string]interface{})
		if !ok {
			return []map[string]interface{}{}, nil
		}
		result = append(result, res)
	}

	return result, nil
}
