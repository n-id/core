// Package opa extends the Open Policy Agent with custom functionality.
package main

import (
	"os"

	opa_envoy_plugin "github.com/open-policy-agent/opa-envoy-plugin/plugin"
	"github.com/open-policy-agent/opa/cmd"
	"github.com/open-policy-agent/opa/runtime"
	"gitlab.com/n-id/core/pkg/utilities/log/v2"
	"gitlab.com/n-id/core/svc/opa/internal/extensions"
)

var version = ""

func main() {
	runtime.RegisterPlugin(opa_envoy_plugin.PluginName, opa_envoy_plugin.Factory{})
	extensions.RegisterHTTPSendMany()

	err := os.Setenv("OPA_RUNTIME_VERSION", version)
	if err != nil {
		log.Fatal(err)
	}

	if err := cmd.RootCommand.Execute(); err != nil {
		log.Errorln(err)
		os.Exit(1)
	}
}
