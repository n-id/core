package main

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"google.golang.org/grpc/metadata"

	"gitlab.com/n-id/core/svc/luarunner/internal"
	"gitlab.com/n-id/core/svc/luarunner/internal/config"

	"google.golang.org/grpc/credentials/insecure"

	"github.com/gin-gonic/gin"
	"github.com/vrischmann/envconfig"
	"google.golang.org/grpc"

	"gitlab.com/n-id/core/pkg/httpserver"
	"gitlab.com/n-id/core/pkg/utilities/database/v2"
	"gitlab.com/n-id/core/pkg/utilities/grpcserver/dial"
	"gitlab.com/n-id/core/pkg/utilities/log/v2"
	authProto "gitlab.com/n-id/core/svc/auth/transport/grpc/proto"
	"gitlab.com/n-id/core/svc/luarunner/models"
)

type contextKeyType string

const ctxB3Key = contextKeyType("B3Trace")

func main() {
	var conf config.Config
	if err := envconfig.Init(&conf); err != nil {
		log.WithError(err).Fatal("unable to load configuration from environment")
	}

	err := log.SetFormat(log.Format(conf.LogFormat))
	if err != nil {
		log.WithError(err).Fatal("unable to set log format")
	}

	authDB := database.MustConnectCustomWithCustomLogger(&database.DBConfig{
		Host:           conf.PGHost,
		User:           conf.PGUser,
		Pass:           conf.PGPass,
		Port:           conf.PGPort,
		RetryOnFailure: true,
		TestMode:       database.TestModeOff,
		DBName:         "auth",
		LogMode:        false,
		AutoMigrate:    false,
		Extensions:     []string{"uuid-ossp"},
	}, models.GetModels(), log.GetLogger())

	conn, err := getGRPCClient("auth", conf.AuthGrpcPort)
	if err != nil {
		log.WithError(err).Fatal("connecting with auth service")
	}

	db := database.MustConnectCustom(&database.DBConfig{
		Host:           conf.PGHost,
		User:           conf.PGUser,
		Pass:           conf.PGPass,
		Port:           conf.PGPort,
		RetryOnFailure: true,
		TestMode:       database.TestModeOff,
		TimeOut:        int(time.Minute.Seconds()),
		DBName:         "luarunner",
		LogMode:        false,
		AutoMigrate:    true,
		Extensions:     []string{"uuid-ossp", "pg_trgm", "postgis"},
	}, models.GetModels())

	luaRunnerDB := internal.NewLuaRunnerDB(db)
	if err != nil {
		log.WithError(err).Fatal("setting up luarunner database")
	}

	authClient := authProto.NewAuthClient(conn)
	luaRunnerService := internal.NewLuaRunnerService(authDB, &authClient, luaRunnerDB)

	server := httpserver.NewGinServer()
	server.Use(func(c *gin.Context) {
		headers := c.Request.Header

		ctx := context.WithValue(c.Request.Context(), ctxB3Key, map[string]string{
			"x-b3-traceid":      headers["X-B3-Traceid"][0],
			"x-b3-spanid":       headers["X-B3-Spanid"][0],
			"x-b3-parentspanid": headers["X-B3-Parentspanid"][0],
			"x-request-id":      headers["X-Request-Id"][0],
		})

		c.Request = c.Request.WithContext(ctx)
		c.Next()
	})

	server.POST("/callback", func(c *gin.Context) {
		luaRunnerService.HTTPCallback(c.Writer, c.Request)
	})

	server.POST("/:organisation/:script/run", luaRunnerService.Run)

	err = server.Run(":" + strconv.Itoa(conf.Port))
	if err != nil {
		panic(err)
	}
}

func getGRPCClient(service string, port string) (*grpc.ClientConn, error) {
	return dial.Service(fmt.Sprintf("%s:%s", service, port), grpc.WithTransportCredentials(
		insecure.NewCredentials()),
		grpc.WithChainUnaryInterceptor(
			func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
				b3Trace := ctx.Value(ctxB3Key).(map[string]string)

				for k := range b3Trace {
					ctx = metadata.AppendToOutgoingContext(ctx, k, b3Trace[k])
				}

				return invoker(ctx, method, req, reply, cc, opts...)
			},
		),
	)
}
