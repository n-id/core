// Package config provides the configuration for the claims aggregator.
package config

import "gitlab.com/n-id/core/pkg/environment"

type Config struct {
	environment.BaseConfig

	AuthGrpcPort string `env:"AUTH_GRPC_PORT" default:"8080"`
}
