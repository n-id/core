// Package onboardingscopes defines the scopes needed for onboarding.
package onboardingscopes

// DataSourceServiceConvertBSNToPseudonym contains the scope for ConvertBSNToPseudonym in the DataSourceService service
const DataSourceServiceConvertBSNToPseudonym = "convertbsn"

// GetAllScopes retrieve all available scopes
func GetAllScopes() []string {
	return []string{
		DataSourceServiceConvertBSNToPseudonym,
	}
}
