package main

import (
	"fmt"
	"testing"

	gqlClientMock "gitlab.com/n-id/core/pkg/gqlclient/mock"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"

	metadataMock "gitlab.com/n-id/core/pkg/utilities/grpcserver/headers/mock"
	"gitlab.com/n-id/core/pkg/utilities/grpctesthelpers"
	onboardingPB "gitlab.com/n-id/core/svc/onboarding/proto"
	pseudoMock "gitlab.com/n-id/core/svc/pseudonymization/mock"
	pseudoPB "gitlab.com/n-id/core/svc/pseudonymization/proto"
)

var ErrAllesIsLek = fmt.Errorf("alles is lek")

type DatasourceServiceTestSuite struct {
	grpctesthelpers.GrpcTestSuite

	dataSourceServiceServer *DataSourceServiceServer
}

func (s *DatasourceServiceTestSuite) SetupTest() {
	s.GrpcTestSuite.SetupTest()
	walletClientMock := &gqlClientMock.Client{}
	walletClientMock.On("Post", mock.Anything, mock.Anything, mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		response := args.Get(2).(*Response)
		response.Users = []PseudoResponse{{Pseudonym: "somepseudonym"}}
	})

	pseudoClientMock := &pseudoMock.PseudonymizerClient{}
	convertResponse := &pseudoPB.ConvertResponse{
		Conversions: make(map[string][]byte),
	}
	convertResponse.Conversions["somepseudonym"] = []byte("resultpseudonym")
	pseudoClientMock.On("Convert", mock.Anything, mock.Anything).Return(convertResponse, nil)

	metadataMock := &metadataMock.GRPCMetadataHelperMock{}
	metadataMock.On("GetValFromCtx", s.Ctx, mock.Anything).Return("By=spiffe://cluster.local/ns/foo/sa/httpbin;Hash=<redacted>;Subject=\"\";URI=spiffe://cluster.local/ns/foo/sa/sleep", nil)

	s.dataSourceServiceServer = &DataSourceServiceServer{
		walletClient:           walletClientMock,
		pseudonimizationClient: pseudoClientMock,
		metadataHelper:         metadataMock,
	}
}

func (s *DatasourceServiceTestSuite) TestTranslateBSN() {
	res, err := s.dataSourceServiceServer.ConvertBSNToPseudonym(s.Ctx, &onboardingPB.ConvertMessage{
		Bsn: "1234567890",
	})
	s.Require().NoError(err)
	s.Equal("resultpseudonym", string(res.Pseudonym))
}

func (s *DatasourceServiceTestSuite) TestCantGetPseudonymForBSN() {
	walletClientMock := &gqlClientMock.Client{}
	walletClientMock.On("Post", mock.Anything, mock.Anything, mock.Anything).Return(ErrAllesIsLek)
	s.dataSourceServiceServer.walletClient = walletClientMock
	res, err := s.dataSourceServiceServer.ConvertBSNToPseudonym(s.Ctx, &onboardingPB.ConvertMessage{
		Bsn: "1234567890",
	})
	s.Require().Error(err)
	s.Nil(res)
}

func (s *DatasourceServiceTestSuite) TestCantConvertPseudonym() {
	pseudoClientMock := &pseudoMock.PseudonymizerClient{}
	pseudoClientMock.On("Convert", mock.Anything, mock.Anything).Return(nil, ErrAllesIsLek)
	s.dataSourceServiceServer.pseudonimizationClient = pseudoClientMock
	res, err := s.dataSourceServiceServer.ConvertBSNToPseudonym(s.Ctx, &onboardingPB.ConvertMessage{
		Bsn: "1234567890",
	})
	s.Require().Error(err)
	s.Nil(res)
}

func (s *DatasourceServiceTestSuite) TestPseudonymNotReturned() {
	pseudoClientMock := &pseudoMock.PseudonymizerClient{}
	convertResponse := &pseudoPB.ConvertResponse{
		Conversions: make(map[string][]byte),
	}
	pseudoClientMock.On("Convert", mock.Anything, mock.Anything).Return(convertResponse, nil)
	s.dataSourceServiceServer.pseudonimizationClient = pseudoClientMock
	res, err := s.dataSourceServiceServer.ConvertBSNToPseudonym(s.Ctx, &onboardingPB.ConvertMessage{
		Bsn: "1234567890",
	})
	s.Require().Error(err)
	s.Nil(res)
}

func TestDatasourceServiceTestSuite(t *testing.T) {
	suite.Run(t, &DatasourceServiceTestSuite{})
}
