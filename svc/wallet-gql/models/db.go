package models

import (
	"gitlab.com/n-id/core/pkg/generator"
)

func GetModels() []interface{} {
	return []interface{}{
		generator.JWT{},
		Client{},
		Consent{},
		Device{},
		EmailAddress{},
		PhoneNumber{},
		User{},
	}
}
