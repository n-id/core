package models

import (
	"errors"

	"gorm.io/gorm"
)

// GetByCode gets device by code
func (m *DeviceDB) GetByCode(code string, preloadUser bool) (*Device, error) {
	query := m.Db
	if preloadUser {
		query = query.Preload("User")
	}

	var device Device
	err := query.Where("code = ?", code).First(&device).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, err
	}

	return &device, err
}
