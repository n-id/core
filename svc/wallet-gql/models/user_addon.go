// Package models provides database functionality
package models

import (
	"errors"

	"gorm.io/gorm"
)

// GetByBsn gets a user by bsn
func (m *UserDB) GetByBsn(bsn string) (*User, error) {
	var user User
	err := m.Db.Where("bsn = ?", bsn).First(&user).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, err
	}

	return &user, err
}

// GetByPseudo gets a user by pseudo
func (m *UserDB) GetByPseudo(pseudo string) (*User, error) {
	var user User
	err := m.Db.Where("pseudonym = ?", pseudo).First(&user).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, err
	}

	return &user, err
}
