// Package wallet-gql
package main

import (
	"strconv"

	"gitlab.com/n-id/core/pkg/jwtconfig"
	"gitlab.com/n-id/core/svc/wallet-gql/graphql"

	"gitlab.com/n-id/core/svc/wallet-gql/cors"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/gin-gonic/gin"
	messagebird "github.com/messagebird/go-rest-api"
	"github.com/vrischmann/envconfig"

	"gitlab.com/n-id/core/pkg/httpserver"
	"gitlab.com/n-id/core/pkg/utilities/database/v2"
	"gitlab.com/n-id/core/pkg/utilities/log/v2"
	"gitlab.com/n-id/core/svc/wallet-gql/auth"
	"gitlab.com/n-id/core/svc/wallet-gql/models"
	postmarkUtils "gitlab.com/n-id/core/svc/wallet-gql/postmark"
)

func initialise() (*graphql.Resolver, *WalletConfig, error) {
	// Init conf
	conf := &WalletConfig{}
	if err := envconfig.Init(&conf); err != nil {
		log.WithError(err).Fatal("unable to load environment config")
		return nil, nil, err
	}

	err := log.SetFormat(log.Format(conf.LogFormat))
	if err != nil {
		log.WithError(err).Fatal("unable to set log format")
	}

	log.Info("Wallet-gql initialising")

	graphql.MessageBirdClient = messagebird.New(conf.Messagebird)
	graphql.PostmarkClient = postmarkUtils.NewClient(conf.Postmark.API, conf.Postmark.Account)
	graphql.AuthorizationURI = conf.AuthorizationURI

	// Connect to db
	db := database.MustConnectCustomWithCustomLogger(
		&database.DBConfig{
			TestMode:       database.TestModeOff,
			AutoMigrate:    false,
			RetryOnFailure: true,
			Extensions:     nil,
			LogMode:        conf.LogMode,
			User:           conf.PGUser,
			Host:           conf.PGHost,
			Port:           conf.PGPort,
			Pass:           conf.PGPass,
			DBName:         "wallet",
		},
		models.GetModels(),
		log.GetLogger(),
	)

	jwtKey, err := jwtconfig.Read(conf.JWTKeyPath)
	if err != nil {
		return nil, nil, err
	}

	return graphql.NewResolver(db, jwtKey), conf, nil
}

func main() {
	resolver, conf, err := initialise()
	if err != nil {
		log.WithError(err).Fatal("tried to initialise wallet gql")
		return
	}

	serverOpts := httpserver.DefaultServerOptions()
	serverOpts.UseLogMiddleware = false

	r := httpserver.NewGinServerWithOpts(serverOpts)

	r.POST("/gql", gqlHandler(resolver))
	r.GET("/gql", gqlHandler(resolver))

	log.Infof("Wallet-gql running on port %d", conf.Port)
	log.Fatal(r.Run(":" + strconv.Itoa(conf.Port)))
}

// Defining the Graphql handler
func gqlHandler(resolver *graphql.Resolver) gin.HandlerFunc {
	// NewExecutableSchema and Config are in the generated.go file
	// Resolver is in the resolver.go file
	h := cors.Cors(cors.ReflectOrigin())(auth.NewCustomIstioAuthMiddleware(resolver.DB)(handler.NewDefaultServer(graphql.NewExecutableSchema(resolver.DefaultConfig()))))

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}
