// Package graphql graphql server for wallet-gql
package graphql

import (
	"context"
	"time"

	"gitlab.com/n-id/core/pkg/jwtconfig"

	"github.com/99designs/gqlgen/graphql"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/extension"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"gitlab.com/n-id/core/pkg/generator"
	"gorm.io/gorm"
)

// Resolver stores data for the gql resolver
type Resolver struct {
	DB     *gorm.DB
	JWTKey *jwtconfig.JWTKey
}

var _ ResolverRoot = &Resolver{}

// Directive defines an interface for handling GraphQL directives.
type Directive interface {
	HasFieldAccess(ctx context.Context, obj interface{}, next graphql.Resolver) (res interface{}, err error)
}

// NewResolver returns new instance of Resolver
func NewResolver(db *gorm.DB, jwt *jwtconfig.JWTKey) *Resolver {
	return &Resolver{DB: db, JWTKey: jwt}
}

// DefaultConfig returns the default configuration for the Resolver.
func (r *Resolver) DefaultConfig() Config {
	config := Config{Resolvers: r}
	clientDir := r.Client().(Directive)
	consentDir := r.Consent().(Directive)
	emailAddressDir := r.EmailAddress().(Directive)
	phoneNumberDir := r.PhoneNumber().(Directive)
	hasUserAccess := r.User().(Directive)
	config.Directives.HasClientAccess = clientDir.HasFieldAccess
	config.Directives.HasConsentAccess = consentDir.HasFieldAccess
	config.Directives.HasEmailAddressAccess = emailAddressDir.HasFieldAccess
	config.Directives.HasPhoneNumberAccess = phoneNumberDir.HasFieldAccess
	config.Directives.HasUserAccess = hasUserAccess.HasFieldAccess
	return config
}

// NewDefaultHandler returns a new instance of the gql handler
func (c Config) NewDefaultHandler() *handler.Server {
	return c.NewDefaultHandlerWithExtraOptions(NewDefaultExtraOptions())
}

// ExtraOptions extra parameters for the resolver
type ExtraOptions struct {
	Logger generator.ErrorLogger
}

// NewDefaultExtraOptions creates new default ExtraOptions.
func NewDefaultExtraOptions() *ExtraOptions {
	return &ExtraOptions{
		Logger: &generator.LogrusErrorLogger{},
	}
}

// NewDefaultHandlerWithExtraOptions creates a new default handler with extra options.
func (c Config) NewDefaultHandlerWithExtraOptions(extraOptions *ExtraOptions) *handler.Server { //nolint:hugeParam
	schema := NewExecutableSchema(c)
	srv := handler.New(schema)
	presenter := generator.NewPresenter(extraOptions.Logger)
	srv.SetErrorPresenter(presenter.Present)
	srv.Use(extension.Introspection{})
	srv.AddTransport(transport.Websocket{
		KeepAlivePingInterval: 10 * time.Second,
	})
	srv.AddTransport(transport.Options{})
	srv.AddTransport(transport.GET{})
	srv.AddTransport(transport.POST{})
	srv.AddTransport(transport.MultipartForm{})
	return srv
}

// Mutation returns a resolver for mutations.
func (r *Resolver) Mutation() MutationResolver {
	return &mutationResolver{r}
}

// Query returns a resolver for queries.
func (r *Resolver) Query() QueryResolver {
	return &queryResolver{r}
}

// Client returns a resolver for clients.
func (r *Resolver) Client() ClientResolver {
	return &clientResolver{r, &CustomClientHooks{r}}
}

// Consent returns a resolver for consent.
func (r *Resolver) Consent() ConsentResolver {
	return &consentResolver{r, &CustomConsentHooks{r}}
}

// EmailAddress returns a resolver for email addresses.
func (r *Resolver) EmailAddress() EmailAddressResolver {
	return &emailAddressResolver{r, &CustomEmailAddressHooks{r}}
}

// PhoneNumber returns a resolver for phone numbers.
func (r *Resolver) PhoneNumber() PhoneNumberResolver {
	return &phoneNumberResolver{r, &CustomPhoneNumberHooks{r}}
}

// User returns a resolver for users.
func (r *Resolver) User() UserResolver {
	return &userResolver{r, &CustomUserHooks{r}}
}

type mutationResolver struct{ *Resolver }

var _ MutationResolver = &mutationResolver{nil}

type queryResolver struct{ *Resolver }

var _ QueryResolver = &queryResolver{nil}
