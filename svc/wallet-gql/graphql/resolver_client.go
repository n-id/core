package graphql

import (
	"context"
	"fmt"
	"strings"

	"github.com/99designs/gqlgen/graphql"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"gitlab.com/n-id/core/pkg/generator"
	"gitlab.com/n-id/core/svc/wallet-gql/auth"
	"gitlab.com/n-id/core/svc/wallet-gql/models"
	"gorm.io/gorm"
)

type clientResolver struct {
	*Resolver
	Hooks *CustomClientHooks
}

var _ ClientResolver = &clientResolver{nil, nil}

type customClientHooks interface{}

type CustomClientHooks struct{ *Resolver }

var _ customClientHooks = &CustomClientHooks{nil}

func (r *mutationResolver) CreateClient(ctx context.Context, input CreateClient) (*models.Client, error) {
	var err error
	var m models.Client

	hasCreateAccess := false

	// Scope: 'api:admin', Relation: None
	if auth.UserHasScope(ctx, "api:admin") {
		if !input.containsField("DeletedAt") {
			hasCreateAccess = true
		}
	}

	if !hasCreateAccess {
		return nil, generator.ErrAccessDenied
	}

	err = r.DB.Transaction(func(tx *gorm.DB) error {
		if m, err = input.ToModel(ctx, r.Resolver); err != nil {
			return errors.Wrap(err, "converting input CreateClient to model")
		}
		create := tx.Create(&m)
		if err := create.Error; err != nil {
			return generator.WrapAsInternal(err, "creating Client")
		}
		if err := create.First(&m).Error; err != nil {
			return generator.WrapAsInternal(err, "getting result from create of Client")
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return &m, nil
}

func (r *mutationResolver) UpdateClient(ctx context.Context, id uuid.UUID, input UpdateClient) (*models.Client, error) {
	var err error
	var m models.Client
	fieldCtx := graphql.GetFieldContext(ctx)

	filters := []string{fmt.Sprintf("/*path: %v*/ /*fallback*/false", fieldCtx.Path())}
	var values []interface{}
	var restrictedFields []string

	// Scope: 'api:admin', Relation: None
	if auth.UserHasScope(ctx, "api:admin") {
		filters = append(filters, "/*api:admin None*/ true")
	}

	for _, f := range restrictedFields {
		if input.containsField(f) {
			return nil, fmt.Errorf("%w: %s", generator.ErrFieldAccessDenied, generator.GraphFieldName(f))
		}
	}

	err = r.DB.Transaction(func(tx *gorm.DB) error {
		m, err = input.ToModel(ctx, r.Resolver)
		if err != nil {
			return errors.Wrap(err, "converting input UpdateClient to model")
		}
		m.ID = id
		changes := generator.ExtractChanges(ctx, m)
		update := tx.Model(&m).Where(strings.Join(filters, " OR "), values...).Updates(changes)
		if err := update.Error; err != nil {
			return generator.WrapAsInternal(err, fmt.Sprintf("updating Client %v", m.ID))
		}
		if update.RowsAffected == 0 {
			return fmt.Errorf("%w (type=Client,id=%v)", generator.ErrRecordNotFound, m.ID)
		}
		if err := update.First(&m).Error; err != nil {
			return generator.WrapAsInternal(err, "getting update result")
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return &m, nil
}

func (r *queryResolver) constructFilterClient(ctx context.Context, joins map[string]string) (filter string, values []interface{}, restrictedFields []string) {
	fieldCtx := graphql.GetFieldContext(ctx)
	filters := []string{fmt.Sprintf("/*path: %v*/ /*fallback*/false", fieldCtx.Path())}

	// Scope: 'api:access', Relation: None
	if auth.UserHasScope(ctx, "api:access") {
		filters = append(filters, "/*api:access None*/ true")
	}

	return strings.Join(filters, " OR "), values, restrictedFields
}

func (r *queryResolver) readFilterClient(ctx context.Context, filter *ClientFilterInput, joins map[string]string) (*gorm.DB, error) {
	db := r.Resolver.DB.Model(&models.Client{})

	for _, v := range joins {
		db = db.Joins(v)
	}

	filters, values, restrictedFields := r.constructFilterClient(ctx, joins)
	db = db.Where(filters, values...)

	if filter != nil {
		for _, f := range restrictedFields {
			if filter.containsField(f) {
				return nil, fmt.Errorf("%w: %s", generator.ErrFieldAccessDenied, generator.GraphFieldName(f))
			}
		}
		expr, args := filter.parse()
		db = db.Where(expr, args...)
		if filter.Unscoped != nil && *filter.Unscoped {
			db = db.Unscoped()
		}
	}

	return db, nil
}

func (r *queryResolver) Client(ctx context.Context, id uuid.UUID) (*models.Client, error) {
	m := models.Client{}
	db, err := r.readFilterClient(ctx, nil, nil)
	if err != nil {
		return nil, err
	}
	err = db.Where(`"clients"."id" = ?`, id).First(&m).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, generator.ErrRecordNotFound
		}
		return nil, generator.WrapAsInternal(err, fmt.Sprintf("getting Client record with id %s from db", id))
	}
	return &m, nil
}

func (r *queryResolver) Clients(ctx context.Context, limit *int, offset *int, filter *ClientFilterInput, orderBy *string, order ClientFieldName, orderDirection OrderDirection) ([]*models.Client, error) {
	var m []*models.Client
	db, err := r.readFilterClient(ctx, filter, nil)
	if err != nil {
		return nil, err
	}

	orderString, err := generator.ParseOrderBy(ctx, orderBy, &order, &orderDirection)
	if err != nil {
		return nil, generator.WrapAsInternal(err, "parsing order by")
	}

	err = db.Limit(*limit).Offset(*offset).Order(orderString).Find(&m).Error
	if err != nil {
		return nil, generator.WrapAsInternal(err, "getting Clients from db")
	}
	return m, nil
}

func (r *clientResolver) Consents(ctx context.Context, obj *models.Client, limit *int, offset *int, filter *ConsentFilterInput, orderBy *string, order ConsentFieldName, orderDirection OrderDirection) ([]*models.Consent, error) {
	if filter == nil {
		filter = &ConsentFilterInput{}
	}
	if filter.And == nil {
		filter.And = []*ConsentFilterInput{}
	}
	filter.And = append(filter.And, &ConsentFilterInput{
		ClientID: &UUIDFilterInput{Eq: &obj.ID},
	})
	return r.Query().Consents(ctx, limit, offset, filter, orderBy, order, orderDirection)
}

func (r *clientResolver) ID(ctx context.Context, obj *models.Client) (uuid.UUID, error) {
	return obj.ID, nil
}

var _ Directive = &clientResolver{nil, nil}

func (r *clientResolver) HasFieldAccess(ctx context.Context, obj interface{}, next graphql.Resolver) (interface{}, error) {
	model := castClient(obj)
	if model == nil {
		return nil, generator.ErrAccessDenied
	}
	fieldName := graphql.GetFieldContext(ctx).Field.Name

	// Scope: 'api:access', Relation: None
	if auth.UserHasScope(ctx, "api:access") {
		if map[string]bool{
			"color":       true,
			"consents":    true,
			"createdAt":   true,
			"extClientId": true,
			"icon":        true,
			"id":          true,
			"logo":        true,
			"name":        true,
			"updatedAt":   true,
		}[fieldName] {
			return next(ctx)
		}
	}

	return nil, generator.ErrAccessDenied
}

func castClient(obj interface{}) *models.Client {
	switch res := obj.(type) {
	case **models.Client:
		return *res
	case *models.Client:
		return res
	case models.Client:
		return &res
	default:
		return nil
	}
}
