// Package graphql implements GQL handler for wallet-gql
package graphql

import (
	"context"
	"encoding/base64"
	"strings"

	"gorm.io/datatypes"
	"gorm.io/gorm"

	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/svc/wallet-gql/models"
)

// AfterReadSetToken after read create the token field
func (cwh *CustomConsentHooks) AfterReadSetToken(_ context.Context, _ *gorm.DB, model *models.Consent) error {
	jwtPayloadEncoded := strings.Split(model.AccessToken, ".")[1]

	var jwtPayloadBytes []byte
	_, err := base64.URLEncoding.Decode(jwtPayloadBytes, []byte(jwtPayloadEncoded))
	if err != nil {
		return errors.Wrap(err, "unable to decode jwt payload")
	}
	model.Token = datatypes.JSON(jwtPayloadBytes)
	return nil
}
