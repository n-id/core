package graphql

import (
	"context"
	"fmt"
	"strings"

	"github.com/99designs/gqlgen/graphql"
	"github.com/gofrs/uuid"
	"github.com/google/go-cmp/cmp"
	"github.com/pkg/errors"
	"gitlab.com/n-id/core/pkg/generator"
	"gitlab.com/n-id/core/svc/wallet-gql/auth"
	"gitlab.com/n-id/core/svc/wallet-gql/models"
	"gorm.io/gorm"
)

type phoneNumberResolver struct {
	*Resolver
	Hooks *CustomPhoneNumberHooks
}

var _ PhoneNumberResolver = &phoneNumberResolver{nil, nil}

type customPhoneNumberHooks interface {
	BeforeCreateHook(ctx context.Context, tx *gorm.DB, input *CreatePhoneNumber) error
	AfterCreateHook(ctx context.Context, tx *gorm.DB, model *models.PhoneNumber) error
}

type CustomPhoneNumberHooks struct{ *Resolver }

var _ customPhoneNumberHooks = &CustomPhoneNumberHooks{nil}

func (r *mutationResolver) CreatePhoneNumber(ctx context.Context, input CreatePhoneNumber) (*models.PhoneNumber, error) {
	var err error
	var m models.PhoneNumber
	user := auth.GetUser(ctx)
	if input.UserID == nil {
		if user != nil {
			input.UserID = &user.ID
		} else {
			return nil, fmt.Errorf("%w: userId", generator.ErrFieldNotProvided)
		}
	}

	hasCreateAccess := false

	// Scope: 'api:access', Relation: HasMyUserID, UserIDField: UserID
	if auth.UserHasScope(ctx, "api:access") && cmp.Equal(input.UserID, &user.ID) {
		if !input.containsField("DeletedAt") {
			hasCreateAccess = true
		}
	}

	if !hasCreateAccess {
		return nil, generator.ErrAccessDenied
	}

	err = r.DB.Transaction(func(tx *gorm.DB) error {
		if err := r.Resolver.PhoneNumber().(*phoneNumberResolver).Hooks.BeforeCreateHook(ctx, tx, &input); err != nil {
			return errors.Wrap(err, "error in BeforeCreateHook")
		}
		if m, err = input.ToModel(ctx, r.Resolver); err != nil {
			return errors.Wrap(err, "converting input CreatePhoneNumber to model")
		}
		create := tx.Create(&m)
		if err := create.Error; err != nil {
			return generator.WrapAsInternal(err, "creating PhoneNumber")
		}
		if err := create.First(&m).Error; err != nil {
			return generator.WrapAsInternal(err, "getting result from create of PhoneNumber")
		}
		if err := r.Resolver.PhoneNumber().(*phoneNumberResolver).Hooks.AfterCreateHook(ctx, tx, &m); err != nil {
			return errors.Wrap(err, "error in AfterCreateHook")
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return &m, nil
}

func (r *mutationResolver) UpdatePhoneNumber(ctx context.Context, id uuid.UUID, input UpdatePhoneNumber) (*models.PhoneNumber, error) {
	var err error
	var m models.PhoneNumber
	user := auth.GetUser(ctx)
	if input.UserID == nil {
		if user != nil {
			input.UserID = &user.ID
		} else {
			return nil, fmt.Errorf("%w: userId", generator.ErrFieldNotProvided)
		}
	}

	fieldCtx := graphql.GetFieldContext(ctx)

	filters := []string{fmt.Sprintf("/*path: %v*/ /*fallback*/false", fieldCtx.Path())}
	var values []interface{}
	var restrictedFields []string

	// Scope: 'api:access', Relation: HasMyUserID, UserIDField: UserID
	if auth.UserHasScope(ctx, "api:access") {
		filters = append(filters, "/*api:access HasMyUserID*/ \"phone_numbers\".\"user_id\" = ?")
		values = append(values, user.ID)
		restrictedFields = append(restrictedFields, "DeletedAt", "Unscoped")
	}

	for _, f := range restrictedFields {
		if input.containsField(f) {
			return nil, fmt.Errorf("%w: %s", generator.ErrFieldAccessDenied, generator.GraphFieldName(f))
		}
	}

	err = r.DB.Transaction(func(tx *gorm.DB) error {
		m, err = input.ToModel(ctx, r.Resolver)
		if err != nil {
			return errors.Wrap(err, "converting input UpdatePhoneNumber to model")
		}
		m.ID = id
		changes := generator.ExtractChanges(ctx, m)
		update := tx.Model(&m).Where(strings.Join(filters, " OR "), values...).Updates(changes)
		if err := update.Error; err != nil {
			return generator.WrapAsInternal(err, fmt.Sprintf("updating PhoneNumber %v", m.ID))
		}
		if update.RowsAffected == 0 {
			return fmt.Errorf("%w (type=PhoneNumber,id=%v)", generator.ErrRecordNotFound, m.ID)
		}
		if err := update.First(&m).Error; err != nil {
			return generator.WrapAsInternal(err, "getting update result")
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return &m, nil
}

func (r *queryResolver) constructFilterPhoneNumber(ctx context.Context, joins map[string]string) (filter string, values []interface{}, restrictedFields []string) {
	fieldCtx := graphql.GetFieldContext(ctx)
	filters := []string{fmt.Sprintf("/*path: %v*/ /*fallback*/false", fieldCtx.Path())}
	user := auth.GetUser(ctx)

	// Scope: 'api:access', Relation: HasMyUserID, UserIDField: UserID
	if auth.UserHasScope(ctx, "api:access") {
		filters = append(filters, "/*api:access HasMyUserID*/ \"phone_numbers\".\"user_id\" = ?")
		values = append(values, user.ID)
		restrictedFields = append(restrictedFields, "DeletedAt", "Unscoped")
	}

	return strings.Join(filters, " OR "), values, restrictedFields
}

func (r *queryResolver) readFilterPhoneNumber(ctx context.Context, filter *PhoneNumberFilterInput, joins map[string]string) (*gorm.DB, error) {
	db := r.Resolver.DB.Model(&models.PhoneNumber{})

	for _, v := range joins {
		db = db.Joins(v)
	}

	filters, values, restrictedFields := r.constructFilterPhoneNumber(ctx, joins)
	db = db.Where(filters, values...)

	if filter != nil {
		for _, f := range restrictedFields {
			if filter.containsField(f) {
				return nil, fmt.Errorf("%w: %s", generator.ErrFieldAccessDenied, generator.GraphFieldName(f))
			}
		}
		expr, args := filter.parse()
		db = db.Where(expr, args...)
		if filter.Unscoped != nil && *filter.Unscoped {
			db = db.Unscoped()
		}
	}

	return db, nil
}

func (r *queryResolver) PhoneNumber(ctx context.Context, id uuid.UUID) (*models.PhoneNumber, error) {
	m := models.PhoneNumber{}
	db, err := r.readFilterPhoneNumber(ctx, nil, nil)
	if err != nil {
		return nil, err
	}
	err = db.Where(`"phone_numbers"."id" = ?`, id).First(&m).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, generator.ErrRecordNotFound
		}
		return nil, generator.WrapAsInternal(err, fmt.Sprintf("getting PhoneNumber record with id %s from db", id))
	}
	return &m, nil
}

func (r *queryResolver) PhoneNumbers(ctx context.Context, limit *int, offset *int, filter *PhoneNumberFilterInput, orderBy *string, order PhoneNumberFieldName, orderDirection OrderDirection) ([]*models.PhoneNumber, error) {
	var m []*models.PhoneNumber
	db, err := r.readFilterPhoneNumber(ctx, filter, nil)
	if err != nil {
		return nil, err
	}

	orderString, err := generator.ParseOrderBy(ctx, orderBy, &order, &orderDirection)
	if err != nil {
		return nil, generator.WrapAsInternal(err, "parsing order by")
	}

	err = db.Limit(*limit).Offset(*offset).Order(orderString).Find(&m).Error
	if err != nil {
		return nil, generator.WrapAsInternal(err, "getting PhoneNumbers from db")
	}
	return m, nil
}

func (r *phoneNumberResolver) ID(ctx context.Context, obj *models.PhoneNumber) (uuid.UUID, error) {
	return obj.ID, nil
}

func (r *phoneNumberResolver) User(ctx context.Context, obj *models.PhoneNumber) (*models.User, error) {
	return r.Query().User(ctx, obj.UserID)
}

var _ Directive = &phoneNumberResolver{nil, nil}

func (r *phoneNumberResolver) HasFieldAccess(ctx context.Context, obj interface{}, next graphql.Resolver) (interface{}, error) {
	model := castPhoneNumber(obj)
	if model == nil {
		return nil, generator.ErrAccessDenied
	}
	fieldName := graphql.GetFieldContext(ctx).Field.Name
	user := auth.GetUser(ctx)

	// Scope: 'api:access', Relation: HasMyUserID, UserIDField: UserID
	if auth.UserHasScope(ctx, "api:access") && cmp.Equal(&model.UserID, &user.ID) {
		if map[string]bool{
			"createdAt":         true,
			"id":                true,
			"phoneNumber":       true,
			"updatedAt":         true,
			"user":              true,
			"userId":            true,
			"verificationToken": true,
			"verificationType":  true,
			"verified":          true,
		}[fieldName] {
			return next(ctx)
		}
	}

	return nil, generator.ErrAccessDenied
}

func castPhoneNumber(obj interface{}) *models.PhoneNumber {
	switch res := obj.(type) {
	case **models.PhoneNumber:
		return *res
	case *models.PhoneNumber:
		return res
	case models.PhoneNumber:
		return &res
	default:
		return nil
	}
}
