package graphql

import (
	"context"

	"gitlab.com/n-id/core/pkg/password"
)

// PasswordManager is the password manager responsible for hashing
// nolint: gochecknoglobals
var PasswordManager password.IManager

// MutateAppPassword hashes the password before it is put in the database
func (cwh *CustomUserHooks) MutateAppPassword(_ context.Context, password string) (string, error) {
	return PasswordManager.GenerateHash(password)
}
