package graphql

import (
	"context"
	"fmt"
	"strings"

	"github.com/99designs/gqlgen/graphql"
	"github.com/gofrs/uuid"
	"github.com/google/go-cmp/cmp"
	"github.com/pkg/errors"
	"gitlab.com/n-id/core/pkg/generator"
	"gitlab.com/n-id/core/svc/wallet-gql/auth"
	"gitlab.com/n-id/core/svc/wallet-gql/models"
	"gorm.io/gorm"
)

type emailAddressResolver struct {
	*Resolver
	Hooks *CustomEmailAddressHooks
}

var _ EmailAddressResolver = &emailAddressResolver{nil, nil}

type customEmailAddressHooks interface {
	BeforeCreateHook(ctx context.Context, tx *gorm.DB, input *CreateEmailAddress) error
	AfterCreateHook(ctx context.Context, tx *gorm.DB, model *models.EmailAddress) error
}

// CustomEmailAddressHooks resolver for customemailaddresshooks
type CustomEmailAddressHooks struct{ *Resolver }

var _ customEmailAddressHooks = &CustomEmailAddressHooks{nil}

// CreateEmailAddress creates an email address
//
//nolint:dupl
func (r *mutationResolver) CreateEmailAddress(ctx context.Context, input CreateEmailAddress) (*models.EmailAddress, error) {
	var err error
	var m models.EmailAddress
	user := auth.GetUser(ctx)
	if input.UserID == nil {
		if user != nil {
			input.UserID = &user.ID
		} else {
			return nil, fmt.Errorf("%w: userId", generator.ErrFieldNotProvided)
		}
	}

	hasCreateAccess := false

	// Scope: 'api:access', Relation: HasMyUserID, UserIDField: UserID
	if auth.UserHasScope(ctx, "api:access") && cmp.Equal(input.UserID, &user.ID) {
		if !input.containsField("DeletedAt") {
			hasCreateAccess = true
		}
	}

	if !hasCreateAccess {
		return nil, generator.ErrAccessDenied
	}

	err = r.DB.Transaction(func(tx *gorm.DB) error {
		if err := r.Resolver.EmailAddress().(*emailAddressResolver).Hooks.BeforeCreateHook(ctx, tx, &input); err != nil {
			return errors.Wrap(err, "error in BeforeCreateHook")
		}
		if m, err = input.ToModel(ctx, r.Resolver); err != nil {
			return errors.Wrap(err, "converting input CreateEmailAddress to model")
		}
		create := tx.Create(&m)
		if err := create.Error; err != nil {
			return generator.WrapAsInternal(err, "creating EmailAddress")
		}
		if err := create.First(&m).Error; err != nil {
			return generator.WrapAsInternal(err, "getting result from create of EmailAddress")
		}
		if err := r.Resolver.EmailAddress().(*emailAddressResolver).Hooks.AfterCreateHook(ctx, tx, &m); err != nil {
			return errors.Wrap(err, "error in AfterCreateHook")
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return &m, nil
}

func (r *mutationResolver) UpdateEmailAddress(ctx context.Context, id uuid.UUID, input UpdateEmailAddress) (*models.EmailAddress, error) {
	var err error
	var m models.EmailAddress
	user := auth.GetUser(ctx)
	if input.UserID == nil {
		if user != nil {
			input.UserID = &user.ID
		} else {
			return nil, fmt.Errorf("%w: userId", generator.ErrFieldNotProvided)
		}
	}

	fieldCtx := graphql.GetFieldContext(ctx)

	filters := []string{fmt.Sprintf("/*path: %v*/ /*fallback*/false", fieldCtx.Path())}
	var values []interface{}
	var restrictedFields []string

	// Scope: 'api:access', Relation: HasMyUserID, UserIDField: UserID
	if auth.UserHasScope(ctx, "api:access") {
		filters = append(filters, "/*api:access HasMyUserID*/ \"email_addresses\".\"user_id\" = ?")
		values = append(values, user.ID)
		restrictedFields = append(restrictedFields, "DeletedAt", "Unscoped")
	}

	for _, f := range restrictedFields {
		if input.containsField(f) {
			return nil, fmt.Errorf("%w: %s", generator.ErrFieldAccessDenied, generator.GraphFieldName(f))
		}
	}

	err = r.DB.Transaction(func(tx *gorm.DB) error {
		m, err = input.ToModel(ctx, r.Resolver)
		if err != nil {
			return errors.Wrap(err, "converting input UpdateEmailAddress to model")
		}
		m.ID = id
		changes := generator.ExtractChanges(ctx, m)
		update := tx.Model(&m).Where(strings.Join(filters, " OR "), values...).Updates(changes)
		if err := update.Error; err != nil {
			return generator.WrapAsInternal(err, fmt.Sprintf("updating EmailAddress %v", m.ID))
		}
		if update.RowsAffected == 0 {
			return fmt.Errorf("%w (type=EmailAddress,id=%v)", generator.ErrRecordNotFound, m.ID)
		}
		if err := update.First(&m).Error; err != nil {
			return generator.WrapAsInternal(err, "getting update result")
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return &m, nil
}

func (r *queryResolver) constructFilterEmailAddress(ctx context.Context, _ map[string]string) (filter string, values []interface{}, restrictedFields []string) {
	fieldCtx := graphql.GetFieldContext(ctx)
	filters := []string{fmt.Sprintf("/*path: %v*/ /*fallback*/false", fieldCtx.Path())}
	user := auth.GetUser(ctx)

	// Scope: 'api:access', Relation: HasMyUserID, UserIDField: UserID
	if auth.UserHasScope(ctx, "api:access") {
		filters = append(filters, "/*api:access HasMyUserID*/ \"email_addresses\".\"user_id\" = ?")
		values = append(values, user.ID)
		restrictedFields = append(restrictedFields, "DeletedAt", "Unscoped")
	}

	return strings.Join(filters, " OR "), values, restrictedFields
}

func (r *queryResolver) readFilterEmailAddress(ctx context.Context, filter *EmailAddressFilterInput, joins map[string]string) (*gorm.DB, error) {
	db := r.Resolver.DB.Model(&models.EmailAddress{})

	for _, v := range joins {
		db = db.Joins(v)
	}

	filters, values, restrictedFields := r.constructFilterEmailAddress(ctx, joins)
	db = db.Where(filters, values...)

	if filter != nil {
		for _, f := range restrictedFields {
			if filter.containsField(f) {
				return nil, fmt.Errorf("%w: %s", generator.ErrFieldAccessDenied, generator.GraphFieldName(f))
			}
		}
		expr, args := filter.parse()
		db = db.Where(expr, args...)
		if filter.Unscoped != nil && *filter.Unscoped {
			db = db.Unscoped()
		}
	}

	return db, nil
}

func (r *queryResolver) EmailAddress(ctx context.Context, id uuid.UUID) (*models.EmailAddress, error) {
	m := models.EmailAddress{}
	db, err := r.readFilterEmailAddress(ctx, nil, nil)
	if err != nil {
		return nil, err
	}
	err = db.Where(`"email_addresses"."id" = ?`, id).First(&m).Error
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, generator.ErrRecordNotFound
		}
		return nil, generator.WrapAsInternal(err, fmt.Sprintf("getting EmailAddress record with id %s from db", id))
	}
	return &m, nil
}

func (r *queryResolver) EmailAddresses(ctx context.Context, limit *int, offset *int, filter *EmailAddressFilterInput, orderBy *string, order EmailAddressFieldName, orderDirection OrderDirection) ([]*models.EmailAddress, error) {
	var m []*models.EmailAddress
	db, err := r.readFilterEmailAddress(ctx, filter, nil)
	if err != nil {
		return nil, err
	}

	orderString, err := generator.ParseOrderBy(ctx, orderBy, &order, &orderDirection)
	if err != nil {
		return nil, generator.WrapAsInternal(err, "parsing order by")
	}

	err = db.Limit(*limit).Offset(*offset).Order(orderString).Find(&m).Error
	if err != nil {
		return nil, generator.WrapAsInternal(err, "getting EmailAddresses from db")
	}
	return m, nil
}

func (r *emailAddressResolver) ID(_ context.Context, obj *models.EmailAddress) (uuid.UUID, error) {
	return obj.ID, nil
}

func (r *emailAddressResolver) User(ctx context.Context, obj *models.EmailAddress) (*models.User, error) {
	return r.Query().User(ctx, obj.UserID)
}

var _ Directive = &emailAddressResolver{nil, nil}

func (r *emailAddressResolver) HasFieldAccess(ctx context.Context, obj interface{}, next graphql.Resolver) (interface{}, error) {
	model := castEmailAddress(obj)
	if model == nil {
		return nil, generator.ErrAccessDenied
	}
	fieldName := graphql.GetFieldContext(ctx).Field.Name
	user := auth.GetUser(ctx)

	// Scope: 'api:access', Relation: HasMyUserID, UserIDField: UserID
	if auth.UserHasScope(ctx, "api:access") && cmp.Equal(&model.UserID, &user.ID) {
		if map[string]bool{
			"createdAt":         true,
			"emailAddress":      true,
			"id":                true,
			"updatedAt":         true,
			"user":              true,
			"userId":            true,
			"verificationToken": true,
			"verified":          true,
		}[fieldName] {
			return next(ctx)
		}
	}

	return nil, generator.ErrAccessDenied
}

func castEmailAddress(obj interface{}) *models.EmailAddress {
	switch res := obj.(type) {
	case **models.EmailAddress:
		return *res
	case *models.EmailAddress:
		return res
	case models.EmailAddress:
		return &res
	default:
		return nil
	}
}
