# Auth Service Chart

> [!IMPORTANT]  
> This chart uses the `opa` library chart, to configure OPA please refer to the [OPA Helm chart Library](../libs/opa/README.md).

## Deploy without database

You can deploy the auth service without a database by setting leaving all PG environment variables empty,
and setting the `PG_RETRY_ON_FAILURE` environment variable to `false`. To ensure Token endpoints are available, you'll need to set `SCOPE_VALIDATOR` to `opa` or `none`.

## Configuration

### Environment Variables

The following table lists of environment variables for the Auth Service and their default values.

| Parameter                   | Default                 | Description                                                                          |
| --------------------------- | ----------------------- | ------------------------------------------------------------------------------------ |
| `SERVER_HOST`               | `.Values.hosts[0]`      | Declares the value which the well-known service returns for the openID configuration |
| `SCOPE_VALIDATOR`           | opa                     | Type of scope validation: `none`, `opa`, or `database`                               |
| `OPA_SCOPE_VALIDATOR_URL`   | <http://localhost:8181> | When OPA scope validation is enabled, URL of the OPA server.                         |
| `OPA_SCOPE_VALIDATOR_PATH`  | scope/validation/result | When OPA scope validation is enabled, path of the policy on the OPA server.          |
| `CLAIMS_VALIDATION_ENABLED` | false                   | Enable claims validation.                                                            |
| `CLAIMS_VALIDATION_PATH`    |                         | Path to the claims validation policy.                                                |
| `CLAIMS_VALIDATION_URL`     |                         | URL to the OPA server containing the policy.                                         |
