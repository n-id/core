{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "service.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "service.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{- define "service.namespaceFullname" -}}
{{- coalesce .Values.namespaceOverride .Release.Namespace}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "service.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}


{{- define "readinessAnnotations" -}}
    {{ $newArray := list }}
    {{- if gt (len .Values.protocol.grpc) 0}}
    {{- range $grpc := .Values.protocol.grpc }}
        {{- $newArray = append $newArray $grpc.servicePort }}
    {{- end }}
    {{- end}}
    {{- if gt (len .Values.protocol.http) 0}}
    {{- range $http := .Values.protocol.http }}
        {{- $newArray = append $newArray $http.servicePort }}
    {{- end}}
    {{- end }}
    {{- join "," $newArray }}
{{- end -}}

{{- define "userVolumes" }}
{{- range $grpc := .Values.protocol.grpc -}}
{{- range $k, $v := $.Values.userSidecarVolumeMounts -}}{"name":"{{ $k }}","configMap":{"name":"{{ $v.configMap }}"}},{{- end -}}
{{- end -}}
{{ end -}}


{{- define "userVolumeMounts" }}
{{- range $grpc := .Values.protocol.grpc }}
{{- range $k, $v := $.Values.userSidecarVolumeMounts -}}{"name":"{{ $k }}", "mountPath":"{{ $v.mountPath }}"},{{- end -}}{{- end -}}
{{end -}}


{{- define "imageRepository" }}
{{- if regexMatch "(:latest)" .Values.image }}
{{fail "You can't use the latest tag for your image"}}
{{- else}}
          image: {{required "image parameter is required, this should be a path to a docker image from a registry" .Values.image }}
{{- end}}
{{- end}}

{{/* This template will help setting the http rules in the virtual service */}}
{{- define "httpRules" }}
  - name: {{ template "service.fullname" .root }}-{{ .protocol.name }}
    route:
    - destination:
        host: {{ template "service.fullname" .root }}.{{ .root.Release.Namespace }}.svc.cluster.local
        port:
          number: {{ .protocol.servicePort }}
  {{- if or .protocol.routes .protocol.sourcePorts }}
    match:
    {{- $routesObjects := list }}
    {{- range .protocol.routes }}
    {{- if kindIs "string" . }}
    - uri:
        prefix: {{ . }}
    {{- else }}
    {{- $routesObjects = append $routesObjects . }}
    {{- end}}
    {{- end }}
    {{- if gt (len $routesObjects) 0 }}
    {{- $routesObjects | toYaml | nindent 4}}
    {{- end }}
    {{- if .protocol.sourcePorts }}
    {{- range .protocol.sourcePorts}}
    - port: {{ . }}
    {{- end}}
    {{- end}}
    {{- if .protocol.rewrite }}
    rewrite:
{{ .protocol.rewrite | toYaml | indent 6 }}
    {{- end}}
  {{- end}}
    {{- if .protocol.timeout }}
    timeout: {{ .protocol.timeout }}
    {{- end}}
    {{- if .protocol.headers.enabled }}
    headers:
      {{- if or .protocol.headers.security.enabled .protocol.headers.response }}
      response:
        {{- if or .protocol.headers.security.enabled .protocol.headers.response.add}}
        add:
          {{- if .protocol.headers.security.enabled }}
          {{- if .protocol.headers.security.csp.enabled }}
          Content-Security-Policy: {{ .protocol.headers.security.csp.value }}
          {{- end }}
          {{- if .protocol.headers.security.stricttransportsecurity.enabled }}
          Strict-Transport-Security: max-age={{.protocol.headers.security.stricttransportsecurity.remember}}; {{- if .protocol.headers.security.stricttransportsecurity.subdomains}} includeSubDomains {{- end}}
          {{- end }}
          {{- if .protocol.headers.security.xcontenttype.enabled}}
          X-Content-Type-Options: {{ .protocol.headers.security.xcontenttype.value }}
          {{- end }}
          {{- if .protocol.headers.security.xframeoptions.enabled}}
          X-Frame-Options: {{ .protocol.headers.security.xframeoptions.allowed}}
          {{- end }}
          {{- if .protocol.headers.security.xssprotection.enabled}}
          X-XSS-Protection: {{ .protocol.headers.security.xssprotection.value}}; {{- if .protocol.headers.security.xssprotection.mode }} mode={{ .protocol.headers.security.xssprotection.mode }} {{- end}}
          {{- end }}
          {{- if .protocol.headers.security.referrerpolicy.enabled}}
          Referrer-Policy: {{ .protocol.headers.security.referrerpolicy.value}}
          {{- end }}
          {{- end }}
          {{- if and .protocol.headers.response .protocol.headers.response.add }}
{{ .protocol.headers.response.add | toYaml | indent 10}}
          {{- end }}
        {{- end}}
        {{- if and .protocol.headers.response .protocol.headers.response.set }}
        set:
{{ .protocol.headers.response.set | toYaml | indent 10}}
        {{- end }}
        {{- if and .protocol.headers.response .protocol.headers.response.remove }}
        remove:
{{ .protocol.headers.response.remove | toYaml | indent 10}}
        {{- end }}
      {{- end}}
      {{- if .protocol.headers.request }}
      request:
{{ .protocol.headers.request | toYaml | indent 8}}
      {{- end }}
    {{- end}}
    {{- if .protocol.corsPolicy }}
    corsPolicy:
{{ .protocol.corsPolicy | toYaml | indent 6}}
    {{- else if .root.Values.corsPolicy}}
    corsPolicy:
{{ .root.Values.corsPolicy | toYaml | indent 6}}
    {{- end}}
{{- end}}
