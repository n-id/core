# service

Service chart that can be used for http or grpc based microservices.

![AppVersion: 0.1](https://img.shields.io/badge/AppVersion-0.1-informational?style=flat-square)

## Additional Information

## Health checking
This pod adds service health checking. For http you need to expose an endpoint at `/v1/health`, that returns `200 OK` when the service is healthy.
For grpc services the `grpc_health_probe` is used. You can add this to your docker container using:

```
RUN GRPC_HEALTH_PROBE_VERSION=v0.2.0 && \
    wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64 && \
    chmod +x /bin/grpc_health_probe

```
 
## Autoscaler
This helm chart allows you to specify and apply autoscaling.
Currently, with this helm chart you can apply two different ways of autoscaling, namely horizontal pod autoscaling (hpa) and multi dimensional pod autoscaling (mpa).

For both the hpa and mpa you'll need to configure `resources` in your deployment. Because it'll need that information to create metrics on so it can measure if it'll need to scale.

**Note**
The mpa can only be used with gcloud and you must also have ```vertical-pod autoscaling``` enabled on your cluster, check
[terraform-gke-vpa](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/container_cluster#vertical_pod_autoscaling).
 
### Horizontal Pod Autoscaler (HPA)   
To set up an hpa you need to provide metrics.
The metrics that have already been tested are:

```yaml
metrics:
    cpu:
        target:
            type: Utilization
            averageUtilization: 50
    memory:
        target:
            type: Utilization
            averageUtilization: 50
```

By default, the hpa is off. You can easily activate it for a service by setting `enabled: true`.
Also the default configuration of the minimum replicas is 2 replicas and the max replicas is 4.
You can specify it yourself.
   
```yaml
autoscaler:
    enabled: false
    type: hpa
    minReplicas: 3 # set the min replicas
    maxReplicas: 20 # set the max replicas
```
You have to take into account the space you have on your nodes.

### Multidimensional Pod Autoscaler (MPA)
**Prerequisites**
- GKE cluster version 1.19.4-gke.1700 or later.
- Enable vertical Pod autoscaling in your cluster. (can be done in terraform)

The mpa is suitable for situations where your resources fluctuate over time. It is important to monitor carefully what the lower limits of your resources are.

To be able to use the mpa, you need to activate the autoscaler and set the type to mpa.

It is recommended to scale horizontally on CPU, for example, and vertically on memory. This has to do with race conditions if both conditions become active they can interact with each other.

If you configure a mpa with vertical scaling on memory and horizontal on the cpu you'll get something like this.

```yaml
autoscaler:
    enabled: true
    type: mpa
    minReplicas: 2
    maxReplicas: 4
    metrics:
        cpu:
            target:
                type: Utilization
    averageUtilization: 50

    containerControlledResources: ["memory"]
    constraints:
        minAllowed:
            memory: "16Mi"
        maxAllowed:
            memory: "128Mi"
```

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| -httpHealthPath | string | `nil` | Deprecated. Use healthProbes.readiness.path and healthProbes.liveness.path instead |
| -httpHealthPort | int | `nil` | Deprecated. Use healthProbes.readiness.port and healthProbes.liveness.port instead |
| autoscaler | object | `{"behavior":{},"constraints":{},"containerControlledResources":[],"enabled":false,"maxReplicas":4,"metrics":{},"minReplicas":2,"type":"hpa"}` | Autoscaling pods with Horizontal Pod Scaling or with Multidimensional Pod Autoscaler |
| autoscaler.behavior | object | `{}` | Behaviour defines how you want to scale down and scale up. It's not required to define these. |
| autoscaler.constraints | object | `{}` | Constraints for vertical scale a pod. Note: constraints override goals. |
| autoscaler.containerControlledResources | list | `[]` | Specify a list of controlled container resources to scale vertically.  Container Resources that should be controlled by the autoscaler. memory is the only supported value. (At this moment of writing). https://cloud.google.com/kubernetes-engine/docs/how-to/multidimensional-pod-autoscaling |
| autoscaler.enabled | bool | `false` | Set to true to enable the scaling |
| autoscaler.maxReplicas | int | `4` | MaxReplicas are the maximum of pods which can be created by the autoscaler according to the defined resources, it's part of the goals of the autoscaler |
| autoscaler.metrics | object | `{}` | Define the metrics for cpu or memory, based on these metrics a pod will scale horizontally |
| autoscaler.minReplicas | int | `2` | MinReplicas are the minimum of pods which will be spawned it's part of the goals of the autoscaler |
| autoscaler.type | string | `"hpa"` | Type of the autoscaler, it can be hpa or mpa. By default, we use the hpa, because the hpa needs some configuration first. |
| containerSecurityContext | object | `{"allowPrivilegeEscalation":false,"capabilities":{"drop":["ALL"]},"enabled":false,"runAsNonRoot":true,"seccompProfile":{"type":"RuntimeDefault"}}` | SecurityContext according to the k8s [securitycontext spec](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.26/#securitycontext-v1-core) |
| deploymentAnnotations | object | `{}` | An array of annotations that set on the deployment |
| disableHealthProbes | bool | `nil` | Deprecated. Use heathProbes.enabled instead. |
| distinctnodes | bool | `false` |  |
| entrypoint | string | `nil` | the path to the entrypoint script AKA the CMD in the dockerfile. This overwrites CMD in Dockerfile. |
| fullnameOverride | string | `nil` | If specified will completely replace generated release name. Example: fullnameOverride: override, then Deployment will be named override |
| gateways | string | `nil` | Gateways according to the istio [VirtualService.Gateway spec](https://istio.io/docs/reference/config/networking/virtual-service/#VirtualService) |
| grpcHealthCheckPort | int | `nil` | Deprecated. Use healthProbes.readiness.port and healthProbes.liveness.port instead |
| healthProbes | object | `{"enabled":true,"liveness":{"failureThreshold":3,"initialDelaySeconds":10,"path":"/v1/live","periodSeconds":3,"port":1161,"timeoutSeconds":1},"readiness":{"failureThreshold":10,"initialDelaySeconds":5,"path":"/v1/ready","periodSeconds":2,"port":1161,"successThreshold":1,"timeoutSeconds":1},"type":""}` | Configuration of the health probes settings |
| healthProbes.enabled | bool | `true` | Set to false to disable the health probes |
| healthProbes.liveness.failureThreshold | int | `3` | Number of liveness probe failures before the container is restarted |
| healthProbes.liveness.initialDelaySeconds | int | `10` | Delay after container start before liveness probe is initiated |
| healthProbes.liveness.path | string | `"/v1/live"` | Endpoint for HTTP liveness probe (ignored for grpc probes) |
| healthProbes.liveness.periodSeconds | int | `3` | Time between consecutive liveness probes |
| healthProbes.liveness.port | int | `1161` | Port for the liveness probe |
| healthProbes.liveness.timeoutSeconds | int | `1` | Time after which the liveness probe times out |
| healthProbes.readiness.failureThreshold | int | `10` | Number of readiness probe failures before the container is marked as unready |
| healthProbes.readiness.initialDelaySeconds | int | `5` | Delay after container start before readiness probe is initiated |
| healthProbes.readiness.path | string | `"/v1/ready"` | Endpoint for the HTTP readiness probe (ignored for grpc probes) |
| healthProbes.readiness.periodSeconds | int | `2` | Time between consecutive readiness probes |
| healthProbes.readiness.port | int | `1161` | Port for the readiness probe |
| healthProbes.readiness.successThreshold | int | `1` | Consecutive successes before a failed readiness probe is considered successful again |
| healthProbes.readiness.timeoutSeconds | int | `1` | Time after which the readiness probe times out |
| healthProbes.type | string | `""` | Type of health probes, either http or grpc. However grpc is deprecated, include a grpc check in your http handler implementation instead |
| hosts | string | `nil` | An array with the hosts for this service (these should also be defined in the gateway referenced under gateway). (Note: `istio` should be true for this to have effect) |
| image | string | `"egistry.gitlab.com/n-id/core/service:useatag"` | The docker image of the service (required) |
| imagePullPolicy | string | `"Always"` | Image Pull Policy for the deployment of the service |
| imagePullSecrets | list | `["registry-secret"]` | You can add multiple secrets for a deployment |
| istio | bool | `true` | Istio support, adds a virtual service and service entry support |
| istioVersion | string | `"0.0.0"` | Lowest possible version by default |
| livenessDelay | int | `nil` | Deprecated. Use healthProbes.liveness.initialDelaySeconds instead |
| logFormat | string | `"json"` | The log format used for logging (view _values.schema.json for options) |
| logLevel | string | `"info"` | The log level to be mounted as environment variable to container (view _values.schema.json for options) |
| minReadySeconds | string | `nil` | Optional field that specifies the minimum number of seconds for which a newly created Pod should be ready without any of its containers crashing, for it to be considered available. This defaults to 0 |
| nameOverride | string | `nil` | If specified will replace name of chart. Example: nameOverride: name-override, then Deployment will be named release-name-name-override |
| namespaceOverride | string | `nil` | If specified it will overwrite the Release.Namespace value |
| prometheus | bool | `true` | Enable prometheus scraping for this service |
| prometheusPort | int | `8080` | The port at which prometheus should scrape this service |
| protocol.grpc | list | `[]` | grpc protocol is an array of grpc protocols. Note: max 1 allowed |
| protocol.http | list | `[]` | http is an array of http protocols |
| readinessDelay | int | `nil` | Deprecated. Use healthProbes.readiness.initialDelaySeconds instead |
| replicas | int | `1` | Amount of replicas for the service |
| resources | string | `nil` | Specify resource limits and requests for the container [spec](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#resource-requests-and-limits-of-pod-and-container) |
| rollingUpdate | bool | `true` | Enable rolling update (boolean) |
| serviceAccountName | string | `nil` | Provides an identity for processes that run in a Pod. If you do not specify a service account, it is automatically assigned the default service account in the same namespace [spec](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/) |
| vault | object | `{"enabled":false}` | Values needed to configure vault agent sidecar injection. ServiceAccountName should be set to the one created for vault. entrypoint must be set if vault enabled. |
| volumeMounts | string | `nil` | List of details of where to mount the specified volumes [spec](https://kubernetes.io/docs/concepts/storage/volumes/) |
| volumes | string | `nil` | List of volumes to provide for the Pod [spec](https://kubernetes.io/docs/concepts/storage/volumes/) |


