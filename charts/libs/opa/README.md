# OPA Helm chart Library

This helm library chart provides a way to install OPA as a sidecar container in a pod an configure them in a simple way.

> [!IMPORTANT]
> If you make use of environment variable substitution as described by opa, make sure you use quotes around the value,
> and make sure no multiline values are used. For example, when using a PEM encoded certificate this will look something like:
>
> ```yaml
> ---
> "-----BEGIN CERTIFICATE-----\nMIIDzTCCArWgAwIBAgIUI7 \n-----END CERTIFICATE-----"
> ```
>
> Encode this whole string, with quotes!

## Usage

To use this library, include the container and volumes in your deployment and include the ConfigMap as a separate resource:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: with-opa
spec:
  containers: { { - include "opa.container" . | nindent 8 } }
  volumes: { { - include "opa.volumes" . | nindent 8 - } }
---
{ { - include "opa.config" . | nindent 2 } }
```

## Configuration

All configuration is nested in the `opa` key.

This library consists out of 3 templates to run opa, which all need to be used to get the full functionality. They cannot be used without each other.

- `opa.container` - This template is used to define the container that will run OPA.
- `opa.config` - A ConfigMap that will be used to configure OPA.
- `opa.volumes` - An empty dir for OPA to save bundles.

This library also contains 3 templates to deploy the OPA playground, which can be used to test policies.
To use the playground image directly, an imagePullSecret for GithubHubs registry (ghcr.io) is required.

- `opa.playground.container` - This template is used to define the container that will run OPA playground.
- `opa.playground.service` - A Service for traffic management to the playground within the cluster.
- `opa.playground.virtual_service` - A VirtualService for traffic outside of the cluster.

### Values OPA

| Key                      | Type                                 | Default                           | Description                                    |
| ------------------------ | ------------------------------------ | --------------------------------- | ---------------------------------------------- |
| name                     | string                               | opa-istio                         | Name of the container.                         |
| image                    | string                               | registry.gitlab.com/n-id/core/opa | Opa image.                                     |
| additionalArgs           | []string                             |                                   | Additional arguments to pass to the OPA Server |
| port                     | int                                  | 8181                              | OPA port to expose.                            |
| diagnosticPort           | int                                  | 8282                              | OPA diagnostic port to expose.                 |
| containerSecurityContext | [SecurityContext](#securitycontext)  |                                   | Security context for the container.            |
| config                   | object                               | decision_logs.console = true      | OPA yaml configuration.                        |
| playground               | [Playground](#values-opa-playground) |                                   | OPA playground configuration.                  |

### Values OPA Playground

| Key              | Type     | Default                     | Description                                          |
| ---------------- | -------- | --------------------------- | ---------------------------------------------------- |
| enabled          | boolean  | false                       | Enable the deployment of the playground specifically |
| port             | int      | 8080                        | Port to expose the playground on.                    |
| image            | string   | ghcr.io/oma-apps/oma:latest | Image to use for the playground.                     |
| env              | object   |                             | Environment variables to set in the playground.      |
| imagePullSecrets | string[] | []                          | Image pull secrets to use for the playground image.  |

### SecurityContext

Default Kubernetes [SecurityContext](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.26/#securitycontext-v1-core) configure to not run as root enabled toggle:

| Key          | Type | Default | Description                                |
| ------------ | ---- | ------- | ------------------------------------------ |
| enabled      | bool | true    | Enable security context for the container. |
| runAsNonRoot | bool | true    | Run as non root user.                      |
| runAsUser    | int  | 10000   | User to run as.                            |
| runAsGroup   | int  | 10000   | Group to run as.                           |
