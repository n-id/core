# Resource Server

This chart is designed to deploy [Open Policy Agent (OPA)](https://www.openpolicyagent.org/) as a sidecar to an
application and configure an Envoy to route all incoming request to the application through OPA first for Authorization.
The chart deploys several Kubernetes resources including a Deployment, a Service, an EnvoyFilter, config maps, and
potentially more depending on the configuration.

The chart is designed to be highly configurable through the use of variables that can be defined in the Helm values
file. By default, the proxy is enabled. With the proxy enabled the chart will deploy an NGINX container and route all
incoming requests (when authorized by the policies) to the audience claim in the JWT.

## Prerequisites

- Istio 1.21+
- Kubernetes 1.27+
- Helm 3.x

## Installing the Chart

```shell
helm repo add nid https://gitlab.com/api/v4/projects/51490161/packages/helm/stable
helm repo update

helm install resource-server nid/resource-server
```

This chart uses Istio `External Authorization` to authorize requests,
the authorization requests are handled by OPA which is deployed as a sidecar container.
To enable `External Authorization` add the following config to your Istio service mesh:

```yaml
# Envoy External Authorization filter that will query OPA.
mesh: |-
  extensionProviders:
  - name: opa-ext-authz-grpc
    envoyExtAuthzGrpc:
      service: {{ .Release.Name }}.{{ .Release.Namespace }}.svc.cluster.local
      port: "9191"
```

## Policies

Policies are a collection of rules that are used to determine whether a request should be allowed or denied. Policies
are written in [Rego](https://www.openpolicyagent.org/docs/latest/policy-language/) By default, policies are pulled from
a repository and verified with a public key. This is also the recommended way of managing and verifying policies.

Policies used in the OPA Envoy get the request as input and should return a boolean value.
If the value is true, the request is allowed; otherwise it is denied.
The default key used for this decision is `allow` but can be configured
using the `envoy_ext_authz_grpc`.`path`.

### Policy Input

Policies get the request as input along with some computed fields. The following is a list of all fields that are
available in the input.

```json
{
  "attributes": {
    "destination": {
      "address": {
        "socketAddress": {
          "address": "1.2.3.4",
          "portValue": 80
        }
      },
      "principal": "spiffe://cluster.local/ns/nid/sa/default"
    },
    "metadataContext": {},
    "request": {
      "http": {
        "headers": {
          "key": "value"
        },
        "host": "example.com",
        "id": "123456789",
        "method": "POST",
        "path": "/users",
        "protocol": "HTTP/1.1",
        "scheme": "http"
      },
      "time": "2023-11-14T13:08:21.836769Z"
    },
    "source": {
      "address": {
        "socketAddress": {
          "address": "1.2.3.4",
          "portValue": 12345
        }
      },
      "principal": "spiffe://cluster.local/ns/istio-system/sa/istio-ingressgateway"
    }
  },
  "parsed_body": {
    "my": "body"
  },
  "parsed_path": ["users"],
  "parsed_query": {
    "query": ["value"]
  },
  "truncated_body": false,
  "version": {
    "encoding": "protojson",
    "ext_authz": "v3"
  }
}
```

### Policy Functions

Along with any functions defined in rego or provided by default OPA, the following functions are available to use in
policies in the OPA Envoy in this project. These functions are custom built for this project and are not available in
OPA by default. The documentation for these functions can be found in
the [OPA service documentation](../../svc/opa/README.md).

## OPA Playground

This chart includes an option to deploy an OPA Playground. The OPA Playground is a web-based tool that allows you to
write, test, and debug OPA policies. The playground is a great tool for developing and testing policies before deploying
them to your OPA instance. The playground is deployed as a separate service and can be accessed through a web browser.

Refer to the [OPA Playground documentation](https://github.com/oma-apps/OMA) for more information on how to use the playground.
And configure the deployment of the playground via the [`opa.playground`](../libs/opa/README.md#values-opa-playground) values.

## Variables

> [!IMPORTANT]  
> This chart uses the `opa` library chart, to configure OPA please refer to
> the [OPA Helm chart Library](../libs/opa/README.md).

The following variables can be configured in the Helm values file.

| Variable     | Type                                    | Description                                                       |
| ------------ | --------------------------------------- | ----------------------------------------------------------------- |
| replicaCount | string                                  | Number of replicas of your application that should be maintained. |
| servicePort  | number                                  | Port of the Service to listens on.                                |
| filters      | [Filters](#filters)                     | Envoy filter configuration.                                       |
| proxy        | [Proxy](#proxy)                         | Proxy configuration.                                              |
| istio        | [Istio](#istio)                         | Docker image that will be used for your application.              |
| container    | [Container](#container)                 | Container to deploy with the OPA sidecar.                         |
| opa          | [OPA](../libs/opa/README.md#values-opa) | OPA specific configuration.                                       |

### Filters

Configuration variables for the included Envoy filters:

- decision-id-to-header: this filter adds the OPA decision id toin the response headers.

| Variable | Type    | Description                |
| -------- | ------- | -------------------------- |
| enabled  | boolean | Enable/Disable all filters |

### Proxy

Configuration variables for the proxy. The proxy is a NGINX configured to route incoming traffic to the uri in the 'aud'
claim of the JWT token.

| Variable         | Type          | Description                                           |
| ---------------- | ------------- | ----------------------------------------------------- |
| enabled          | boolean       | Whether the                                           |
| port             | number        | Port that the proxy listens on.                       |
| image            | string        | Docker image that will be used for the proxy.         |
| imagePullSecrets | string[]      | List of image pull secrets that should be used.       |
| issuer           | string        | The issuer of the JWT token.                          |
| jwksUri          | string        | The URI to the JWKS endpoint.                         |
| mtls             | [mTLS](#mtls) | mTLS specific configuration                           |
| dns              | DNS           | DNS resolver that should be used by your application. |

#### DNS

Proxy settings for routing requests to either local or external services.

| Variable | Type   | Description                             |
| -------- | ------ | --------------------------------------- |
| resolver | string | Resolver to resolve the proxy requests. |

#### mTLS

Configuration for managing mTLS and certificates.

| Variable             | Type                      | Description                                                   |
| -------------------- | ------------------------- | ------------------------------------------------------------- |
| enabled              | boolean                   | Whether the DNS resolver should be enabled.                   |
| certs.key.fromSecret | [fromSecret](#fromsecret) | Key secret to be used by mTLS.                                |
| ~~certs.key.value~~  | ~~string~~                | ~~Key value, to be placed in a secret by the chart.~~         |
| certs.crt.fromSecret | [fromSecret](#fromsecret) | Certificate secret to be used by mTLS.                        |
| ~~certs.crt.value~~  | ~~string~~                | ~~Certificate value, to be placed in a secret by the chart.~~ |

#### FromSecret

Defines a secret and key that should be mounted as a volume in the container.

| Variable | Type   | Description                            |
| -------- | ------ | -------------------------------------- |
| name     | string | Name of the secret.                    |
| key      | string | Key in the secret that should be used. |

### Istio

Configuration variables for Istio. Used to configure the virtual service.

| Variable  | Type   | Description                                                                    |
| --------- | ------ | ------------------------------------------------------------------------------ |
| host      | string | Host to be used in the virtual service, it is used with {release-name}.{host}. |
| namespace | string | Namespace istio gateway is installed in.                                       |
| gateway   | string | Istio gateway that should handle traffic for your application.                 |

### Container

Configuration variables for the container that will be deployed.

| Variable | Type   | Description                                          |
| -------- | ------ | ---------------------------------------------------- |
| port     | number | Port that your application listens on.               |
| image    | string | Docker image that will be used for your application. |
