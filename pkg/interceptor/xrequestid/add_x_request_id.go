// Package xrequestid helps with the propagation of x-request-id
package xrequestid

import (
	"context"

	"gitlab.com/n-id/core/pkg/utilities/log/v2"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

// ContextKey context key name for x-request-id
type ContextKey string

// XRequestID key value for the context of x-request-id
const XRequestID ContextKey = "x-request-id"

// AddXRequestID propagates x-request-id header for the grpc server/client
func AddXRequestID(ctx context.Context, req interface{}, _ *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	md, ok := metadata.FromIncomingContext(ctx)
	header := md[string(XRequestID)]

	if ok && len(header) > 0 {
		ctx = metadata.AppendToOutgoingContext(ctx, string(XRequestID), header[0])
		ctx = context.WithValue(ctx, XRequestID, header[0]) // For clients which are not grpc.
	}

	if len(header) > 1 {
		log.Warn("multiple x-request-id headers found")
	}

	return handler(ctx, req)
}
