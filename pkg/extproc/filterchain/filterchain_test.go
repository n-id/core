package filterchain

import (
	"context"
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/n-id/core/pkg/extproc/filter"
	"gitlab.com/n-id/core/pkg/extproc/filter/mock"
)

type FilterChainTestSuite struct {
	suite.Suite
}

func TestFilterChainTestSuite(t *testing.T) {
	suite.Run(t, &FilterChainTestSuite{})
}

func (s *FilterChainTestSuite) TestBuildFilterChain() {
	mockFilter1 := mock.NewFilter(s.T())
	mockFilter3 := mock.NewFilter(s.T())

	mockFilterInitializer1 := mock.NewInitializer(s.T())
	mockFilterInitializer1.EXPECT().Name().Return("somefilter")
	mockFilterInitializer1.EXPECT().NewFilter().Return(mockFilter1, nil)

	mockFilterInitializer2 := mock.NewInitializer(s.T())
	mockFilterInitializer2.EXPECT().Name().Return("someotherfilter")

	mockFilterInitializer3 := mock.NewInitializer(s.T())
	mockFilterInitializer3.EXPECT().Name().Return("yetanotherfilter")
	mockFilterInitializer3.EXPECT().NewFilter().Return(mockFilter3, nil)

	initializers := []filter.Initializer{
		mockFilterInitializer1,
		mockFilterInitializer2,
		mockFilterInitializer3,
	}

	chain, err := BuildDefaultFilterChain([]string{"somefilter", "yetanotherfilter"}, initializers)

	s.Require().NoError(err)
	s.Require().NotNil(chain)
	s.Require().NotNil(chain.filters)
	s.Require().Len(chain.filters, 2)
	s.Require().Equal(mockFilter1, chain.filters[0])
	s.Require().Equal(mockFilter3, chain.filters[1])
}

func (s *FilterChainTestSuite) TestFilterChainGoesInReverseOnResponse() {
	mockFilter1 := mock.NewFilter(s.T())
	mockFilter2 := mock.NewFilter(s.T())
	mockFilter3 := mock.NewFilter(s.T())

	filters := []filter.Filter{
		mockFilter1,
		mockFilter2,
		mockFilter3,
	}

	ctx := context.TODO()
	mockFilter1.EXPECT().Name().Return("filter1")
	mockFilter1.EXPECT().OnHTTPResponse(ctx, []byte("appeltaart"), map[string]string{}).Return(&filter.ProcessingResponse{
		NewHeaders:        nil,
		NewBody:           []byte("appeltaart is lekker"),
		ImmediateResponse: nil,
	}, nil)
	mockFilter2.EXPECT().Name().Return("filter2")
	mockFilter2.EXPECT().OnHTTPResponse(ctx, []byte("appel"), map[string]string{}).Return(&filter.ProcessingResponse{
		NewHeaders:        nil,
		NewBody:           []byte("appeltaart"),
		ImmediateResponse: nil,
	}, nil)
	mockFilter3.EXPECT().Name().Return("filter3")
	mockFilter3.EXPECT().OnHTTPResponse(ctx, []byte("a"), map[string]string{}).Return(&filter.ProcessingResponse{
		NewHeaders:        nil,
		NewBody:           []byte("appel"),
		ImmediateResponse: nil,
	}, nil)

	chain := &DefaultChain{filters: filters}

	chain.ProcessResponseHeaders(map[string]string{})
	res, err := chain.ProcessResponseBody(ctx, []byte("a"))

	s.Require().NoError(err)
	s.Require().Equal([]byte("appeltaart is lekker"), res.Body)
}

func (s *FilterChainTestSuite) TestFilterChainRequest() {
	mockFilter1 := mock.NewFilter(s.T())
	mockFilter2 := mock.NewFilter(s.T())
	mockFilter3 := mock.NewFilter(s.T())

	filters := []filter.Filter{
		mockFilter1,
		mockFilter2,
		mockFilter3,
	}

	mapSomeValue := map[string]string{"coolheader": "some value"}
	mapSomeOtherValue := map[string]string{"coolheader": "some other value"}

	ctx := context.TODO()
	mockFilter1.EXPECT().Name().Return("filter1")
	mockFilter1.EXPECT().OnHTTPRequest(ctx, []byte("a"), map[string]string{}).Return(&filter.ProcessingResponse{
		NewHeaders:        mapSomeValue,
		NewBody:           []byte("appel"),
		ImmediateResponse: nil,
	}, nil)
	mockFilter2.EXPECT().Name().Return("filter2")
	mockFilter2.EXPECT().OnHTTPRequest(ctx, []byte("appel"), map[string]string{"coolheader": "some value"}).Return(&filter.ProcessingResponse{
		NewHeaders:        mapSomeOtherValue,
		NewBody:           []byte("appeltaart"),
		ImmediateResponse: nil,
	}, nil)
	mockFilter3.EXPECT().Name().Return("filter3")
	mockFilter3.EXPECT().OnHTTPRequest(ctx, []byte("appeltaart"), map[string]string{"coolheader": "some other value"}).Return(&filter.ProcessingResponse{
		NewHeaders:        map[string]string{"coolheader": "some other value"},
		NewBody:           []byte("appeltaart is beter dan chocoladetaart"),
		ImmediateResponse: nil,
	}, nil)

	chain := &DefaultChain{filters: filters}

	res, err := chain.ProcessRequestHeaders(ctx, map[string]string{}, false)
	s.Require().Equal(&ProcessingResponse{}, res)
	s.Require().NoError(err)

	res, err = chain.ProcessRequestBody(ctx, []byte("a"))

	s.Require().NoError(err)
	s.Equal(map[string]string{"coolheader": "some other value"}, res.Headers)
	s.Equal([]byte("appeltaart is beter dan chocoladetaart"), res.Body)
}
