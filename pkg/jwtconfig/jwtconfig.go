// Package jwtconfig provides the jwt config
package jwtconfig

import (
	"crypto/rsa"
	"os"
	"path/filepath"

	"gitlab.com/n-id/core/pkg/utilities/errors"
	"gitlab.com/n-id/core/pkg/utilities/jwt/v2"
)

// JWTKey contains the secret parameters for jwt
type JWTKey struct {
	*rsa.PrivateKey
	ID string
}

// Read reads the jwt secrets from a directory
func Read(path string) (*JWTKey, error) {
	kid, err := os.ReadFile(filepath.Clean(path + "/kid")) //nolint:goconst
	if err != nil {
		return nil, errors.Wrap(err, "unable to read kid from file")
	}
	keyBytes, err := os.ReadFile(filepath.Clean(path + "/jwtkey.pem")) //nolint:goconst
	if err != nil {
		return nil, errors.Wrap(err, "unable to read JWT key from file")
	}

	privKey, err := jwt.ParseKey(keyBytes)
	if err != nil {
		return nil, errors.Wrap(err, "unable to parse JWT key bytes")
	}

	return &JWTKey{
		PrivateKey: privKey,
		ID:         string(kid),
	}, nil
}
