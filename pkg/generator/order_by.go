package generator

import (
	"context"
	"fmt"
	"strings"

	"github.com/99designs/gqlgen/graphql"
)

const (
	orderDirectionAsc  = "ASC"
	orderDirectionDesc = "DESC"
)

type enum interface {
	String() string
	UnmarshalGQL(v interface{}) error
}

func extractOrderDirection(orderBy *string, orderDirection enum, direction string) error {
	if strings.HasSuffix(strings.ToUpper(*orderBy), " "+direction) {
		// If *orderBy ends with " "+direction, strip it and set orderDirection.
		err := orderDirection.UnmarshalGQL(direction)
		if err != nil {
			return err
		}
		*orderBy = (*orderBy)[:len(*orderBy)-len(direction)-1]
	}
	return nil
}

// ParseOrderBy takes all the order parameters from a resolver and parses and sanitizes these to a
// single SQL 'ORDER BY' statement.
func ParseOrderBy(ctx context.Context, orderBy *string, order enum, orderDirection enum) (string, error) {
	fieldCtx := graphql.GetFieldContext(ctx)

	// Only check orderBy if order was not set manually.
	orderIsSet := fieldCtx.Field.Arguments.ForName("order") != nil
	return parseOrderBy(orderIsSet, orderBy, order, orderDirection)
}

func parseOrderBy(orderIsSet bool, orderBy *string, order enum, orderDirection enum) (string, error) {
	if !orderIsSet && orderBy != nil {
		if err := extractOrderDirection(orderBy, orderDirection, orderDirectionDesc); err != nil {
			return "", err
		}
		if err := extractOrderDirection(orderBy, orderDirection, orderDirectionAsc); err != nil {
			return "", err
		}
		if err := order.UnmarshalGQL(GraphEnumValue(*orderBy)); err != nil {
			return "", err
		}
	}

	orderFieldName := strings.ToLower(order.String())
	return fmt.Sprintf("%s %s", orderFieldName, orderDirection), nil
}
