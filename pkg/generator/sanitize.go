package generator

import (
	"bytes"
	"github.com/goadesign/goa/goagen/codegen"
	"github.com/iancoleman/strcase"
	"strings"
	"unicode"
)

func GoFieldName(name string) string {
	return codegen.Goify(name, true)
}

func DBFieldName(name string) string {
	name = GoifyToCamelCase(name)
	name = codegen.SnakeCase(name)
	if strings.HasSuffix(name, "_i_d") {
		name = strings.TrimSuffix(name, "_i_d")
		name = name + "_id"
	}
	if name == "i_d" {
		name = "id"
	}
	return name
}

func GraphFieldName(name string) string {
	name = GoifyToCamelCase(name)
	name = strcase.ToLowerCamel(name)
	return name
}

// GoifyToCamelCase converts the output of Goify to CamelCase, for example: "APIFoo" to "ApiFoo"
func GoifyToCamelCase(val string) string {
	if len(val) == 0 {
		return ""
	}
	var b bytes.Buffer
	var prev rune
	for i, v := range val {
		if unicode.IsUpper(v) && unicode.IsUpper(prev) {
			b.WriteRune(unicode.ToLower(v))
		} else {
			if unicode.IsUpper(prev) {
				if i != 0 {
					b.Truncate(i - 1)
					b.WriteRune(unicode.ToUpper(prev))
				}
			}
			b.WriteRune(v)
		}
		prev = v
	}
	return b.String()
}

// GraphEnumValue converts a string to graphql enum value, screaming snake per graphql convention
func GraphEnumValue(val string) string {
	return strings.ToUpper(DBFieldName(val))
}
