package generator

import (
	"context"

	"github.com/99designs/gqlgen/graphql"
	"github.com/fatih/structs"
)

// ExtractChanges extracts from the context which fields should be updated and the changeset.
func ExtractChanges(ctx context.Context, inputModel interface{}) map[string]interface{} {
	fieldCtx := graphql.GetFieldContext(ctx)
	opCtx := graphql.GetOperationContext(ctx)

	m := structs.Map(inputModel)
	changes := map[string]interface{}{}

	args := fieldCtx.Field.ArgumentMap(opCtx.Variables)["input"].(map[string]interface{})
	for fieldName := range args {
		dbFieldName := DBFieldName(fieldName)
		goFieldName := GoFieldName(dbFieldName)
		changes[dbFieldName] = m[goFieldName]
	}

	return changes
}
