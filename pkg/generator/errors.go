package generator

import (
	"fmt"

	"github.com/pkg/errors"
)

// Error Definitions
var (
	ErrFieldNotProvided             = NewGraphQLError("field not provided", false, "GEN-001")
	ErrAccessDenied                 = NewGraphQLError("access denied", false, "GEN-003")
	ErrNotLoggedIn                  = NewGraphQLError("not logged in", false, "GEN-004")
	ErrEnumsMustBeStrings           = NewGraphQLError("enums must be strings", false, "GEN-005")
	ErrNotAValidEnumValue           = NewGraphQLError("not a valid enum value", false, "GEN-006")
	ErrCouldNotCastEnumValueToInt64 = NewGraphQLError("could not cast enum value to int64", true, "GEN-007")
	ErrNotAValidNameForEnumValue    = NewGraphQLError("not a valid name for an enum value", false, "GEN-008")
	ErrRecordNotFound               = NewGraphQLError("record not found", false, "GEN-009")
	ErrFieldAccessDenied            = NewGraphQLError("field access denied", false, "GEN-010")
	errGenericInternal              = NewGraphQLError("internal error", true, "GEN-000")
)

// GraphQLError is an error with info that is relevant for graphQL presentation
type GraphQLError struct {
	message     string
	isInternal  bool
	userMessage string
	code        string
}

// GraphQLErrorWithCause is a GraphQLError that is caused by another error
type GraphQLErrorWithCause struct {
	*GraphQLError
	cause error
}

// WithCode has a method to get a code
type WithCode interface {
	// GetCode returns the code
	GetCode() string
}

// WithIsInternal has a method to decide whether or not an error is internal
type WithIsInternal interface {
	// IsInternal returns whether or not this error is internal
	IsInternal() bool
}

// WithUserMessage contains information about a possible user message
type WithUserMessage interface {
	// HasUserMessage returns whether or not this error has a user message
	HasUserMessage() bool
	// GetUserMessage returns the user message
	GetUserMessage() string
}

// IsInternal returns whether or not this error is internal
func (err *GraphQLError) IsInternal() bool {
	return err.isInternal
}

// HasUserMessage returns whether or not this error has a user message
func (err *GraphQLError) HasUserMessage() bool {
	return err.userMessage != ""
}

// GetUserMessage returns the user message if it is set and otherwise returns Error()
func (err *GraphQLError) GetUserMessage() string {
	if err.HasUserMessage() {
		return err.userMessage
	}
	return err.Error()
}

// GetCode returns the code
func (err *GraphQLError) GetCode() string {
	return err.code
}

// Error returns the error message
func (err *GraphQLError) Error() string {
	return err.message
}

// Cause returns the causer of the error
func (err *GraphQLErrorWithCause) Cause() error {
	return err.cause
}

// Unwrap unwraps the error, returning the causer
func (err *GraphQLErrorWithCause) Unwrap() error {
	return err.cause
}

// Error returns the error message, containing the message from the causer
func (err *GraphQLErrorWithCause) Error() string {
	return fmt.Sprintf("%s: %s", err.message, err.cause.Error())
}

// NewGraphQLError creates a new graphQL error
func NewGraphQLError(message string, isInternalError bool, code string) *GraphQLError {
	return &GraphQLError{
		message:     message,
		isInternal:  isInternalError,
		userMessage: "",
		code:        code,
	}
}

// AttachCause attaches a cause to a graphQLError
func AttachCause(gqlErr *GraphQLError, cause error) *GraphQLErrorWithCause {
	return &GraphQLErrorWithCause{
		GraphQLError: gqlErr,
		cause:        cause,
	}
}

// WrapAsInternal wraps an error and then marks it as internal
func WrapAsInternal(err error, msg string) *GraphQLErrorWithCause {
	return AttachCause(errGenericInternal, errors.Wrap(err, msg))
}
