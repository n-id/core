package generator

import (
	"context"
	goErr "errors"

	"github.com/99designs/gqlgen/graphql"
	"github.com/sirupsen/logrus"
	"github.com/vektah/gqlparser/v2/gqlerror"
)

// ErrorLogger logs internal errors
type ErrorLogger interface {
	// LogInternalError logs an internal error
	LogInternalError(error)
}

// LogrusErrorLogger is the logrus implementation of ErrorLogger
type LogrusErrorLogger struct{}

// LogInternalError logs an internal error
func (*LogrusErrorLogger) LogInternalError(err error) {
	logrus.WithError(err).Error("internal server error")
}

// Presenter provides an error presenter for errors implemening the interfaces in this package
type Presenter struct {
	logger ErrorLogger
}

// NewPresenter creates a new Presenter
func NewPresenter(logger ErrorLogger) *Presenter {
	return &Presenter{logger: logger}
}

// Present presents an error in graphql format
func (p *Presenter) Present(ctx context.Context, e error) *gqlerror.Error {
	err := graphql.DefaultErrorPresenter(ctx, e)
	err.Extensions = make(map[string]interface{})

	var codeErr WithCode
	if goErr.As(err, &codeErr) {
		code := codeErr.GetCode()
		err.Extensions["code"] = code
	}

	var isInternalErr WithIsInternal
	if goErr.As(err, &isInternalErr) {
		if isInternalErr.IsInternal() {
			p.logger.LogInternalError(err)
			err.Message = "internal server error"
		}
	}

	var userMessageErr WithUserMessage
	if goErr.As(err, &userMessageErr) {
		if userMessageErr.HasUserMessage() { // Override, even for non-user errors
			err.Message = userMessageErr.GetUserMessage()
		}
	}

	return err
}
