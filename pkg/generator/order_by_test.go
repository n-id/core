package generator

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type OrderByTestSuite struct {
	suite.Suite
}

type orderDirection string

const (
	OrderDirectionAsc  orderDirection = "ASC"
	OrderDirectionDesc orderDirection = "DESC"
)

func (e orderDirection) String() string {
	return string(e)
}

func (e *orderDirection) UnmarshalGQL(v interface{}) error {
	str, ok := v.(string)
	if !ok {
		return ErrEnumsMustBeStrings
	}

	*e = orderDirection(str)
	return nil
}

type fieldName string

const (
	FieldNameName         fieldName = "NAME"
	FieldNameCreatedAt    fieldName = "CREATED_AT"
	FieldNameLicensePlate fieldName = "LICENSE_PLATE"
)

func (e fieldName) String() string {
	return string(e)
}

func (e *fieldName) UnmarshalGQL(v interface{}) error {
	str, ok := v.(string)
	if !ok {
		return ErrEnumsMustBeStrings
	}

	*e = fieldName(str)
	return nil
}

func TestOrderByTestSuite(t *testing.T) {
	s := &OrderByTestSuite{}
	suite.Run(t, s)
}

type orderByTest struct {
	OrderIsSet     bool
	OrderBy        string
	Order          fieldName
	OrderDirection orderDirection
	Result         string
}

func (row orderByTest) String() string {
	return row.OrderBy + "_" + row.Order.String() + "_" + row.OrderDirection.String()
}

func (s *OrderByTestSuite) runOrderByTest(test orderByTest) {
	var orderByPtr *string = nil
	if test.OrderBy != "" {
		orderByPtr = &test.OrderBy
	}

	var orderPtr *fieldName = nil
	if test.Order != "" {
		orderPtr = &test.Order
	}

	var orderDirectionPtr *orderDirection = nil
	if test.OrderDirection != "" {
		orderDirectionPtr = &test.OrderDirection
	}

	result, err := parseOrderBy(test.OrderIsSet, orderByPtr, orderPtr, orderDirectionPtr)
	s.Require().NoError(err)

	s.Equal(test.Result, result)
}

func (s *OrderByTestSuite) TestParseOrderBy() {
	table := []orderByTest{
		{OrderIsSet: true, OrderBy: "", Order: FieldNameName, OrderDirection: OrderDirectionAsc, Result: "name ASC"},
		{OrderIsSet: true, OrderBy: "", Order: FieldNameName, OrderDirection: OrderDirectionDesc, Result: "name DESC"},
		{OrderIsSet: false, OrderBy: "name ASC", Order: FieldNameCreatedAt, OrderDirection: OrderDirectionDesc, Result: "name ASC"},
		{OrderIsSet: false, OrderBy: "name DESC", Order: FieldNameCreatedAt, OrderDirection: OrderDirectionDesc, Result: "name DESC"},
		{OrderIsSet: false, OrderBy: "name", Order: FieldNameCreatedAt, OrderDirection: OrderDirectionAsc, Result: "name ASC"},
		{OrderIsSet: false, OrderBy: "name", Order: FieldNameCreatedAt, OrderDirection: OrderDirectionDesc, Result: "name DESC"},
		{OrderIsSet: true, OrderBy: "", Order: FieldNameLicensePlate, OrderDirection: OrderDirectionAsc, Result: "license_plate ASC"},
		{OrderIsSet: true, OrderBy: "", Order: FieldNameLicensePlate, OrderDirection: OrderDirectionDesc, Result: "license_plate DESC"},
		{OrderIsSet: false, OrderBy: "license_plate ASC", Order: FieldNameCreatedAt, OrderDirection: OrderDirectionDesc, Result: "license_plate ASC"},
		{OrderIsSet: false, OrderBy: "license_plate DESC", Order: FieldNameCreatedAt, OrderDirection: OrderDirectionDesc, Result: "license_plate DESC"},
		{OrderIsSet: false, OrderBy: "license_plate", Order: FieldNameCreatedAt, OrderDirection: OrderDirectionAsc, Result: "license_plate ASC"},
		{OrderIsSet: false, OrderBy: "license_plate", Order: FieldNameCreatedAt, OrderDirection: OrderDirectionDesc, Result: "license_plate DESC"},
	}

	for _, test := range table {
		s.Run(test.String(), func() { s.runOrderByTest(test) })
	}
}

func (s *OrderByTestSuite) TestParseOrderByOverride() {
	table := []orderByTest{
		{OrderIsSet: true, OrderBy: "name ASC", Order: FieldNameLicensePlate, OrderDirection: OrderDirectionDesc, Result: "license_plate DESC"},
		{OrderIsSet: true, OrderBy: "name DESC", Order: FieldNameLicensePlate, OrderDirection: OrderDirectionAsc, Result: "license_plate ASC"},
		{OrderIsSet: true, OrderBy: "license_plate ASC", Order: FieldNameName, OrderDirection: OrderDirectionDesc, Result: "name DESC"},
		{OrderIsSet: true, OrderBy: "license_plate DESC", Order: FieldNameName, OrderDirection: OrderDirectionAsc, Result: "name ASC"},
	}

	for _, test := range table {
		s.Run(test.String(), func() { s.runOrderByTest(test) })
	}
}

func (s *OrderByTestSuite) TestParseOrderByCaseInsensitive() {
	table := []orderByTest{
		{OrderIsSet: false, OrderBy: "name asc", Order: FieldNameCreatedAt, OrderDirection: OrderDirectionDesc, Result: "name ASC"},
		{OrderIsSet: false, OrderBy: "name desc", Order: FieldNameLicensePlate, OrderDirection: OrderDirectionDesc, Result: "name DESC"},
		{OrderIsSet: false, OrderBy: "name aSc", Order: FieldNameCreatedAt, OrderDirection: OrderDirectionDesc, Result: "name ASC"},
		{OrderIsSet: false, OrderBy: "name deSc", Order: FieldNameLicensePlate, OrderDirection: OrderDirectionDesc, Result: "name DESC"},
	}

	for _, test := range table {
		s.Run(test.String(), func() { s.runOrderByTest(test) })
	}
}
