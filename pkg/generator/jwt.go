package generator

import (
	"crypto/rsa"
	"time"

	"github.com/gofrs/uuid"
)

// JWT jwt Database model
type JWT struct {
	ID         uuid.UUID `gorm:"primaryKey"`
	CreatedAt  time.Time
	DeletedAt  *time.Time
	UpdatedAt  time.Time
	UserID     string
	Expiration time.Time
}

// TableName Database table name
func (m JWT) TableName() string {
	return "jwts"
}

var (
	privateKey *rsa.PrivateKey
	publicKey  *rsa.PublicKey
)

func PublicKey() *rsa.PublicKey {
	return publicKey
}

func PrivateKey() *rsa.PrivateKey {
	return privateKey
}
