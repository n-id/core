package scalars

import (
	"io"

	"github.com/99designs/gqlgen/graphql"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

// MarshalUUID implements the graphql.Marshaler interface
func MarshalUUID(id uuid.UUID) graphql.Marshaler {
	return graphql.WriterFunc(func(w io.Writer) {
		str := id.String()
		_, _ = w.Write([]byte("\"" + str + "\""))
	})
}

// UnmarshalUUID implements the graphql.Marshaler interface
func UnmarshalUUID(v interface{}) (uuid.UUID, error) {
	s, ok := v.(string)
	if !ok {
		return uuid.Nil, errors.New("UUID should be a string")
	}

	id, err := uuid.FromString(s)
	return id, errors.Wrap(err, "UnmarshalUUID")
}
