package scalars

import (
	"encoding/json"
	"fmt"
	"io"
	"strconv"

	"github.com/pkg/errors"
)

type Geography struct {
	Lng float64 `json:"lng"`
	Lat float64 `json:"lat"`
}

func (p *Geography) String() string {
	return fmt.Sprintf("POINT(%v %v)", p.Lng, p.Lat)
}

// MarshalGQL implements the graphql.Marshaler interface
func (g Geography) MarshalGQL(w io.Writer) {
	err := json.NewEncoder(w).Encode(g)
	if err != nil {
		println(err)
	}
}

// UnmarshalGQL implements the graphql.Marshaler interface
func (g *Geography) UnmarshalGQL(v interface{}) error {
	s, ok := v.(map[string]interface{})
	if !ok {
		return errors.New("Geography must be of type map[string]interface{}")
	}
	lat, ok := s["lat"]
	if !ok {
		return errors.New("Geography must have a 'lng' and 'lat' property")
	}
	lng, ok := s["lng"]
	if !ok {
		return errors.New("Geography must have a 'lng' and 'lat' property")
	}
	var err error
	if g.Lat, err = strconv.ParseFloat(fmt.Sprint(lat), 64); err != nil {
		return errors.Wrap(err, "could not parse latitude value")
	}
	if g.Lng, err = strconv.ParseFloat(fmt.Sprint(lng), 64); err != nil {
		return errors.Wrap(err, "could not parse longitude value")
	}
	return nil
}
