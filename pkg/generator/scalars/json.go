package scalars

import (
	"encoding/json"
	"io"

	"github.com/99designs/gqlgen/graphql"
	"github.com/pkg/errors"
	"gorm.io/datatypes"
)

// MarshalGQL implements the graphql.Marshaler interface
func MarshalJSON(j datatypes.JSON) graphql.Marshaler {
	return graphql.WriterFunc(func(w io.Writer) {
		w.Write(j)
	})
}

// UnmarshalGQL implements the graphql.Marshaler interface
func UnmarshalJSON(v interface{}) (datatypes.JSON, error) {
	b, err := json.Marshal(v)
	if err != nil {
		return datatypes.JSON{}, errors.Wrap(err, "scalars.JSON.UnmarshalGQL")
	}
	return datatypes.JSON(b), nil
}
