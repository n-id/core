package scalars

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/suite"
)

type SuiteGeography struct {
	suite.Suite
}

func TestSuiteGeography(t *testing.T) {
	suite.Run(t, new(SuiteGeography))
}

func (s *SuiteGeography) TestMarshalGQL() {
	g := Geography{51.9238367, 4.4903756}
	w := new(bytes.Buffer)
	g.MarshalGQL(w)
	expect := `{"lng":51.9238367,"lat":4.4903756}`
	s.Equal(expect+"\n", w.String())
}

func (s *SuiteGeography) TestUnmarshalGQL() {
	g := Geography{}
	expect := map[string]interface{}{"lng": 51.9238367, "lat": 4.4903756}
	err := g.UnmarshalGQL(expect)
	s.NoError(err)
	s.Equal(51.9238367, g.Lng)
	s.Equal(4.4903756, g.Lat)
}
