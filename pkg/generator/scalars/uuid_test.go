package scalars

import (
	"bytes"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/suite"
)

type SuiteUUID struct {
	suite.Suite
}

func TestSuiteUUID(t *testing.T) {
	suite.Run(t, new(SuiteUUID))
}

func (s *SuiteUUID) TestMarshalGQL() {
	id := uuid.Must(uuid.FromString("601cb591-0214-44d8-adab-73bb820c31c5"))
	w := new(bytes.Buffer)
	MarshalUUID(id).MarshalGQL(w)
	s.Equal(`"601cb591-0214-44d8-adab-73bb820c31c5"`, w.String())
}

func (s *SuiteUUID) TestUnmarshalGQL() {
	id, err := UnmarshalUUID("601cb591-0214-44d8-adab-73bb820c31c5")
	s.NoError(err)
	s.EqualValues(uuid.Must(uuid.FromString("601cb591-0214-44d8-adab-73bb820c31c5")), id)
}
