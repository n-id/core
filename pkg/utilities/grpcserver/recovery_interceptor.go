package grpcserver

import (
	"context"
	"runtime/debug"

	"gitlab.com/n-id/core/pkg/utilities/log/v2"

	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/n-id/core/pkg/utilities/grpcserver/logfields"
)

// getRecoveryFunction returns a function that attempts to recover the panic and return the correct response.
// The problemDetails parameter specifies if the recoveryFunction should return problem details.
func getRecoveryFunction() grpc_recovery.RecoveryHandlerFuncContext {
	return func(ctx context.Context, p interface{}) error {
		log.WithContext(ctx).WithField(logfields.RecoveredPanic, p).WithField(logfields.Stack, string(debug.Stack())).Error("unhandled panic recovered in panicRecoveryHandlerFunc")
		return status.Errorf(codes.Internal, "Internal server error")
	}
}
