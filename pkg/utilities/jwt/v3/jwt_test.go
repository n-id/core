package jwt

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"strings"
	"testing"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/stretchr/testify/suite"
)

type ClientTestSuite struct {
	suite.Suite

	privateKey    *rsa.PrivateKey
	claims        jwt.RegisteredClaims
	claimsExpired jwt.RegisteredClaims
}

func (c *ClientTestSuite) SetupTest() {
	c.privateKey = parseKey(testingKey(`-----BEGIN RSA TESTING KEY-----
MIICXQIBAAKBgQDC1XGcCpTfMcotl4lAmYba3eGMWDUgxY/qp0TyO/yvbVN9Yd7B
OiiCj/vLVch7/5RykSlmrwZFzDk/mmlWJp1EHLBT9eY0lQ4rTH54jzP/CVgYz4tE
8L6CkBtSIaz53HvC6g+7qO7Oq4/xgc7yJu6mf3XA5oHqBCgYt9GFAgtdlwIDAQAB
AoGBAIvwgwlxEDy/plfY39xmaVYKyFXlhUNko1oHip3yOCxIeg80nKps+2vLvy7t
DnMBv74KRZJbZEX9yaDFZ6NZWNqyWiPJxaD0aUA6OoW71Rjai4BuDGaolmLmzCOQ
qQkNMcixc3Mf/EZNGAHRubMqZiJrV7C0fpcdKMBUu8QT8mkBAkEA5EjzNa+EEL6J
WoqWi9H0WJOAENQe9TKpCUTntuwzYWeZx330BGViKLrGpsdBz0o1HttOvH9Ct/S0
RTuBwn8oXQJBANp81l/sUeBRb0h5igF/KqCPMkaQZpurzxmjXkCRpaRaLR7DAX1L
yFk/l+uWXi1joi9ibevMSukA79w+g5kNLoMCQCJWns7ageEK6BpIcnDBuiGAvmjJ
n4Z+glK0nK41RV1Heig51/S5U1SRE8SOgGWv4eGsV2GfOuf01RD87H3XJrkCQQCA
zFrrzTovDJKXnN5YJ4kPgZrXskRqvIunx4DGe5W0H1y956M3I7eFtVFvlgMKqtNv
L40xFguK8/xyT3/wd67VAkBagJGCtkvnDwuYCyEUWHehkP6k/A4MQY4+eQrp9NtI
SSMaw7IVokiCt8HSO4vFEF0uEqJMUR131yVjbKEBFsea
-----END RSA TESTING KEY-----`))

	c.claims = NewDefaultRefreshTokenClaims("321", "123", 200)

	c.claimsExpired = NewDefaultRefreshTokenClaims("321", "123", 250)
	c.claimsExpired.ExpiresAt = jwt.NewNumericDate(time.Now().Add(-time.Hour))
}

func parseKey(s string) *rsa.PrivateKey {
	p, _ := pem.Decode([]byte(s))
	k, err := x509.ParsePKCS1PrivateKey(p.Bytes)
	if err != nil {
		panic(err)
	}
	return k
}

func testingKey(s string) string { return strings.ReplaceAll(s, "TESTING KEY", "PRIVATE KEY") }

func (c *ClientTestSuite) TestCreateNewClient() *Client {
	jwtClientOpts := DefaultOpts()
	client := NewJWTClientWithOpts(c.privateKey, &c.privateKey.PublicKey, jwtClientOpts)
	c.Require().NotNil(client)

	return client
}

func (c *ClientTestSuite) TestSignRefreshTokenWithClaim() string {
	client := c.TestCreateNewClient()

	token, err := client.SignToken(c.claims)
	c.Require().NoError(err)

	c.Require().NotEmpty(token)

	return token
}

func (c *ClientTestSuite) TestParseAndVerifyToken() {
	jwtClientOpts := DefaultOpts()
	client := NewJWTClientWithOpts(c.privateKey, &c.privateKey.PublicKey, jwtClientOpts)
	c.Require().NotNil(client)

	token, err := client.SignToken(c.claims)
	c.Require().NoError(err)
	c.Require().NotEmpty(token)

	claims := jwt.RegisteredClaims{}
	parsedToken, err := jwt.ParseWithClaims(token, &claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, ErrInvalidToken
		}

		return c.privateKey.Public(), nil
	})
	c.Require().NoError(err)

	c.Require().NotEmpty(parsedToken)
	c.Require().Equal(claims, c.claims)
	c.Require().True(parsedToken.Valid)
}

func (c *ClientTestSuite) TestParseAndVerifyInvalidToken() {
	token := c.TestSignRefreshTokenWithClaim()

	token += "IfIAddThisToTheSignatureThenTheSignatureBecomesInvalid"

	claims := jwt.RegisteredClaims{}
	parsedToken, err := jwt.ParseWithClaims(token, &claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, ErrInvalidToken
		}

		return c.privateKey.Public(), nil
	})
	c.Require().Error(err)

	c.Require().NotEmpty(parsedToken)
	c.Require().Equal(claims, c.claims)
	c.Require().False(parsedToken.Valid)
}

func (c *ClientTestSuite) TestParseTokenWithInvalidToken_ExpiredToken() {
	client := c.TestCreateNewClient()

	token, err := client.SignToken(c.claimsExpired)
	c.Require().NoError(err)
	c.Require().NotEmpty(token)

	claims := jwt.RegisteredClaims{}
	parsedToken, err := jwt.ParseWithClaims(token, &claims, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodRSA); !ok {
			return nil, ErrInvalidToken
		}

		return c.privateKey.Public(), nil
	})
	c.Require().Error(err)

	c.Require().NotEmpty(parsedToken)
	c.Require().Equal(claims, c.claimsExpired)
	c.Require().False(parsedToken.Valid)
}

func TestClientTestSuite(t *testing.T) {
	suite.Run(t, &ClientTestSuite{})
}
