package jwt

import (
	"time"

	"github.com/golang-jwt/jwt/v5"

	"github.com/gofrs/uuid"
)

// NewDefaultClaims will initiate the DefaultClaims struct
func NewDefaultClaims(expDurationHours time.Duration) jwt.RegisteredClaims {
	return jwt.RegisteredClaims{
		Issuer:    "nid.nl",
		Audience:  []string{"nid"},
		ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * expDurationHours)),
		ID:        uuid.Must(uuid.NewV4()).String(),
		IssuedAt:  jwt.NewNumericDate(time.Now()),
		NotBefore: jwt.NewNumericDate(time.Now().Add(-2 * time.Minute)),
	}
}

// NewDefaultRefreshTokenClaims  returns claims for the refresh token
func NewDefaultRefreshTokenClaims(subjectID, tokenID string, expDurationHours time.Duration) jwt.RegisteredClaims {
	return jwt.RegisteredClaims{
		ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Hour * expDurationHours)),
		ID:        tokenID,
		IssuedAt:  jwt.NewNumericDate(time.Now()),
		Subject:   subjectID,
		NotBefore: jwt.NewNumericDate(time.Now()),
	}
}
