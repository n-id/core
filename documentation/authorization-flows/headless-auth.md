# Headless authorisation

Dit document beschrijft de autorisatie flow waarbij burgers niet direct betrokken zijn, denk hierbij aan "consent by law". Binnen dit document noemen we deze flow "authorize headless".

De authorize headless flow creëert een autorisatie token zonder dat hierbij goedkeuring nodig is van de burger. De aanvraag van een verzoek service wordt beoordeeld door een LUA script. Dit script bepaald vervolgens of de verzoek service toegang krijgt tot de gevraagde bron. De implementatie van dit script kan bijvoorbeeld zijn "het beoordelen van de aanvraag op basis van wettelijke grondslag". Na het beoordelen van de aanvraag zal er een authenticatie code naar de opgegeven redirect URL worden gestuurd, hiermee kan de partij een JWT token opvragen welke vervolgens gebruikt kan worden voor het raadplegen van de bron.

# Onboarding

Om gebruik te kunnen maken van de Headless authorisation, dient de verzoek service aan een aantal voorwaarden te voldoen: 

 - Verzoek service moet geregistreerd staan bij het platform.
 - Verzoek service moet toegang hebben tot de LUA runner.
 - Inhoud en werking van het LUA script moet overeengekomen worden tussen het platform en de verzoek service. Momenteel biedt het platform geen standaard oplossingen voor de implementatie van het script.  

# Flow

 - Verzoek service roept LUA runner aan met het betreffende subject en eventueel aanvullende gegevens. 
 - LUA runner verwerkt aanvraag en roept de redirect URL van de verzoek service aan. 
 - Verzoek service gebruikt autorisatie token om een JWT token op te vragen. 
 - Verzoek service raadpleegt de bron met behulp van de JWT token.  