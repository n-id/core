# Aggregated Claims

nID can manage identities or use external identity management, either way,
nID can verify these identities to create a token for the subject.
Since nID is only an identity agent, it cannot issue many claims about the subject.
It can however aggregate claims from multiple sources and issue a token with these claims.

<!-- toc -->

- [What is Aggregated Claims?](#what-is-aggregated-claims)
- [How does it work?](#how-does-it-work)
- [Verifying a nID Token](#verifying-a-nid-token)
- [Issuing Authority (IA) requirements](#issuing-authority-ia-requirements)
  - [Token endpoint with Identity Chaining Across Domains grant type](#token-endpoint-with-identity-chaining-across-domains-grant-type)
  - [Claims Endpoint](#claims-endpoint)
  - [JWKS](#jwks)

## What is Aggregated Claims?

Aggregated claims is a concept that allows an identity agent (nID) to aggregate claims from multiple sources.
The token is signed by the identity agent (nID) and can be verified by anyone using the JWKS.
Inside the token are claims each coming from a Issuing Authority (IA), with each claim signed by the IA.

Aggregating claims this way allows nID to be responsible for the identity of the subject, but not
responsible for the claims about the subject. But, most importantly, it allows the subject to
verify the claims and sources of the claims.

## How does it work?

When aggregation is enabled, nID will need a way to know which claims to fetch from which Issuing Authority (IA).
This is done by a Claim Names Provider. nID will send the subject's information, audience and scopes to the claim names provider and expects a map of Issuing Authority URLs with a list of claim names to requested from it.

Then the identity agent (nID) will request an access token from the Issuing Authority (IA) for the subject via [Identity Chaining Across Domains](https://www.ietf.org/archive/id/draft-ietf-oauth-identity-chaining-01.html#section-2.3).
Using the access token, the nID will request the claims from the Issuing Authority (IA) and put the claim names, with the signed claims, in the nID token.

Below is a diagram of the process

```mermaid
sequenceDiagram
    autonumber
    participant S as Subject (Dentist)
    participant IdA as Identity-Agent (nID)
    participant CNP as Claim Names Provider (OPA Policy)
    participant IA as Issuing-Authority (Claims Provider (Vecozo))

    S->>+IdA: Request Data from Indicatie register
    note over S,IdA: With Scopes and Audience

    IdA->>+IdA: Identity Check

    IdA->>+CNP: Request Claim Names
    note over IdA,CNP: With Subject, Audience and Scopes
    CNP-->>-IdA: Return Claim Names

    loop
        IdA->>+IA: Obtain Access Token via Identity Chaining Across Domains
        IA-->>-IdA: Return Access Token bound to subject

        IdA->>+IA: Obtain Claims about Subject
        IA-->>-IdA: Return Signed Claims about Subject
    end

    IdA->>IdA: Aggregate Claims

    IdA-->>-S: Aggregated claims token
```

## Verifying a nID Token

Verifying a nID token when using aggregated claims is now a multi-step process.
First the nID token is verified using the JWKS of the nID.
Then the claim sources in the nID token are verified using the JWKS of the Issuing Authority (IA).

```mermaid
sequenceDiagram
    autonumber
    participant S as Subject (Tandarts)
    participant RP as Resource Proxy
    participant IdA as Identity-Agent (nID)
    participant IA as Issuing-Authority (Claims Provider(Vecozo))
    participant RS as Resource Server

    S->>+RP: Request Data from Indicatie register
    RP->>+IdA: Get JWKS from root token
    IdA-->>-RP: Return JWKS
    RP->>+RP: Verify token

    loop
        RP->>+IA: Get claim source JWKS
        IA-->>-RP: Return JWKS
        RP->>+RP: Verify claims source token
    end

    RP->>+RS: Request Data from Indicatie register
    RS-->>-S: Return Indicatie register data
```

## Issuing Authority (IA) requirements

### Token endpoint with Identity Chaining Across Domains grant type

The token endpoint of the Issuing Authority (IA) must support the Identity Chaining Across Domains grant type. (see [draft-ietf-oauth-identity-chaining-01](https://www.ietf.org/archive/id/draft-ietf-oauth-identity-chaining-01.html))

This grant type uses an assertion, containing the subject's information as a JWT signed the nID.
The token endpoint must validate the assertion and use the information in the assertion to issue an access token.

### Claims Endpoint

The Issuing Authority (IA) must provide a claims endpoint that can be requested with an access token following the Aggregation specification. (see [claims-aggregation-draft-02](https://openid.net/specs/openid-connect-claims-aggregation-1_0.html)).
The claims endpoint must return a JWT containing the claims of the subject.

### JWKS

The Issuing Authority (IA) must publish a JWKS in the JWKS endpoint. This JWKS is used to verify the nID token containing the aggregated claims.
