# scopes
Elk verzoek aan de burger bevat bepaalde scopes, dit zijn de velden waar de informatieservice toegang tot vraagt. <br>
Binnen de sandbox zijn er twee databronnen beschikbaar hieronder per bron de beschikbare access models

## Databron
Omschrijving: Voor- en achternaam<br>
Naam: databron:name<br>
Hash: 3bf8826ff2805c4822536e609ae9d1dcb3c3b9b89e077f0d73963c60c1a408e9<br>

Omschrijving: Bankrekeningen<br>
Naam: databron:bankaccounts<br>
Hash: 791a985542b63d6593830bda69258da190d79cfdd28482cc5e6f85031c375189<br>

Omschrijving: Voor- en achternaam middels BSN<br>
Naam: databron:name:bsn<br>
Hash: 3bf8826ff2805c4822536e609ae9d1dcb3c3b9b89e077f0d73963c60c1a408e9<br>

Omschrijving: Bankrekeningen middels BSN<br>
Naam: databron:bankaccounts:bsn<br>
Hash: 791a985542b63d6593830bda69258da190d79cfdd28482cc5e6f85031c375189<br>


## Other databron
Omschrijving: Adresgegevens<br>
Naam: databron:addresses<br>
Hash: 3bf8826ff2805c4822536e609ae9d1dcb3c3b9b89e077f0d73963c60c1a408e9<br>

Omschrijving: Contactgegevens<br>
Naam: databron:contactdetails<br>
Hash: 791a985542b63d6593830bda69258da190d79cfdd28482cc5e6f85031c375189<br>