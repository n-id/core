# Developer portal

> **_LET OP:_** Enige kennis van het TWI platform is nodig om gebruik te maken van het developer portal, daarom wordt het aangeraden om eerst de algemene [documentatie](../introduction/) door te lezen.


Het developer portal is bedoeld voor het testen van informatieservices. Ontwikkelaars kunnen hun informatieservice testen binnen een sandbox voordat zij aangesloten worden op de productie omgeving. 
Binnen dit document worden de verschillende onderdelen van het portal omschreven, tevens wordt uitgelegd hoe een ontwikkelaar gebruik kan maken van het developer portal.

## Sandbox
Het developer portal beschikt over een omgeving waarin ontwikkelaars hun informatieservice kunnen testen, deze omgeving noemen we de "Sandbox". Hierin draaien alle services van TWI zoals ze ook in de productieomgeving draaien. De ontwikkelaar kan zijn informatieservice aansluiten op de sandbox, waarna de aansluiting en integratie met nid getest kan worden. 

Hieronder worden de stappen beschreven die nodig zijn om gebruik te kunnen maken van de sandbox

### aansluiten
Als eerste dient de ontwikkelaar zich aan te melden voor het developers portal, hierbij moet men een `redirect URL` aanleveren. Deze wordt gebruikt in de [OAuth flow](../introduction/autorisatie/autorisatie.md). Nadat de ontwikkelaar goedgekeurd is wordt er een `client ID` en `password` aangeleverd, deze zijn nodig tijdens de OAuth flow. 

Tevens ontvangt de ontwikkelaar de gegevens van een voorbeeld burger, hiermee kan de OAuth flow doorlopen worden.

### toestemming vragen 

De eerste stap voor de ontwikkelaar zal zijn om toestemming te vragen aan de burger voor het gebruik van bepaalde gegevens. Hiervoor kan de onderstaande URL in de browser geplakt worden nadat de parameters zijn vervangen:

```curl
https://auth.twi.twi-sandbox.n-id.network/authorize?scope=$scopes&response_type=code&client_id=$client_id&redirect_uri=$redirect_uri&audience=$audience&optional_scopes=$optional_scopes
```

Hieronder worden de benodigde parameters toegelicht:

|Param|Betekenis|
|---|---|
|scope|Geef hier de [scope](./scopes.md) op die nodig is voor het verzoek, met het format: `openid%20{name}%40{hash}` (url encoded)|
|client_id|Het verkregen client ID|
|redirect_uri|De redirect URL waarop de informatieservice luistert naar verzoeken (URL kan niet eindigen op een /) |
|audience|De URL van de [bron](./bronnen.md) service waar gegevens opgehaald moeten worden|
|optional_scopes|Geef hier optionele [scopes](./scopes.md) op die zijn is voor het verzoek, met het format: `{name}%40{hash}` (url encoded)|

### Toestemming geven

Als de toestemmings URL in de browser wordt geplakt zal deze de gebruiker doorsturen naar het TWI platform. Hier kan worden ingelogd met de test gebruiker zodat er toestemming kan worden gegeven tot het opvragen van de gegevens. 

Om toestemming te kunnen geven is ook de bijbehorende TWI wallet nodig:
`https://wallet.twi.twi-sandbox.n-id.network`

### Inwisselen code voor JWT token

Als de gebruiker toegang heeft gegeven wordt de `redirect URL` van de informatieservice aangeroepen. Dit is dus een endpoint van de te ontwikkelen applicatie. Deze URL wordt aangeroepen met een parameter `authorization_code`. Deze code kan gebruikt worden om een JWT token op te halen die toegang verschaft tot de bronnen. 

Het ophalen van de JWT kan doormiddel van een request naar `http://auth.twi.twi-sandbox.n-id.network/token?grant_type=authorization_code&authorization_code={code}`, dit endpoint vereist authenticatie middels basic auth headers ([base64-encoded credentials](https://datatracker.ietf.org/doc/html/rfc7617)). Hieronder een voorbeeld met cURL: 

```bash
curl --location --request --tlsv1.3 GET 'http://auth.twi.twi-sandbox.n-id.network/token?grant_type=authorization_code&authorization_code={code}' \
--header 'Authorization: Basic {basiAuth}'
```

Hierbij is het nodig om Basic Authorization headers mee te geven, hiervoor kan het client ID als username gebruikt worden samen met het aangeleverde wachtwoord.

### Bevragen bron

Eenmaal in het bezit van een JWT token, kan deze gebruikt worden om de bron te bevragen. Doordat de informatieservice op dit moment nog extern draait, zal er gebruik gemaakt moeten worden van het toegangspunt voor externe services: External Service Gateway (ESG).

Een verzoek naar de bron `databron` ziet er dan als volgt uit: 
```bash
curl --location --request POST 'https://esg.twi.twi-sandbox.n-id.network/databron.twi/gql' \
--header 'authorization: bearer {JWT}' \
--header 'Content-Type: application/json' \
--data-raw '{"query":"query {\n    users(filter: {\n        pseudonym: {\n            eq: \"$$nid:subject$$\"\n        }\n    }){\n            \n        firstName\n        lastName\n       \n    }\n}","variables":{}}'
```
`Databron` is een GraphQL API de GQL query die gebruikt is ziet er als volgt uit:
```gql

query {
    users(filter: {
        pseudonym: {
            eq: "$$nid:subject$$"
        }
    }){
            
        firstName
        lastName
       
    }
}
```