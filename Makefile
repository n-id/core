.PHONY: lint test generate

# Default variables
GOBIN = $(GOPATH)/bin

# Include your custom make commands here.
GOLANGCI_LINT := $(GOPATH)/bin/golangci-lint

GO_TEST_BUILD_TAGS="proxytest"
GOLANGCI_ADDITONAL_ARGS="--build-tags=proxytest"

clean:
	rm svc/*/proto/*.pb.go
	rm svc/*/proto/*.swagger.json
	rm svc/*/proto/api_descriptor.pb

test.e2e: ## Runs the e2e tests on your e2e cluster
	go clean -testcache
	(\
		go test -p 10 -v --tags=integration ./integration-tests $(ARGS) -short\
	)

### Linting
GOLANGCI_CONCURRENCY := "8"

GOLANGCI_OUTPUT_FILE := "lintoutput.txt"

lint: COMMITS_WITH_UPSTREAM := $(shell git rev-list --reverse $(MAIN_BRANCH_NAME)..HEAD^ | tr '\n' " " | rev | cut -c2- | rev)
lint: CURRENT_REVISION := $(shell git rev-parse --abbrev-ref HEAD)

lint: .golangci.yml $(GO_FILES) ## Lint the project, the SINCE parameter can be used to lint all commits between head and the provided ref, to only show lint issues introduced after a certain ref use the NEW_FROM_REF parameter
	@printf "Linting commits: $(COMMITS_WITH_UPSTREAM)"
	@if [[ "$(SINCE)" != "" ]]; then\
		test -d $$(git rev-parse --show-toplevel)/.git/rebase-merge -o -d $$(git rev-parse --show-toplevel)/.git/rebase-merge && printf "\033[1;31mCannot lint from a commit when a rebase is in progress\033[0m\n" && exit 1;\
		for commit in $(COMMITS_WITH_UPSTREAM); do \
			printf "linting $$commit\n";\
			git checkout $$commit;\
			golangci-lint  -v --concurrency=$(GOLANGCI_CONCURRENCY) --config=.golangci.yml --issues-exit-code=0 run --out-format=colored-line-number ${GOLANGCI_ADDITONAL_ARGS} --modules-download-mode="" 2> $(GOLANGCI_OUTPUT_FILE) || failed=1;\
			if [[ "$$failed" -ne 0 ]]; then\
				printf "\x1b[1;31mLinting failed for $$commit\033[0m\n";\
				cat $(GOLANGCI_OUTPUT_FILE);\
				printf "\x1b[1;31m You can fix this using 'git rebase --interactive '$$commit^\n'\033[0m\n";\
				git checkout $(CURRENT_REVISION);\
				exit 1;\
			fi;\
		done;\
		git checkout $(CURRENT_REVISION);\
	fi
	@printf "Linted previous commits\n"
	@golangci-lint  -v --concurrency=$(GOLANGCI_CONCURRENCY) --config=.golangci.yml --issues-exit-code=0 run \
	--out-format=colored-line-number ${GOLANGCI_ADDITONAL_ARGS} --modules-download-mode="" \
	--new-from-rev=$(NEW_FROM_REF)

### Test

GOTEST = $(GOBIN)/gotest

$(GOTEST):
	go install github.com/rakyll/gotest

test: $(shell find . -name "*.go") $(GQL_PATH) $(MODELS_PATH) | $(GOTEST) ## Test runs all tests in the project (GO_TEST_PARALLEL_NUM: max parallel tests (default: 4), GO_TEST_BUILD_TAGS: build tags to provdide while testing)
	@mkdir -p reports
	# Use env var GO_TEST_DIR if defined, otherwise use make var GO_TEST_DIR if defined, otherwise use ./...
	@if [ -n "${GO_TEST_DIR}" ]; then\
		GO_TEST_DIR=${GO_TEST_DIR};\
	else\
		GO_TEST_DIR="./...";\
	fi;\
	LOGFORMAT=ASCII gotest -covermode=count -p=$(or ${GO_TEST_PARALLEL_NUM},${GO_TEST_PARALLEL_NUM},4) -v -coverprofile reports/codecoverage_all.cov --tags=${GO_TEST_BUILD_TAGS} `go list $$GO_TEST_DIR`
	@echo "Done running tests"
	@go tool cover -func=reports/codecoverage_all.cov > reports/functioncoverage.out
	@go tool cover -html=reports/codecoverage_all.cov -o reports/coverage.html
	@echo "Run make coverage-report to view coverage report"
	@tail -n 1 reports/functioncoverage.out

coverage-report: ## Shows the test coverage report of your last test run
	@open reports/coverage.html

generate:
	@for folder in $$(find ./svc -maxdepth 1 -mindepth 1 -type d); do \
		echo "Running generation in $$folder"; \
		\
		proto_dir=$$(find "$$folder" -type d -name 'proto'); \
		if [ -d "$$proto_dir" ]; then \
			echo "Found proto folder in $$proto_dir"; \
			docker run --rm --platform linux/amd64 -v $$proto_dir:/defs namely/protoc-all:1.51_2 -d . -l descriptor_set -o . --descr-filename api_descriptor.pb --grpc-out generate_package_definition; \
			docker run --rm --platform linux/amd64 -v $$proto_dir:/defs namely/protoc-all:1.51_2 -d . -l go -o . --with-validator --go-grpc-require-unimplemented-servers; \
		else \
			echo "No proto folder found in $$folder"; \
		fi; \
		\
	done
	@echo "Running mockery"
	@mockery --log-level="warn"
